import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import ApolloClient, { InMemoryCache } from "apollo-boost";
import { RootSession } from "./App";
import * as serviceWorker from "./serviceWorker";
import { LOCAL_API_URL, LOCAL_API_PATH } from "./containers/Utils/UrlConfig";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import "./index.css";
import "./antd.css";
import "./Language/index";

const stripePromise = loadStripe(
  "pk_live_51IK8ZbCf1EdHpU7RC2PVdZ3ZftRihOfazxCC8da5mJJifgDsc6xCbUPwn6cYTdFE011ZeTbIJN4Fp1KhdjyqTmfN00fBl3wKMW"
);

// Configuración del Apollo Client
const client = new ApolloClient({
  uri: LOCAL_API_URL + LOCAL_API_PATH,
  // enviar token al servidor
  fetchOptions: {
    credentials: "include",
  },
  request: (operation) => {
    const token = localStorage.getItem("token");
    operation.setContext({
      headers: {
        authorization: token,
      },
    });
  },
  cache: new InMemoryCache({
    addTypename: false,
  }),
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors", graphQLErrors);
    // if(graphQLErrors && graphQLErrors[0].extensions.code === 'UNAUTHENTICATED'){
    //   message.error('Debe iniciar sesión para agregar un servicio.');
    // }
    console.log("networkError", networkError);
  },
});

ReactDOM.render(
  <Elements stripe={stripePromise}>
    <ApolloProvider client={client}>
      <RootSession />
    </ApolloProvider>
  </Elements>,
  /* //AppRegistry.registerComponent("App", () => RootSession),
  AppRegistry.runApplication("App", {
    rootTag: document.getElementById("root"),
  }) */
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
