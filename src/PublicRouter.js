import React from "react";
import { Route, Redirect } from "react-router-dom";

const PublicRoutePro = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      localStorage.getItem("token") ? (
        <Redirect
          to={{
            pathname: "/profile",
            state: { from: props.location },
          }}
        />
      ) : (
        <Component {...props} refetch={rest.refetch} />
      )
    }
  />
);

export default PublicRoutePro;
