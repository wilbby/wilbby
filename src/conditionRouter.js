import React from "react";
import { Route, Redirect } from "react-router-dom";
import { Query } from "react-apollo";
import { GET_CITY } from "./containers/GraphQL/query";

const ConditionRoute = ({ component: Component, ...rest }) => (
  <Query query={GET_CITY} variables={{ city: localStorage.getItem("city") }}>
    {(response) => {
      if (response) {
        const ciudada =
          response && response.data && response.data.getCity
            ? response.data.getCity.data
            : {};
        return (
          <Route
            {...rest}
            render={(props) =>
              ciudada && !ciudada.close ? (
                <Component {...props} />
              ) : (
                <Redirect
                  to={{
                    pathname: "/",
                    state: { from: props.location },
                  }}
                />
              )
            }
          />
        );
      }
    }}
  </Query>
);

export default ConditionRoute;
