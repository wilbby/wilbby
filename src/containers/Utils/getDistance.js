const GOOGLE_DISTANCE_API =
  "https://maps.googleapis.com/maps/api/distancematrix/json";
const GOOGLE_API_KEY = "AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA";

export const getDistacia = async (
  coordetate,
  coordenateone,
  coordetateRest,
  coordenateoneRest
) => {
  let apiUrlWithParams = `${GOOGLE_DISTANCE_API}?units=imperial&origins=${coordetate},${coordenateone}&destinations=${coordetateRest},${coordenateoneRest}&key=${GOOGLE_API_KEY}`;
  const response = await fetch(apiUrlWithParams);

  return response;
};
