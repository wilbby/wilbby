export const formaterPrice = (price, language, currency) => {
  const prices = new Intl.NumberFormat(language ? language : "es-ES", {
    style: "currency",
    currency: currency ? currency : "EUR",
  }).format(price);
  return prices;
};
