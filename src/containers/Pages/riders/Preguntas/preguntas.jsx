import React from "react";
import "./index.css";
import { Collapse } from "antd";

const { Panel } = Collapse;

export default function Preguntas() {
  return (
    <div className="cont_AQ">
      <h1>Preguntas frecuentes</h1>
      <Collapse accordion>
        <Panel header="¿Qué es un repartidor Wilbby?" key="1">
          <p>
            Los repartidores de Wilbby son usuarios que quieren hacer la vida
            más fácil a los demás entregándoles lo que necesitan, cuando lo
            necesitan. Son profesionales independientes que optan por realizar
            las entregas con su propio vehículo en franjas horarias previamente
            programadas.
          </p>
        </Panel>
        <Panel header="¿Cómo activo mi cuenta de rider?" key="2">
          <p>
            El proceso es rápido y fácil. Simplemente regístrate en esta página,
            ve una breve sesión informativa en línea y envía los documentos
            solicitados. Después, una vez que tengas una mochila isotérmica y
            hayas aprendido a utilizar la aplicación para repartidores, ¡ya
            puedes empezar a entregar pedidos!
          </p>
        </Panel>
        <Panel header="¿Cómo funcionan las tarifas de Wilbby?" key="3">
          <p>
            Los riders reciben un pago por cada pedido entregado. El cálculo de
            la tarifa de cada pedido se realiza en base a la complejidad del
            mismo. Recibirás la información de la tarifa del pedido, así como el
            local de recogida y la dirección de entrega, cuando recibas el mismo
            en tu app y podrás decidir libremente si aceptar o rechazar el mismo
            sin ninguna penalización. Cada dos lunes recibirás un resumen con
            las cantidades pendientes de cobro y los ajustes de facturación.
            También puedes cobrar cuando lo desees con la funcionalidad de
            “Cobro instantáneo” que te permite retirar tus ingresos en el
            momento que prefieras. Este servicio tiene un coste de 0,50 céntimos
            en cada uso, pero te permite disponer de tus ingresos al instante si
            es día laboral. Los riders reciben el 100% de las propinas que dan
            los clientes a través de la app, y estas se pagan a la vez que el
            resto de tarifas de tus pedidos.
          </p>
        </Panel>
        <Panel header="¿Qué incluye el kit de rider de Wilbby?" key="4">
          <p>
            Una vez seas rider de Wilbby podrás adquirir un kit de reparto a
            través de nuestra web, disfrutando de muchas promociones y
            descuentos.
          </p>
          <p>
            Actualmente todos los repartidores que lo solicitan reciben el
            siguiente material de reparto.
          </p>
          <h3>Bicicletas y Motos</h3>
          <ul style={{ marginLeft: 20 }}>
            <li>
              <p>Chaqueta reflectante impermeable</p>
            </li>
            <li>
              <p>Soporte de teléfono</p>
            </li>
            <li>
              <p>Casco</p>
            </li>
            <li>
              <p>Bolsa térmica pequeña</p>
            </li>
            <li>
              <p>Mochila térmica</p>
            </li>
          </ul>
        </Panel>
        <Panel
          header="¿Cuáles son los requisitos necesarios para entregar pedidos con Wilbby?"
          key="6"
        >
          <p>
            Para poder ofrecer tus servicios como repartidor, deberás firmar
            nuestro contrato y cumplir la normativa de tu región relativa a los
            profesionales autónomos. También necesitarás tu propio vehículo y
            ser mayor de edad.
          </p>
        </Panel>
      </Collapse>
    </div>
  );
}
