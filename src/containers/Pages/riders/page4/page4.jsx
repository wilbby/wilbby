import React from "react";
import "./index.css";
import Line from "./lines.svg";
import { CheckCircleFilled } from "@ant-design/icons";

const img1 = require("./riders.png");

export default function Page() {
  return (
    <div className="inf">
      <img src={Line} alt="" style={{ width: "100%" }} />
      <div className="page_containers">
        <div className="page_containers_items">
          <div className="p_items">
            <h3>
              Gana dinero con tu moto, bici o bici eléctrica. El horario lo
              pones tú.
            </h3>
            <img src={img1} alt="" />
          </div>
          <div className="p_items">
            <div className="check_conts">
              <h1>Qué necesitas</h1>

              <ul>
                <li>
                  <CheckCircleFilled
                    style={{
                      fontSize: 24,
                      color: "#90c33c",
                      marginRight: 20,
                      marginTop: 5,
                    }}
                  />
                  <p>Equipo de seguridad (p. ej., casco)</p>
                </li>
                <li>
                  <CheckCircleFilled
                    style={{
                      fontSize: 24,
                      color: "#90c33c",
                      marginRight: 20,
                      marginTop: 5,
                    }}
                  />
                  <p>Smartphone con iOS 11 / Android 6.0 o superior</p>
                </li>
                <li>
                  <CheckCircleFilled
                    style={{
                      fontSize: 24,
                      color: "#90c33c",
                      marginRight: 20,
                      marginTop: 5,
                    }}
                  />
                  <p>Permiso para trabajar por cuenta propia en España</p>
                </li>
                <li>
                  <CheckCircleFilled
                    style={{
                      fontSize: 24,
                      color: "#90c33c",
                      marginRight: 20,
                      marginTop: 5,
                    }}
                  />
                  <p>Alta en el régimen especial de trabajadores autónomos</p>
                </li>
                <li>
                  <CheckCircleFilled
                    style={{
                      fontSize: 24,
                      color: "#90c33c",
                      marginRight: 20,
                      marginTop: 5,
                    }}
                  />
                  <p>Ser mayor de edad</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
