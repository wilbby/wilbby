import React, { useEffect } from "react";
import Page1 from "./page1";
import Page2 from "./page2";
import Page4 from "./page3";
import Page5 from "./page4";
import QA from "./Preguntas";
import Form from "./Form/form";
import "./index.css";
import Apple from "../../Assets/images/apple.svg";
import Google from "../../Assets/images/google.png";
import Header from "../../Components/HeaderTransparent";
import GetApp from "../../Components/GetApp";

export default function Riders() {
  useEffect(() => {
    document.title = "Wilbby | Inscribete - reparte con Wilbby";
  }, []);

  return (
    <>
      <Header fromHome={true} />
      <div className="containers_rider">
        <div className="containers_rider_full">
          <div className="containers_rider_full_items">
            <div>
              <div>
                <h1 className="title">Gana dinero repartiendo en tu ciudad</h1>
                {/* <p className="subtitle">
                  Flexibilidad de horarios, ingresos competitivos y conoce tu
                  ciudad repartiendo al aire libre.
                </p> */}
                <ul className="tarifas">
                  <li>
                    <p>
                      <span>Tarifa base:</span> 2,80 €
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Pago por KM:</span> 0,40 € / km
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Tiempo de espera: </span>0,10 € / min (después de 8
                      minutos)
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Promociones económicas:</span> variable
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Dirección extra:</span> 1,90 €
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Pedido combinado:</span> 1,50 €
                    </p>
                  </li>
                  <li>
                    <p>
                      <span>Bonificación por lluvia:</span> + 40%
                    </p>
                  </li>
                </ul>
                <div className="apps_cont">
                  <a
                    href="https://apps.apple.com/es/app/wilbby-riders/id1538133349"
                    target="_black"
                  >
                    <img src={Apple} alt="Apple Store" />
                  </a>
                </div>
              </div>
            </div>
            <div className="fo">
              <Form />
            </div>
          </div>
        </div>
        <Page1 />
        <Page2 />
        <Page4 />
        <Page5 />
        <QA />
      </div>
    </>
  );
}
