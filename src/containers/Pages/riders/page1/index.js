import React from "react";

import "./index.css";

const img = require("./item.svg");
const img1 = require("./item1.svg");
const img2 = require("./item2.svg");

export default function Page() {
  return (
    <div className="container_page1">
      <h1 className="restaurant_title">Reparte cuando quieras</h1>
      <div className="page1_childrem">
        <div className="item">
          <div className="bg">
            <img src={img} alt="" />
          </div>
          <h3>Ingresos atractivos</h3>
          <p>
            Recibirás atractivos ingresos por cada pedido que entregues. También
            recibirás el 100% de las propinas de los clientes, que te ayudarán a
            alcanzar tus objetivos de ingresos.
          </p>
        </div>
        <div className="item">
          <div className="bg">
            <img src={img1} alt="" />
          </div>
          <h3>Colabora cuando quieras</h3>
          <p>
            Colabora con total libertad. Tú eliges cuándo te conectas y qué
            pedidos aceptas.
          </p>
        </div>
        <div className="item">
          <div className="bg">
            <img src={img2} alt="" />
          </div>
          <h3>Disfruta de tu ciudad</h3>
          <p>
            Entre la recogida y la entrega, nada se interpone entre tú y la
            carretera. Descubre tu ciudad.
          </p>
        </div>
      </div>
    </div>
  );
}
