import React from "react";
import "./index.css";

export default function ComoFunciona() {
  return (
    <div className="container_qa">
      <h1>¿Listo para comenzar?</h1>
      <p>Regístrate para hacer entregas</p>
      <a href="#unirme">Registrarse ahora</a>
    </div>
  );
}
