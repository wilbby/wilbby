import React, { useState, useEffect } from "react";
import { Button, Form, Input, Select, Checkbox } from "antd";
import "./form.css";
import animationData from "../../../Assets/Animate/success.json";
import Lottie from "react-lottie";
import { LOCAL_API_URL } from "../../../Utils/UrlConfig";
import { message } from "antd";

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: animationData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const { Option } = Select;

export default function Forms() {
  const [done, setDone] = useState(false);
  const [Loading, setLoading] = useState(false);
  const [municipios, setmunicipios] = useState([]);

  const onChange = async (values) => {
    setLoading(true);
    const data = { values };
    fetch(`${LOCAL_API_URL}/contact-for-rider`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then(async (res) => {
        const data = await res.json();
        if (data.success) {
          message.success(data.messages);
          setDone(true);
        } else {
          setLoading(false);
          message.warning("Alno va bien intentalo de nuevo");
        }
      })
      .catch((e) => {
        setLoading(false);
        setDone(false);
        message.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const getMuni = async () => {
    const res = await fetch(
      `https://raw.githubusercontent.com/IagoLast/pselect/master/data/provincias.json`
    );
    const response = await res.json();
    setmunicipios(response);
  };

  useEffect(() => {
    getMuni();
  }, []);

  return (
    <div className="form_content" id="unirme">
      {done ? (
        <div style={{ textAlign: "center" }}>
          <Lottie options={defaultOptions} height={200} width={200} />
          <h1 style={{ marginBottom: 30 }}>Solicitud enviada con éxito</h1>
          <p style={{ color: "gray" }}>
            Solicitud enviada con éxito en un plazo de 24 a 48H nos pondremos en
            contacto contigo para continuar con el proceso.
          </p>
          <Button
            type="primary"
            shape="round"
            onClick={() => setDone(false)}
            style={{ width: 200, height: 50 }}
            loading={false}
          >
            Volver al inicio
          </Button>
        </div>
      ) : (
        <>
          <h1>¡Empieza hoy mismo!</h1>
          <h2>Contacto</h2>
          <Form onFinish={(value) => onChange(value)}>
            <div style={{ display: "flex" }}>
              <Form.Item style={{ margin: 5, width: "50%" }} name="name">
                <Input placeholder="Nombre" required={true} />
              </Form.Item>

              <Form.Item style={{ margin: 5, width: "50%" }} name="lastName">
                <Input placeholder="Apellido(s)" required={true} />
              </Form.Item>
            </div>

            <div style={{ display: "flex", marginBottom: 20 }}>
              <Form.Item style={{ margin: 5, width: "50%" }} name="email">
                <Input placeholder="Correo electrónico" required={true} />
              </Form.Item>

              <Form.Item style={{ margin: 5, width: "50%" }} name="phone">
                <Input
                  placeholder="Telefono"
                  type="tel"
                  required={true}
                  defaultValue="+34"
                />
              </Form.Item>
            </div>
            <h2>¿Dónde quieres repartir?</h2>
            <Form.Item name="adress">
              <Input placeholder="Tu dirección" type="text" required={true} />
            </Form.Item>

            <div style={{ display: "flex", marginBottom: 20 }}>
              <Form.Item style={{ margin: 5, width: "50%" }} name="country">
                <Select allowClear placeholder="País" required={true}>
                  <Option value="España">España</Option>
                </Select>
              </Form.Item>

              <Form.Item style={{ margin: 5, width: "50%" }} name="city">
                <Select
                  allowClear
                  placeholder="Ciudad"
                  required={true}
                  showSearch
                >
                  {municipios.map((c, i) => {
                    return (
                      <Option value={c.nm} key={i}>
                        {c.nm}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </div>
            <Form.Item>
              <Checkbox required={true}>
                Si haces clic en «Enviar», confirmas haber leído y aceptado la{" "}
                <a
                  href="https://wilbby.com/politica-de-privacidad"
                  target="_black"
                >
                  Política de protección de datos de Wilbby
                </a>
                .{" "}
              </Checkbox>
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                shape="round"
                htmlType="submit"
                style={{ width: 200, height: 50 }}
                loading={Loading}
              >
                Enviar
              </Button>
            </Form.Item>
          </Form>
        </>
      )}
    </div>
  );
}
