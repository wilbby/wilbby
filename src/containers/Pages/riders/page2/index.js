import React from "react";
import "./index.css";
import Phone from "./phone.png";
import Phone1 from "./phone1.png";

export default function Sofware() {
  return (
    <div className="software_container">
      <h1 className="_title">Cómo funciona Wilbby</h1>
      <p className="_subtitle">
        Con la app de Wilbby Riders es bastante fácil gestionar tus pedidos
      </p>
      <div className="software_container_childrem">
        <div className="software_item">
          <div>
            <img src={Phone} alt="Wilbby" />
          </div>
          <div>
            <h1>Recibe pedidos en tu móvil</h1>
            <p>
              Desde tu app podrás conectarte siempre que estés en una zona
              Wilbby durante nuestro horario de actividad. Mientras permanezcas
              en línea, te iremos ofreciendo pedidos. En el mapa, podrás ver las
              zonas con más volumen de pedidos activos, lo que te ayudará a
              aprovechar al máximo el tiempo y aumentar tus ingresos. Puedes
              conectarte siempre que quieras, ¡tú decides!
            </p>
          </div>
          <div>
            <h1>Gestiona y procesa tu pedidos</h1>
            <p>
              Cada vez que salgas a repartir, estaremos a tu lado para ayudarte:
              tan sencillo como escribirnos a través de la app. Tu seguridad es
              nuestra prioridad, por eso tenemos un seguro para nuestros riders
              en caso de accidente. Es totalmente gratuito y te cubre desde el
              momento en el que te conectas.
            </p>
          </div>
          <div>
            <img src={Phone1} alt="Wilbby" />
          </div>
        </div>
      </div>
    </div>
  );
}
