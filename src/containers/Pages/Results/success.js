import React, { useEffect } from "react";
import { Result, Button } from "antd";
import { withRouter } from "react-router-dom";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import "./index.css";

function Success(props) {
  const { history } = props;
  const id = props.match.params.id;

  useEffect(() => {
    document.title = "Gracias por tu pedido";
  }, []);

  return (
    <>
      <GetApp />
      <Header />
      <div className="result_container">
        <Result
          status="success"
          title="Pedido realizado con éxito!"
          subTitle={`Pedido número: ${id.slice(
            0,
            8
          )} ha sido realizado éxitosamente te iremos informando sobre su progreso.`}
          extra={[
            <Button
              type="primary"
              key="console"
              shape="round"
              onClick={() => history.push("/profile")}
            >
              Ir al pedido
            </Button>,
          ]}
        />
      </div>
    </>
  );
}

export default withRouter(Success);
