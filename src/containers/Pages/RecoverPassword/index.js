import React, { useEffect } from "react";
import "./Forgot.css";
import NormalforgotForm from "./ForgotForm";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";

function ForgotClient() {
  useEffect(() => {
    document.title = "Recuperar contraseña";
  }, []);

  return (
    <>
      <GetApp />
      <Header />
      <div className="containers">
        <div className="Containerformm">
          <p>Olvidaste tu contraseña!</p>
          <NormalforgotForm />
        </div>
      </div>
    </>
  );
}

export default ForgotClient;
