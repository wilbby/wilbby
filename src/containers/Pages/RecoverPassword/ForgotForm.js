import React, { useState } from "react";
import { Form, Input, message, Spin } from "antd";
import "./Forgot.css";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { useHistory } from "react-router-dom";

const NormalforgotForm = () => {
  const [loading, setloading] = useState(false);
  const history = useHistory();

  return (
    <Form
      name="normal_login"
      className="login-form"
      initialValues={{ remember: true }}
      onFinish={(value) => {
        setloading(true);
        const handleSubmit = () => {
          const emails = value.email;
          if (emails) {
            fetch(`${LOCAL_API_URL}/forgotpassword?email=${emails}`)
              .then(async (res) => {
                const response = await res.json();
                if (response.success) {
                  setloading(false);
                  message.success(response.message);
                  history.push("/login");
                } else {
                  message.error(response.message);
                  setloading(false);
                }
              })
              .catch((err) => {
                console.log("err:", err);
                setloading(false);
              });
          }
        };
        handleSubmit();
        console.log("dome");
      }}
    >
      {loading ? (
        <Spin style={{ marginBottom: 20 }} />
      ) : (
        <Form.Item
          name="email"
          rules={[{ required: true, message: "Por favor ingresa tu email!" }]}
        >
          <Input placeholder="Email" className="form-control" type="email" />
        </Form.Item>
      )}

      <Form.Item>
        <button
          shape="round"
          type="primary"
          htmlType="submit"
          className="btn-res"
        >
          Enviar enlace
        </button>
      </Form.Item>

      <Form.Item style={{ paddingTop: 30 }}>
        ¿Tienes una cuenta?{" "}
        <a style={{ color: "#90C33C" }} href="/register">
          Regístrate ahora!
        </a>
      </Form.Item>
    </Form>
  );
};

export default NormalforgotForm;
