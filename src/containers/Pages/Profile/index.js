import React, { useState, useEffect } from "react";
import "./index.css";
import { Button, Modal, Avatar, Badge } from "antd";
import {
  UnorderedListOutlined,
  CreditCardOutlined,
  AlignLeftOutlined,
  CloseOutlined,
  GiftOutlined,
  LogoutOutlined,
  ExclamationCircleOutlined,
  EnvironmentOutlined,
  HeartOutlined,
  BellOutlined,
} from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import Logo from "../../Assets/images/logoblackprofile.svg";
import { Query } from "react-apollo";
import { Link } from "react-router-dom";
import {
  USER_DETAIL,
  GET_NOTIFICATION,
  RESTAURANT_FAVORITO,
} from "../../GraphQL/query";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import Pedidos from "./Pedidos/index";
import Wallet from "./wallet";
import MyAcconut from "./MyAccount";
import Direccion from "./Direccion";
import ComparteYGana from "./ComparteyGana";
import Notification from "./Notification";
import Favorito from "./Favoritos";

function Profile(props) {
  const { history } = props;
  const [menuShown, setMenuShown] = useState(false);
  const [active_index, setActive_index] = useState(1);
  const [latitude, setlatitude] = useState(null);
  const [longitude, setlongitude] = useState(null);

  const id = localStorage.getItem("id");
  const city = localStorage.getItem("city");

  function getGeoLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          setlatitude(position.coords.latitude);
          setlongitude(position.coords.longitude);
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  }

  useEffect(() => {
    document.title = "Mi cuenta";
    getGeoLocation();
  }, []);

  const changeTab = (index) => {
    setActive_index(index);
  };

  const toggleMenuMobile = () => {
    setMenuShown(!menuShown);
  };

  const cerrarSesionUsuario = () => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que deseas cerrar sesíon?",
      icon: <ExclamationCircleOutlined />,
      content: "Esperamos verte pronto de por Wilbby",
      okText: "Cerra sesión",
      cancelText: "Cancelar",
      onOk() {
        localStorage.removeItem("token");
        localStorage.removeItem("id");
        history.push("/login");
      },
      onCancel() {},
    });
  };

  return (
    <Query query={USER_DETAIL}>
      {({ data }) => {
        if (data) {
          const user = data.getUsuario.data;
          return (
            <div className="content main-wrapper">
              <div className={`topnav ${menuShown ? "visible-mobile" : ""}`}>
                <div className="toggle-nav" onClick={() => toggleMenuMobile()}>
                  {menuShown ? (
                    <CloseOutlined
                      style={{ fontSize: 28, color: "#95ca3e", paddingTop: 25 }}
                    />
                  ) : (
                    <AlignLeftOutlined
                      style={{ fontSize: 28, color: "#95ca3e", paddingTop: 25 }}
                    />
                  )}
                </div>
                <div className="nav-items" onClick={() => toggleMenuMobile()}>
                  <div
                    className={
                      active_index === 1000 ? "active simple" : "simple"
                    }
                  >
                    <Link to="/">
                      <img src={Logo} alt="Wilbby" className="logo_profile" />
                    </Link>
                  </div>
                  <Button
                    className={active_index === 0 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => changeTab(0)}
                  >
                    <img
                      src={IMAGES_PATH + user.avatar}
                      className="avatar_header"
                      style={{ width: 50, height: 50 }}
                    />
                    <h3 className="primary">MI CUENTA</h3>
                  </Button>
                  <Button
                    className={active_index === 1 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => changeTab(1)}
                  >
                    <UnorderedListOutlined />
                    <h3 className="primary">PEDIDOS</h3>
                  </Button>
                  <Button
                    className={active_index === 2 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => changeTab(2)}
                  >
                    <CreditCardOutlined />
                    <h3 className="primary">TARJETAS</h3>
                  </Button>
                  <Button
                    className={active_index === 4 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => changeTab(4)}
                  >
                    <EnvironmentOutlined />
                    <h3 className="primary">DIRECCIÓN</h3>
                  </Button>
                  <Button
                    className={active_index === 5 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => changeTab(5)}
                  >
                    <GiftOutlined />
                    <h3 className="primary">REGALOS</h3>
                  </Button>

                  <Button
                    className={active_index === 0 ? "active simple" : "simple"}
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                    onClick={() => cerrarSesionUsuario()}
                  >
                    <LogoutOutlined />
                    <h3 className="primary">CERRAR SESIÓN</h3>
                  </Button>
                </div>
              </div>
              <Query query={RESTAURANT_FAVORITO} variables={{ id: id }}>
                {(response) => {
                  if (response) {
                    let favoritosrefetch = response.refetch;
                    response.refetch();
                    let favoritos =
                      response &&
                      response.data &&
                      response.data.getRestaurantFavorito
                        ? response.data.getRestaurantFavorito.list
                        : [];
                    let favoritoscount = favoritos ? favoritos.length : 0;
                    return (
                      <Query query={GET_NOTIFICATION} variables={{ Id: id }}>
                        {(response) => {
                          if (response) {
                            response.refetch();
                            let notificaciones =
                              response &&
                              response.data &&
                              response.data.getNotifications
                                ? response.data.getNotifications.notifications
                                : [];
                            let contentnotiCount = notificaciones
                              ? notificaciones.length
                              : 0;
                            return (
                              <div className="page-content">
                                <div className="header_Profile">
                                  <div>
                                    <button onClick={() => changeTab(7)}>
                                      <Badge
                                        count={favoritoscount}
                                        overflowCount={9}
                                      >
                                        <HeartOutlined
                                          style={{ fontSize: 25 }}
                                        />
                                      </Badge>
                                    </button>
                                    <button onClick={() => changeTab(6)}>
                                      <Badge
                                        count={contentnotiCount}
                                        overflowCount={9}
                                      >
                                        <BellOutlined
                                          style={{ fontSize: 25 }}
                                        />
                                      </Badge>
                                    </button>
                                  </div>
                                </div>

                                {active_index === 0 ? (
                                  <MyAcconut user={user} />
                                ) : null}
                                {active_index === 1 ? (
                                  <Pedidos res={user} />
                                ) : null}
                                {active_index === 2 ? (
                                  <Wallet user={user} />
                                ) : null}
                                {active_index === 4 ? <Direccion /> : null}
                                {active_index === 5 ? <ComparteYGana /> : null}
                                {active_index === 6 ? (
                                  <Notification
                                    data={notificaciones}
                                    refetch={response.refetch}
                                  />
                                ) : null}
                                {active_index === 7 ? (
                                  <Favorito
                                    data={favoritos}
                                    refetch={favoritosrefetch}
                                    coordetate={latitude}
                                    coordenateone={longitude}
                                    city={city}
                                  />
                                ) : null}
                              </div>
                            );
                          }
                        }}
                      </Query>
                    );
                  }
                }}
              </Query>
            </div>
          );
        }
        return null;
      }}
    </Query>
  );
}

export default withRouter(Profile);
