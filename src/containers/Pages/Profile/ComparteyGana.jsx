import React from "react";
import Lottie from "react-lottie";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";
const nodata = require("../../Assets/Animate/gitf.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function ComparteyGana() {
  return (
    <div className="page-content_Items_description">
      <h1>Comparte con tus amigos</h1>
      <p>Comparte con tus amigos y gana dinero.</p>
      <div className="page-content_Items">
        <div style={{ textAlign: "center" }}>
          <Lottie options={defaultOptionsBell} height={180} width={180} />
          <h2>
            Comparte el código HOLAWILBBY y gana un 75% de descuento tu y tu
            amigo en su proximo pedido
          </h2>
          <span>
            Codígo promocional válido para un solo uso, ciertas restrinciones
            aplican.
          </span>
          <div style={{ marginTop: 50 }}>
            <FacebookShareButton
              url={"https://wilbby.com/"}
              title="Comparte el código HOLAWILBBY y gana 75% de descuento tu y tu amigo"
            >
              <FacebookIcon
                size={40}
                round={true}
                style={{ marginLeft: 10, outline: "none" }}
              />
            </FacebookShareButton>

            <TwitterShareButton
              url={"https://wilbby.com/"}
              title="Comparte el código HOLAWILBBY y gana 75% de descuento tu y tu amigo"
            >
              <TwitterIcon
                size={40}
                round={true}
                style={{ marginLeft: 10, outline: "none" }}
              />
            </TwitterShareButton>

            <LinkedinShareButton
              url={"https://wilbby.com/"}
              title="Comparte el código HOLAWILBBY y gana 75% de descuento tu y tu amigo"
            >
              <LinkedinIcon
                size={40}
                round={true}
                style={{ marginLeft: 10, outline: "none" }}
              />
            </LinkedinShareButton>

            <WhatsappShareButton
              url={"https://wilbby.com/"}
              title="Comparte el código HOLAWILBBY y gana 75% de descuento tu y tu amigo"
            >
              <WhatsappIcon
                size={40}
                round={true}
                style={{ marginLeft: 10, outline: "none" }}
              />
            </WhatsappShareButton>
          </div>
        </div>
      </div>
    </div>
  );
}
