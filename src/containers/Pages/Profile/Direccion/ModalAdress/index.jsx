import React, { useState, useEffect } from "react";
import GooglePlacesAutocomplete from "react-google-places-autocomplete";
import { useMutation } from "react-apollo";
import { CREATE_ADRESS } from "../../../../GraphQL/mutation";
import { Input, message } from "antd";
import Mapa from "../../../../Components/Maps";
import "./index.css";

const tipos = [
  {
    id: 1,
    cantidad: "Casa",
    type: "AntDesign",
    name: "home",
  },
  {
    id: 2,
    cantidad: "Trabajo",
    type: "MaterialCommunityIcons",
    name: "briefcase-outline",
  },
  {
    id: 3,
    cantidad: "Mi churri",
    type: "AntDesign",
    name: "hearto",
  },

  {
    id: 3,
    cantidad: "Otro",
    type: "AntDesign",
    name: "pushpino",
  },
];

export default function Direction(props) {
  const {
    setShoModaladress,
    setAdresActive,
    refetch,
    title,
    setVisible,
    setLoadin,
  } = props;
  const [valuesAdress, setValues] = useState(null);
  const [type, setType] = useState("Casa");
  const [latitude, setLatitude] = useState();
  const [longitude, setLongitude] = useState();
  const [piso, setpiso] = useState("");
  const [createAdress] = useMutation(CREATE_ADRESS);

  function getGeoLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          setLatitude(latitude);
          setLongitude(longitude);
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  }

  useEffect(() => {
    getGeoLocation();
  }, [latitude]);

  const createAdressFunc = () => {
    const input = {
      formatted_address: valuesAdress ? valuesAdress.label : "",
      usuario: localStorage.getItem("id"),
      type: type,
      puertaPiso: piso,
      lat: String(latitude),
      lgn: String(longitude),
    };
    createAdress({ variables: { input: input } }).then((res) => {
      if (res.data.createAdress.success) {
        setLoadin(false);
        refetch();
        localStorage.setItem(
          "adressName",
          res.data.createAdress.data.formatted_address
        );
        localStorage.setItem("adressId", res.data.createAdress.data.id);
        if (setShoModaladress) {
          setShoModaladress(false);
        }

        if (setVisible) {
          setVisible();
        }
        if (setAdresActive) {
          setAdresActive();
        }
        message.success("Direción añadida con éxito");
      } else {
        setLoadin(false);
        message.error("Algo va mal intentalo de nuevo");
      }
    });
  };
  return (
    <div className="add_adres_cont">
      <h1>{title}</h1>
      <div className="add_adres_cont_grid">
        <div className="add_adres_cont_input">
          <GooglePlacesAutocomplete
            apiKey="AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA"
            selectProps={{
              placeholder: "Buscar calle, ciudad o distrito",
              valuesAdress,
              onChange: setValues,
            }}
          />
          <div className="cont_types">
            {tipos.map((t, i) => {
              return (
                <button
                  key={i}
                  onClick={() => setType(t.cantidad)}
                  className={
                    type === t.cantidad ? "itemsTipoActivo" : "itemsTipo"
                  }
                >
                  {t.cantidad}
                </button>
              );
            })}
          </div>

          <Input
            placeholder="Piso, puerta, indicaciones"
            style={{ height: 40, marginBottom: 20 }}
            onChange={(e) => setpiso(e.target.value)}
          />

          <button
            className="btn-btn-primary"
            onClick={() => createAdressFunc()}
          >
            Confirmar dirección
          </button>
        </div>
        <div className="add_adres_cont_map">
          <Mapa
            lat={latitude}
            lgn={longitude}
            width="80%"
            height={350}
            title="Burgos"
          />
        </div>
      </div>
    </div>
  );
}
