import React, { useState } from "react";
import { withRouter } from "react-router-dom";
import { message, Modal, Spin } from "antd";
import {
  ExclamationCircleOutlined,
  DeleteOutlined,
  CheckCircleOutlined,
  PlusCircleOutlined,
} from "@ant-design/icons";
import { useMutation, useQuery } from "react-apollo";
import { EDIT_ADRESS } from "../../../GraphQL/mutation";
import "./index.css";
import { GET_ADRESS } from "../../../GraphQL/query";
import AddAdress from "./ModalAdress";
import Lottie from "react-lottie";
const nodata = require("../../../Assets/Animate/1342-location.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

function Direcicon(props) {
  const { setShoModaladress, setAdresActive } = props;
  const [loadin, setLoadin] = useState(false);
  const [visible, setVisible] = useState(false);
  const [actualizarAdress] = useMutation(EDIT_ADRESS);
  const { data, refetch } = useQuery(GET_ADRESS, {
    variables: { id: localStorage.getItem("id") },
  });
  const adress = data && data.getAdress ? data.getAdress.data : [];

  const Eliminardireccion = (id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar tu dirección?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Las direcciones son impresindible para realizar las entregas recuerda que puedes actualizarla.",
      cancelText: "Cancelar",
      okText: "Eliminar",
      onOk() {
        setLoadin(true);

        actualizarAdress({
          variables: {
            input: {
              id: id,
              usuario: null,
            },
          },
        })
          .then((res) => {
            if (res.data.actualizarAdress.success) {
              localStorage.removeItem("adressName");
              localStorage.removeItem("adressId");
              setLoadin(false);
              refetch();
              if (setShoModaladress) {
                setShoModaladress(false);
              }
              message.success("Direción eliminada con éxito");
              if (setAdresActive) {
                setAdresActive();
              }
            } else {
              setLoadin(false);
              message.error("Algo va mal intentalo de nuevo");
            }
          })

          .catch((e) => {
            console.log(e);
            setLoadin(false);
          });
      },
      onCancel() {},
    });
  };

  const confirmAdress = (sdress, id) => {
    localStorage.setItem("adressName", sdress);
    localStorage.setItem("adressId", id);
    message.success("Dirección marcada como predeterminada");
    if (setShoModaladress) {
      setShoModaladress(false);
    }

    if (setAdresActive) {
      setAdresActive();
    }
  };
  return (
    <div className="page-content_Items_description">
      <h1>Direcciones</h1>
      <p>Añade y elimina tu direcciones</p>
      <div className="page-content_Items">
        <>
          {loadin ? (
            <div style={{ textAlign: "center" }}>
              <Spin size="large" />
            </div>
          ) : (
            <>
              <div>
                <div className="add_card">
                  <h3>Añadir dirección</h3>
                  <PlusCircleOutlined
                    onClick={() => setVisible(true)}
                    style={{
                      marginLeft: "auto",
                      color: "#90C33C",
                      fontSize: 28,
                      cursor: "pointer",
                    }}
                  />
                  <Modal
                    title={null}
                    centered
                    visible={visible}
                    footer={false}
                    onCancel={() => setVisible(false)}
                    width="80%"
                  >
                    <AddAdress
                      setShoModaladress={setShoModaladress}
                      setAdresActive={setAdresActive}
                      refetch={refetch}
                      title="Añadir una dirección de entrega"
                      setVisible={setVisible}
                      setLoadin={setLoadin}
                    />
                  </Modal>
                </div>
                <>
                  {adress.length === 0 ? (
                    <div style={{ textAlign: "center" }}>
                      <Lottie
                        options={defaultOptionsBell}
                        height={180}
                        width={180}
                      />
                      <span>Aún no sabemos donde llevarte tus pedidos</span>
                    </div>
                  ) : (
                    <>
                      {adress.map((d, i) => {
                        return (
                          <div
                            key={i}
                            className="item_direccion"
                            onClick={() =>
                              confirmAdress(d.formatted_address, d.id)
                            }
                          >
                            <div>
                              <h3>{d.formatted_address}</h3>
                              <p>{d.type}</p>
                              {localStorage.getItem("adressId") == d.id ? (
                                <span style={{ color: "#90C33C" }}>
                                  Preferida
                                </span>
                              ) : null}
                            </div>
                            <div
                              style={{ marginLeft: "auto", display: "flex" }}
                            >
                              {localStorage.getItem("adressId") == d.id ? (
                                <CheckCircleOutlined
                                  style={{
                                    fontSize: 20,
                                    marginRight: 20,
                                    color: "#90C33C",
                                    cursor: "pointer",
                                  }}
                                />
                              ) : null}
                              <DeleteOutlined
                                style={{
                                  fontSize: 20,
                                  color: "#F5365C",
                                  cursor: "pointer",
                                }}
                                onClick={() => Eliminardireccion(d.id)}
                              />
                            </div>
                          </div>
                        );
                      })}
                    </>
                  )}
                </>
              </div>
            </>
          )}
        </>
      </div>
    </div>
  );
}

export default withRouter(Direcicon);
