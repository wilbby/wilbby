import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  Form,
  Input,
  message,
  Avatar,
  Tooltip,
  Upload,
  Modal,
  Spin,
  Menu,
  Dropdown,
} from "antd";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import {
  PlusCircleOutlined,
  ExclamationCircleOutlined,
  MoreOutlined,
} from "@ant-design/icons";
import { Mutation, useMutation } from "react-apollo";
import {
  UPLOAD_FILE,
  ACTUALIZAR_USUARIO,
  ELIMINAR_USUARIO,
} from "../../../GraphQL/mutation";
import "./index.css";

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
}

function MyAccount(props) {
  const { user, history } = props;
  const [city] = useState(user.city ? user.city : localStorage.getItem("city"));
  const [avatar, setAvatar] = useState(user.avatar);
  const [loadin, setLoadin] = useState(false);
  const [actualizarUsuario] = useMutation(ACTUALIZAR_USUARIO);
  const [eliminarUsuario] = useMutation(ELIMINAR_USUARIO);

  const chagePhone = () => {
    window.location.href = `/verify-phone/${user._id}`;
  };

  const [form] = Form.useForm();

  const uploadButton = (
    <Tooltip title="Añadir foto del perfil">
      <div>
        <PlusCircleOutlined />
        <div className="ant-upload-text">Añadir foto del perfil</div>
      </div>
    </Tooltip>
  );

  const menu = (
    <Menu>
      <Menu.Item>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="mailto:info@wilbby.com"
        >
          Ayuda
        </a>
      </Menu.Item>

      <Menu.Item danger onClick={() => elimimarAccount(user._id)}>
        Eliminar cuenta
      </Menu.Item>
    </Menu>
  );

  const elimimarAccount = (id) => {
    const { confirm } = Modal;
    confirm({
      title: "¿Estás seguro que desea eliminar tu cuenta?",
      icon: <ExclamationCircleOutlined />,
      content:
        "Al eliminar tu cuenta se borran todos los datos de la misma como tarjetas de crédito pedidos y datos personales.",
      cancelText: "Cancelar",
      okText: "Eliminar cuenta",
      onOk() {
        eliminarCuenta(id);
      },
      onCancel() {},
    });
  };

  const eliminarCuenta = (id) => {
    eliminarUsuario({ variables: { id: id } })
      .then((res) => {
        if (res.data.eliminarUsuario.success) {
          message.success("Cuenta eliminada con éxito");
          localStorage.removeItem("id");
          localStorage.removeItem("token");
          history.push("/login");
        } else {
          message.error("Algo salió mal vuelve a intentaorlo por favor");
        }
      })
      .catch((err) => {
        console.log(err);
        message.error("Algo salió mal vuelve a intentaorlo por favor");
      });
  };

  return (
    <div className="page-content_Items_description">
      <h1>Tu Cuenta</h1>
      <p>Aquí podrás ver y editar los datos de tu perfil</p>
      <div className="page-content_Items">
        <>
          {loadin ? (
            <div style={{ textAlign: "center" }}>
              <Spin size="large" />
            </div>
          ) : (
            <div className="prof_cont">
              <div className="prof_cont_avatar">
                <h3>Foto de perfil</h3>
                <Mutation mutation={UPLOAD_FILE}>
                  {(singleUpload) => (
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      customRequest={async (data) => {
                        let imgBlob = await getBase64(data.file);
                        singleUpload({ variables: { imgBlob } })
                          .then((res) => {
                            setAvatar(
                              res && res.data && res.data.singleUpload
                                ? res.data.singleUpload.filename
                                : ""
                            );
                          })
                          .catch((error) => {
                            console.log("fs error: ", error);
                          });
                      }}
                    >
                      {avatar ? (
                        <Tooltip title="Actualizar foto del perfil">
                          <Avatar
                            src={IMAGES_PATH + avatar}
                            size={95}
                            onClick={() => console.log("hola")}
                          />
                        </Tooltip>
                      ) : null}

                      {!avatar ? uploadButton : null}
                    </Upload>
                  )}
                </Mutation>
              </div>
              <div className="prof_cont_form">
                <h3>Detalles De La Cuenta</h3>
                <div
                  style={{
                    marginLeft: "95%",
                    marginTop: -30,
                    marginBottom: 30,
                  }}
                >
                  <Dropdown overlay={menu}>
                    <MoreOutlined
                      style={{
                        marginLeft: "auto",
                        fontSize: 22,
                        cursor: "pointer",
                      }}
                      onClick={(e) => e.preventDefault()}
                    />
                  </Dropdown>
                </div>
                <Form
                  form={form}
                  name="user"
                  onFinish={(values) => {
                    setLoadin(true);
                    const input = {
                      _id: user._id,
                      name: values.name ? values.name : user.name,
                      lastName: values.lastName
                        ? values.lastName
                        : user.lastName,
                      email: values.email ? values.email : user.email,
                      city: city,
                      avatar: avatar,
                    };

                    actualizarUsuario({ variables: { input: input } })
                      .then((res) => {
                        if (res.data.actualizarUsuario) {
                          message.success("Datos actualizo con éxito");
                        }
                      })
                      .catch((err) => {
                        message.error(
                          "Algo salió mal vuelve a intentaorlo por favor"
                        );
                      })
                      .finally(() => setLoadin(false));
                  }}
                  scrollToFirstError
                >
                  <Form.Item name="name">
                    <Input
                      placeholder="Nombre"
                      className="form-control"
                      defaultValue={user.name}
                    />
                  </Form.Item>
                  <Form.Item name="lastName">
                    <Input
                      placeholder="Apellidos"
                      className="form-control"
                      defaultValue={user.lastName}
                    />
                  </Form.Item>
                  <Form.Item name="email">
                    <Input
                      placeholder="Email"
                      className="form-control"
                      defaultValue={user.email}
                    />
                  </Form.Item>
                  <Form.Item name="city">
                    <Input
                      placeholder="Ciudad"
                      className="form-control"
                      defaultValue={city}
                    />
                  </Form.Item>
                  <Form.Item name="telefono">
                    <Input
                      placeholder="Teléfono"
                      className="form-control"
                      defaultValue={user.telefono}
                      onFocus={() => chagePhone()}
                    />
                  </Form.Item>
                  <Form.Item>
                    <button type="submit" className="btn-btn-primary">
                      Guardar cambios
                    </button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          )}
        </>
      </div>
    </div>
  );
}

export default withRouter(MyAccount);
