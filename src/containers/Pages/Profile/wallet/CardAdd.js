import React, { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { LOCAL_API_URL } from "../../../Utils/UrlConfig";
import { message, Spin } from "antd";

import "./index.css";

const CheckoutForm = (props) => {
  const { user, setShow, getCard } = props;
  const [loading, setloading] = useState(false);
  const stripe = useStripe();
  const elements = useElements();

  const handleSubmit = async (event) => {
    setloading(true);
    // Block native form submission.
    event.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);

    // Use your card Element with other Stripe.js APIs
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: "card",
      card: cardElement,
    });

    if (error) {
      console.log("[error]", error);
    } else {
      const bod = {
        customer: user.StripeID,
        paymentMethod,
      };
      const respuesta = await fetch(`${LOCAL_API_URL}/create-card`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(bod),
      });

      const done = await respuesta.json();

      if (done.success) {
        message.success(done.data);
        getCard();
        setShow(false);
        setloading(false);
      } else {
        message.error(done.data);
        setloading(false);
      }
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <CardElement />
      <button type="submit" disabled={!stripe} className="stripebtn">
        {loading ? <Spin size="small" /> : "Añadir tarjeta"}
      </button>
    </form>
  );
};

export default CheckoutForm;
