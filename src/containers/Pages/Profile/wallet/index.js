import React, { useState, useEffect } from "react";
import { LOCAL_API_URL } from "../../../Utils/UrlConfig";
import "./index.css";
import { message, Popconfirm, Modal } from "antd";
import { DeleteOutlined, PlusCircleOutlined } from "@ant-design/icons";
import CheckoutForm from "./CardAdd";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Lottie from "react-lottie";
const nodata = require("../../../Assets/Animate/27630-credit-card-blue.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const visa = require("../../../Assets/images/visa.jpg");
const mastercar = require("../../../Assets/images/master.png");
const americam = require("../../../Assets/images/americam.png");
const orther = require("../../../Assets/images/Card3.png");
const Nodata = require("../../../Assets/images/nocard.svg");

export default function Wallet(props) {
  const [cards, setcards] = useState(null);
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);

  const showModal = () => {
    setShow(true);
  };

  const hidenModal = () => {
    setShow(false);
  };

  const { user, hidenModals, setfromPaypal } = props;

  const getCard = async () => {
    setLoading(true);
    let res = await fetch(
      `${LOCAL_API_URL}/get-card?customers=${user.StripeID}`
    );
    const card = await res.json();
    setcards(card);
    if (res) {
      setLoading(false);
    } else {
      setLoading(false);
    }
  };

  useEffect(() => {
    getCard();
  }, []);

  const createclient = async (id, name, lastName, email) => {
    let res = await fetch(
      `${LOCAL_API_URL}/create-client?userID=${id}&nameclient=${
        name + lastName
      }&email=${email}`
    );
    const create = await res.json();
    console.log(create);
  };

  if (user.StripeID === null) {
    createclient(user._id, user.name, user.lastName, user.email);
    setLoading(false);
  }

  const deleteCard = async (StripeID, id) => {
    let res = await fetch(
      `${LOCAL_API_URL}/delete-card-web?customers=${StripeID}&cardID=${id}`
    );
    const deletec = await res.json();
    if (deletec ? deletec : false) {
      getCard();
      console.log("Tarjeta eliminada con éxito");
      message.success("Tarjeta eliminada con éxito");
    } else {
      message.error("Algo salio mal, intenlalo de nuevo por favor");
    }
  };

  const tarjetes = cards ? cards.data : [];

  const defaultCard = (last4, brand, token, customer) => {
    localStorage.setItem("cardLast4", last4);
    localStorage.setItem("brand", brand);
    localStorage.setItem("tokenCard", token);
    localStorage.setItem("customer", customer);

    message.success("Tarjeta marcada como princial");

    if (hidenModals) {
      hidenModals(false);
    }

    if (setfromPaypal) {
      setfromPaypal(false);
    }
  };
  return (
    <div className="page-content_Items_description">
      <h1>Tarjetas Guardadas</h1>
      <p>Aquí podrás gestionar tus métodos de pago.</p>
      <div className="page-content_Items">
        <>
          <div className="add_card">
            <h3>Añadir tarjeta</h3>
            <PlusCircleOutlined
              onClick={() => showModal()}
              style={{
                marginLeft: "auto",
                color: "#90C33C",
                fontSize: 28,
                cursor: "pointer",
              }}
            />
            <Modal
              title="Añadir tarjeta al wallet"
              visible={show}
              footer={false}
              onCancel={hidenModal}
            >
              <div className="conten-anadir">
                <CheckoutForm user={user} setShow={setShow} getCard={getCard} />
              </div>
            </Modal>
          </div>
          {tarjetes.length === 0 ? (
            <div style={{ textAlign: "center" }}>
              <Lottie options={defaultOptionsBell} height={180} width={180} />
              <p style={{ marginTop: 20 }}>
                Aun no has añadido un metódo de pago
              </p>
            </div>
          ) : null}

          {tarjetes.map((ca, i) => {
            let images = "";
            let brand = "";
            switch (ca.card.brand) {
              case "visa":
                images = visa;
                brand = "Visa";
                break;
              case "mastercard":
                images = mastercar;
                brand = "Master Card";
                break;
              case "amex":
                images = americam;
                brand = "American Express";
                break;
              default:
                images = orther;
                brand = "Mi tarjeta";
            }
            return (
              <div
                className="tarjeta"
                key={i}
                onClick={() =>
                  defaultCard(ca.card.last4, ca.card.brand, ca.id, ca.customer)
                }
              >
                <img src={images} alt="Card" />
                <div>
                  <h3>{brand}</h3>
                  <p>
                    Termina en {ca.card.last4} - Caduca {ca.card.exp_month} /{" "}
                    {ca.card.exp_year}
                  </p>
                  {ca.id == localStorage.getItem("tokenCard") ? (
                    <span style={{ color: "#90C33C" }}>Principal</span>
                  ) : null}
                </div>
                <Popconfirm
                  title="¿Estás seguro que deseas eliminar la tarjeta?"
                  onConfirm={() => deleteCard(ca.customer, ca.id)}
                  onCancel={() => console.log("cancelled")}
                  okText="Eliminar"
                  cancelText="Cancelar"
                >
                  <DeleteOutlined
                    style={{
                      marginLeft: "auto",
                      color: "#F5365C",
                      fontSize: 20,
                      cursor: "pointer",
                    }}
                  />
                </Popconfirm>
              </div>
            );
          })}
        </>
      </div>
    </div>
  );
}
