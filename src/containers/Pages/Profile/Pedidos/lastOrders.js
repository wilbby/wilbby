import React, { useState } from "react";
import { Query, useMutation } from "react-apollo";
import { GET_ALL_ORDEN } from "../../../GraphQL/query";
import {
  CREATE_OPINIO,
  CREAR_VALORACIONES,
  ORDER_PROCEED,
  ACTUALIZAR_RIDER,
} from "../../../GraphQL/mutation";
import { Modal, Rate, Input, Spin, message } from "antd";
import moment from "moment";
import { withRouter } from "react-router-dom";
import { RatingCalculator } from "../../../Utils/calculateRating";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import "./index.css";
import Lottie from "react-lottie";
import { formaterPrice } from "../../../Utils/formatePrice";
const nodata = require("../../Home/chica.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const { TextArea } = Input;

function Order(props) {
  const [visible, setVisible] = useState(false);
  const [dataOrder, setDataOrder] = useState(null);
  const [modalOpina, setModalOpina] = useState(false);
  const [dataOpina, setdataOpina] = useState(null);
  const [modalvalorar, setmodalvalorar] = useState(false);
  const [rate, setRate] = useState(0);
  const [rateRider, setRateRider] = useState(0);
  const [rateValorar, setRateValorar] = useState(0);
  const [opinion, setOpinion] = useState("");
  const [comment, setComment] = useState("");
  const [loadinOpi, setLoadinOpi] = useState(false);
  const [loadinVal, setLoadinVal] = useState(false);
  const [createOpinion] = useMutation(CREATE_OPINIO);
  const [crearValoracion] = useMutation(CREAR_VALORACIONES);
  const [OrdenProceed] = useMutation(ORDER_PROCEED);
  const [actualizarRiders] = useMutation(ACTUALIZAR_RIDER);
  const { res } = props;

  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  const desc = ["Terrible", "Malo", "Normal", "Buena", "Estupendo"];

  const handleChange = (value) => {
    setRate(value);
  };

  const handleChangeRider = (value) => {
    setRateRider(value);
  };

  const handleChangeValorar = (value) => {
    setRateValorar(value);
  };

  ////////////Detalles

  const showModal = (datas) => {
    setVisible(true);
    setDataOrder(datas);
  };

  const handleCancel = () => {
    setVisible(false);
    setDataOrder(null);
  };

  /////// Opinion

  const showModalOpina = (data) => {
    setModalOpina(true);
    setdataOpina(data);
  };

  const handleCancelOpina = () => {
    setModalOpina(false);
    setdataOpina(null);
  };

  ////////valorar

  const showModalValora = () => {
    setmodalvalorar(true);
  };

  const handleCancelValorar = () => {
    setmodalvalorar(false);
  };

  const valorarRestaurant = (id) => {
    setLoadinVal(true);
    if (rateValorar === 0 && !comment && rateRider === 0) {
      message.warning(
        "Debes añadir una puntuación y un comentario para continuar"
      );
      setLoadinVal(false);
    } else {
      const input = {
        user: res._id,
        comment: comment,
        value: rateValorar,
        restaurant: id,
      };

      crearValoracion({ variables: { input: input } })
        .then((res) => {
          if (res.data.crearValoracion) {
            message.success("Tu valoración ha sido añadida con éxito");
            handleCancelValorar();
          }
        })
        .catch((error) => {
          console.log(error);
          message.error("Hubo un error intentalo de nuevo por favor");
        })
        .finally(() => setLoadinVal(false));
    }
  };

  const opinarPlato = (id) => {
    setLoadinOpi(true);
    if (rate === 0 && opinion === "") {
      message.warning(
        "Debes añadir una puntuación y un comentario para continuar"
      );
      setLoadinOpi(false);
    } else {
      const input = {
        plato: id,
        comment: opinion,
        rating: rate,
        user: res._id,
      };
      createOpinion({ variables: { input: input } })
        .then((res) => {
          if (res.data.createOpinion.success) {
            message.success("Tu opinión ha sido añadida con éxito");
            handleCancelOpina();
          }
        })
        .catch((error) => {
          console.log(error);
          message.error("Hubo un error intentalo de nuevo por favor");
        })
        .finally(() => setLoadinOpi(false));
    }
  };

  const ordersProcess = (orden, estado, progreso, status) => {
    OrdenProceed({
      variables: {
        orden: orden,
        estado: estado,
        progreso: progreso,
        status: status,
      },
    })
      .then((res) => {
        if (res.data.OrdenProceed.success) {
          console.log(res);
        } else {
        }
      })
      .catch((e) => console.log(e));
  };

  const valorateRIder = (id, rating) => {
    const input = {
      _id: id,
      rating: rating.concat(rateRider),
    };
    actualizarRiders({ variables: { input: input } });
  };

  return (
    <Query query={GET_ALL_ORDEN} variables={{ usuarios: res._id }}>
      {(response) => {
        if (response.error) {
          return console.log(response.error);
        }
        if (response.loading) {
          return (
            <div style={{ textAlign: "center" }}>
              <Spin size="large" />
            </div>
          );
        }
        if (response) {
          const datos =
            response && response.data
              ? response.data.getOrderByUsuarioAll
              : response.data.getOrderByUsuarioAll.list;
          return (
            <>
              {datos.list.length === 0 ? (
                <div style={{ textAlign: "center" }}>
                  <Lottie
                    options={defaultOptionsBell}
                    height={180}
                    width={180}
                  />
                  <p style={{ marginTop: 20 }}>
                    Aun no has hecho nuevos pedidos
                  </p>
                </div>
              ) : (
                <>
                  {datos.list.map((da, i) => {
                    return (
                      <div
                        onClick={() => showModal(da)}
                        className={
                          da.status === "active"
                            ? "card_order_active"
                            : "card_order"
                        }
                        key={i}
                      >
                        {da.restaurants.image ? (
                          <img
                            src={da.restaurants.image}
                            alt={da.restaurants.title}
                            className="img__orden"
                          />
                        ) : null}

                        <div className="info_order__column">
                          <h1>{da.restaurants.title}</h1>
                          <div className="info_order">
                            {da.platos.map((e, i) => {
                              return (
                                <div key={i}>
                                  <h4>
                                    {" "}
                                    <span>{e.plato.cant} × </span>
                                    {e.plato.title}
                                  </h4>
                                  {e.complementos.map((com, i) => {
                                    return (
                                      <h6 key={i}>
                                        <span style={{ color: "#90c33c" }}>
                                          -
                                        </span>{" "}
                                        {com.name}
                                      </h6>
                                    );
                                  })}
                                </div>
                              );
                            })}
                          </div>
                          <div
                            style={{
                              marginLeft: 15,
                            }}
                          >
                            <h4 style={{ fontWeight: "bold" }}>
                              {formaterPrice(da.total, language, currency)}
                            </h4>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </>
              )}

              {dataOrder ? (
                <Modal
                  footer={false}
                  title={null}
                  visible={visible}
                  onCancel={handleCancel}
                >
                  {dataOrder.restaurants.image ? (
                    <img
                      src={dataOrder.restaurants.image}
                      alt={dataOrder.restaurants.title}
                      className="img__details"
                    />
                  ) : null}

                  <div className="conten-modal-details">
                    <div className="titulos">
                      <h3>{dataOrder.restaurants.title}</h3>
                      <p>{dataOrder.AdresStore.ciudad}</p>
                    </div>

                    <div className="estado">
                      <h3>{dataOrder.estado}</h3>
                      <p>{moment(dataOrder.created_at).format("lll")}</p>
                      <span style={{ marginTop: -10 }}>
                        {" "}
                        ID de Wilbby {dataOrder.display_id}
                      </span>
                      {dataOrder.invoiceUrl ? (
                        <a
                          href={dataOrder.invoiceUrl}
                          target="_black"
                          style={{ marginLeft: 20 }}
                        >
                          Ver factura
                        </a>
                      ) : null}
                    </div>

                    {dataOrder.schedule ? (
                      <div className="titulos">
                        <h3>Pedido programado</h3>
                        <p>{moment(dataOrder.time).format("lll")}</p>
                      </div>
                    ) : (
                      <div className="titulos">
                        <h3>Entrega</h3>
                        <p>
                          {moment(dataOrder.created_at)
                            .add(0.5, "hours")
                            .format("lll")}
                        </p>
                      </div>
                    )}

                    <div className="titulos">
                      <h3>{dataOrder.restaurants.title}</h3>
                      <p>
                        {dataOrder.restaurants.adress.calle},{" "}
                        {dataOrder.restaurants.adress.numero},{" "}
                        {dataOrder.restaurants.adress.codigoPostal}
                      </p>
                      <span style={{ color: "gray" }}>
                        {dataOrder.restaurants.adress.ciudad}
                      </span>
                    </div>

                    {!dataOrder.llevar ? (
                      <div className="titulos">
                        <h3>Dirección</h3>
                        <p>{dataOrder.Adress.formatted_address}</p>
                        <span style={{ color: "gray" }}>
                          {dataOrder.Adress.type}
                        </span>
                      </div>
                    ) : null}

                    {!dataOrder.llevar ? (
                      <>
                        {dataOrder.Riders._id ? (
                          <div className="titulos">
                            <h3>Riders</h3>
                            <p>
                              {dataOrder.Riders.name}{" "}
                              {dataOrder.Riders.lastName}
                            </p>
                            <Rate
                              style={{ paddingTop: -15 }}
                              disabled
                              allowHalf
                              defaultValue={RatingCalculator(
                                dataOrder.Riders.rating
                              )}
                            />
                          </div>
                        ) : (
                          <div className="titulos">
                            <h3>Riders</h3>
                            <p>Sin asignar</p>
                          </div>
                        )}
                      </>
                    ) : null}

                    {dataOrder.platos.map((h, i) => {
                      return (
                        <div className="card_order" key={i}>
                          {h.plato.imagen ? (
                            <img
                              src={h.plato.imagen}
                              alt="hola"
                              className="img__orden"
                            />
                          ) : null}

                          <div className="info_order">
                            <div key={i}>
                              <h4>
                                {" "}
                                <span>{h.plato.cant} × </span>
                                {h.plato.title}
                              </h4>
                              <h6>{h.plato.ingredientes}</h6>
                              {h.complementos.map((com, i) => {
                                return (
                                  <h6 key={i}>
                                    <span style={{ color: "#90c33c" }}>-</span>{" "}
                                    {com.name}
                                  </h6>
                                );
                              })}
                              <p>
                                {formaterPrice(
                                  h.plato.price,
                                  language,
                                  currency
                                )}
                              </p>
                            </div>
                            <button
                              style={{
                                minWidth: 120,
                                borderRadius: 100,
                                backgroundColor: "#90c33c",
                                color: "white",
                              }}
                              onClick={() => showModalOpina(h)}
                            >
                              Opinar
                            </button>
                          </div>
                        </div>
                      );
                    })}

                    {/* Modal para Opinio */}

                    {dataOpina ? (
                      <Modal
                        footer={false}
                        title={dataOpina.plato.title}
                        visible={modalOpina}
                        onCancel={handleCancelOpina}
                      >
                        {dataOpina.plato.imagen ? (
                          <img src={dataOpina.plato.imagen} className="im" />
                        ) : null}

                        <div style={{ marginTop: 30 }}>
                          <div className="_plato">
                            {dataOpina.plato.imagen ? (
                              <img
                                src={dataOpina.plato.imagen}
                                alt={dataOpina.plato.title}
                              />
                            ) : null}

                            <div className="_ingredientes">
                              <h2>{dataOpina.plato.title}</h2>
                              <p>{dataOpina.plato.ingredientes}</p>
                            </div>
                          </div>

                          <div
                            style={{
                              padding: 20,
                              justifyContent: "center",
                              alignItems: "center",
                            }}
                          >
                            <h3>¿Qué te parecio {dataOpina.plato.title}?</h3>
                            <p>
                              Opina sobre este producto y ayuda a otros clientes
                              como tu a decidir mejor
                            </p>
                            <span style={{ marginBottom: 20 }}>
                              <Rate
                                tooltips={desc}
                                onChange={handleChange}
                                value={rate}
                                style={{ fontSize: 24 }}
                              />
                              {rate ? (
                                <span className="ant-rate-text">
                                  {desc[rate - 1]}
                                </span>
                              ) : (
                                ""
                              )}
                            </span>
                            <TextArea
                              rows={4}
                              onChange={(e) => setOpinion(e.target.value)}
                              style={{ marginBottom: 30 }}
                            />
                            <button
                              className="dejar_op"
                              onClick={() => opinarPlato(dataOpina.plato._id)}
                            >
                              {loadinOpi ? (
                                <Spin size="small" />
                              ) : (
                                "Dejar opinión"
                              )}
                            </button>
                          </div>
                        </div>
                      </Modal>
                    ) : null}

                    {/* Modal para opinion fin */}

                    {/* Modal para valoracion */}

                    <Modal
                      footer={false}
                      title={dataOrder.restaurants.title}
                      visible={modalvalorar}
                      onCancel={handleCancelValorar}
                    >
                      {dataOrder.restaurants.image ? (
                        <img
                          src={dataOrder.restaurants.image}
                          alt={dataOrder.restaurants.title}
                          className="im"
                        />
                      ) : null}

                      <div style={{ marginTop: 30 }}>
                        <div className="_plato">
                          {dataOrder.restaurants.logo ? (
                            <img
                              src={dataOrder.restaurants.logo}
                              alt={dataOrder.restaurants.title}
                            />
                          ) : null}

                          <div className="_ingredientes">
                            <h2>{dataOrder.restaurants.title}</h2>
                            <p>{dataOrder.restaurants.categoryName}</p>
                          </div>
                        </div>

                        {!dataOrder.llevar ? (
                          <div className="_plato">
                            {dataOrder.Riders.avatar ? (
                              <img
                                src={IMAGES_PATH + dataOrder.Riders.avatar}
                                alt={dataOrder.Riders.name}
                              />
                            ) : null}

                            <div className="_ingredientes">
                              <h2>
                                {dataOrder.Riders.name}{" "}
                                {dataOrder.Riders.lastName}
                              </h2>
                              <p>Rider</p>
                              <span style={{ marginBottom: 20 }}>
                                <Rate
                                  tooltips={desc}
                                  onChange={handleChangeRider}
                                  value={rateRider}
                                  style={{ fontSize: 24 }}
                                />
                                {rateRider ? (
                                  <span className="ant-rate-text">
                                    {desc[rateRider - 1]}
                                  </span>
                                ) : (
                                  ""
                                )}
                              </span>
                            </div>
                          </div>
                        ) : null}

                        <div
                          style={{
                            padding: 20,
                            justifyContent: "center",
                            alignItems: "center",
                          }}
                        >
                          <h3>
                            ¿Cuál fue tu experiencia en{" "}
                            {dataOrder.restaurants.title}?
                          </h3>
                          <p>
                            Valora el servicio brindado por la tienda para
                            ayudar a otros clientes como tu.
                          </p>
                          <span style={{ marginBottom: 20 }}>
                            <Rate
                              tooltips={desc}
                              onChange={handleChangeValorar}
                              value={rateValorar}
                              style={{ fontSize: 24 }}
                            />
                            {rateValorar ? (
                              <span className="ant-rate-text">
                                {desc[rateValorar - 1]}
                              </span>
                            ) : (
                              ""
                            )}
                          </span>
                          <TextArea
                            rows={4}
                            onChange={(e) => setComment(e.target.value)}
                            style={{ marginBottom: 30 }}
                          />
                          <button
                            className="dejar_op"
                            onClick={() => {
                              ordersProcess(
                                dataOrder.id,
                                "Valorada",
                                "100",
                                "success"
                              );
                              valorarRestaurant(dataOrder.id);
                              valorateRIder(
                                dataOrder.Riders._id,
                                dataOrder.Riders.rating
                              );
                            }}
                          >
                            {loadinVal ? (
                              <Spin size="small" />
                            ) : (
                              "Valorar tienda"
                            )}
                          </button>
                        </div>
                      </div>
                    </Modal>

                    {/* Modal para valoracion fin */}

                    <div className="titulos" style={{ marginTop: 50 }}>
                      <h3>Resumen</h3>
                      <div style={{ display: "flex", marginTop: 20 }}>
                        <div>
                          <p>Subtotal:</p>
                          <p>Complementos extras:</p>
                          <p>Descuentos:</p>
                          <p>Tarifa de servicio:</p>
                          <p>Envío:</p>
                          <p>Propina:</p>
                          <h2>Total:</h2>
                        </div>
                        <div style={{ marginLeft: "auto", textAlign: "right" }}>
                          <p>
                            {formaterPrice(
                              dataOrder.subtotal,
                              language,
                              currency
                            )}
                          </p>
                          <p>
                            {formaterPrice(
                              dataOrder.complement,
                              language,
                              currency
                            )}
                          </p>
                          <p>{dataOrder.descuentopromo}</p>
                          <p>
                            {formaterPrice(
                              dataOrder.tarifa,
                              language,
                              currency
                            )}
                          </p>
                          <p>
                            {formaterPrice(dataOrder.envio, language, currency)}
                          </p>
                          <p>
                            {formaterPrice(
                              dataOrder.propinaTotal,
                              language,
                              currency
                            )}
                          </p>
                          <h2>
                            {formaterPrice(dataOrder.total, language, currency)}
                          </h2>
                        </div>
                      </div>
                      <h6>
                        Este total incluye descuento, propina a la tienda y todo
                        los cargos de transacción.
                      </h6>
                    </div>

                    {dataOrder.estado === "Entregada" ? (
                      <button
                        style={{ backgroundColor: "#FFA500" }}
                        className="valorar"
                        onClick={() => showModalValora()}
                      >
                        Valorar experiencia
                      </button>
                    ) : null}
                  </div>
                </Modal>
              ) : null}
            </>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(Order);
