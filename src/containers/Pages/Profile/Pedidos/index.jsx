import React, { useState } from "react";
import NewOrder from "./NewOrder";
import LastOrder from "./lastOrders";

export default function Order(props) {
  const { res } = props;
  const [activeIndex, setactiveIndex] = useState(0);

  const renderOrder = () => {
    switch (activeIndex) {
      case 0:
        return <NewOrder res={res} />;
      case 1:
        return <LastOrder res={res} />;
      default:
        return <NewOrder res={res} />;
    }
  };
  return (
    <div className="page-content_Items_description">
      <h1>Tus pedidos</h1>
      <p>Aquí podrás ver tus pedido, gestionarlos y valorar los finalizado.</p>
      <div>
        <button
          onClick={() => setactiveIndex(0)}
          className={activeIndex === 0 ? "actv" : "page-content_Items_buttom"}
        >
          NUEVOS PEDIDOS
        </button>
        <button
          onClick={() => setactiveIndex(1)}
          className={activeIndex === 1 ? "actv" : "page-content_Items_buttom"}
        >
          ANTERIORES
        </button>
        <button
          onClick={() => setactiveIndex(2)}
          className={activeIndex === 2 ? "actv" : "page-content_Items_buttom"}
        >
          ENVÍO EXPRESS
        </button>
      </div>
      <div className="page-content_Items">{renderOrder()}</div>
    </div>
  );
}
