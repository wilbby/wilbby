import React from "react";
import CardRestaurant from "../../../Components/CardRestaurant";
import "./index.css";
import Lottie from "react-lottie";

const fav = require("../../../Assets/Animate/hearto.json");

const defaultOptionsFav = {
  loop: true,
  autoplay: true,
  animationData: fav,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Favoritos(props) {
  const { data, refetch, latitude, longitude, city } = props;
  return (
    <div className="page-content_Items_description">
      <h1>Tu tiendas favoritas</h1>
      <p>
        Aquí podrás ver tus tiendas favoritas para que acceda más fácil a ellas.
      </p>
      <div className="page-content_Items">
        {data.length === 0 ? (
          <div style={{ textAlign: "center" }}>
            <Lottie options={defaultOptionsFav} height={180} width={180} />
            <span>No tienes tiendas en la lista de deseos.</span>
          </div>
        ) : (
          <div className="fovatite_Cont">
            {data &&
              data.map((f, i) => {
                return (
                  <CardRestaurant
                    key={i}
                    res={f.restaurant}
                    refetch={refetch}
                    coordetate={latitude}
                    coordenateone={longitude}
                    city={city}
                  />
                );
              })}
          </div>
        )}
      </div>
    </div>
  );
}
