import React from "react";
import { ClockCircleOutlined, EyeOutlined } from "@ant-design/icons";
import { IMAGES_PATH } from "../../../Utils/UrlConfig";
import "./index.css";
import moment from "moment";
import { useMutation } from "react-apollo";
import { READ_NOTIFICATION } from "../../../GraphQL/mutation";
import { message } from "antd";
import Lottie from "react-lottie";

const noti = require("../../../Assets/Animate/bell.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: noti,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

export default function Card(props) {
  const { data, refetch } = props;
  const [readNotification] = useMutation(READ_NOTIFICATION);

  const readNotifications = (id) => {
    readNotification({ variables: { notificationId: id } }).then((res) => {
      if (res.data.readNotification.success) {
        message.success("Notificación marcada como leída");
        refetch();
      } else {
        message.error("Algo va mal intentalo de nuevo");
        refetch();
      }
    });
  };

  console.log(data.length === 0);
  return (
    <>
      {data.length === 0 ? (
        <div style={{ textAlign: "center" }}>
          <Lottie options={defaultOptionsBell} height={180} width={180} />
          <span>Estás al día, no tienes notificaciones.</span>
        </div>
      ) : (
        <>
          {data &&
            data.map((n, i) => {
              let title = "";
              let description = "";
              let avatar = "";

              switch (n.type) {
                case "order_confirmed":
                  title = "Pedido confirmado";
                  description = `Enhorabuena ${n.Restaurant.title} ha confirmado tu pedido.`;
                  avatar = n.Restaurant.logo;
                  break;

                case "accept_order":
                  title = "El rider va por tu pedido";
                  description = `Esto avanza ${n.Riders.name} va de camino a recojer tu pedido.`;
                  avatar = n.Restaurant.logo;
                  break;

                case "order_process_for_rider":
                  title = "Tu pedido ha sido recogido";
                  description = `${n.Riders.name} ha recogido tu pedido y ya esta en camino.`;
                  avatar = IMAGES_PATH + n.Riders.avatar;
                  break;

                case "order_process":
                  title = "El rider va de camino";
                  description = `${n.Riders.name} esta en camino con tu pedido.`;
                  avatar = IMAGES_PATH + n.Riders.avatar;
                  break;

                case "finish_order":
                  title = "Pedido Entregado";
                  description = `Tu pedido ha sido entregado por ${n.Restaurant.title} por favor dejanos tu opinión sobre los productos y la tienda`;
                  avatar = IMAGES_PATH + n.Riders.avatar;
                  break;

                case "finish_order_store":
                  title = "Pedido Entregado";
                  description = `Tu pedido ha sido entregado por ${n.Riders.name} por favor dejanos tu opinión sobre los productos y la tienda`;
                  avatar = n.Restaurant.logo;
                  break;

                case "reject_order":
                  title = "Pedido Rechazado";
                  description = `Upps ${n.Restaurant.title} no ha podido realizar tu pedido lo sentimos.`;
                  avatar = n.Restaurant.logo;
                  break;

                case "resolution_order":
                  title = "Pedido en reclamacion";
                  description = `Tu pedido está en proceso de reclamación sentimos los inconvenientes.`;
                  avatar = n.Restaurant.logo;
                  break;
              }
              return (
                <div
                  className="card_Notification"
                  key={i}
                  onClick={() => readNotifications(n._id)}
                >
                  <div>
                    <img src={avatar} alt={title} />
                  </div>
                  <div className="infor">
                    <h1>{title}</h1>
                    <p>{description}</p>
                    <span>
                      <ClockCircleOutlined />{" "}
                      {moment(Number(n.created_at)).fromNow()} · <EyeOutlined />{" "}
                      Marcar como leído
                    </span>
                  </div>
                </div>
              );
            })}
        </>
      )}
    </>
  );
}
