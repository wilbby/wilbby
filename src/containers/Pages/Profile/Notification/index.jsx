import React from "react";
import Card from "./card";
export default function Notification(props) {
  const { data, refetch } = props;
  return (
    <div className="page-content_Items_description">
      <h1>Notificaciones</h1>
      <p>Mantente al día de todas las novedades</p>
      <div className="page-content_Items">
        <Card data={data} refetch={refetch} />
      </div>
    </div>
  );
}
