import React, { useState } from "react";
import "./index.css";
import { useQuery } from "react-apollo";
import { CATEGORY } from "../../../GraphQL/query";
import { withRouter } from "react-router-dom";
import { Modal, message } from "antd";
import { EnvironmentOutlined, SearchOutlined } from "@ant-design/icons";
import SimpleMap from "../../../Components/Maps";
import { validateDates } from "../../../Utils/calculateDate";
import { useTranslation } from "react-i18next";

function Category(props) {
  const { history, city, lat, lgn } = props;
  const [visible, setvisible] = useState(false);
  const [dataModal, setdataModal] = useState(null);
  const { t } = useTranslation("common", { useSuspense: false });
  const { data } = useQuery(CATEGORY);

  const datos = data && data.getCategory ? data.getCategory.data : [];

  const openModal = (datas) => {
    setvisible(true);
    setdataModal(datas);
  };

  return (
    <div className="category__cont">
      <div className="category__cont_item">
        {datos.map((c, i) => {
          const nav = () => {
            switch (c.title) {
              case "Algo Especial":
                message.warning("Pronto estáremos disponible");
                break;
              case "Envío Express":
                message.warning("Pronto estáremos disponible");
                break;
              default:
                openModal(c);
                break;
            }
          };
          return (
            <div
              className="cats"
              key={i}
              onClick={() => (validateDates(9, 30, 23, 59) ? nav() : null)}
            >
              <img
                src={c.image}
                className={
                  validateDates(9, 30, 23, 59) ? "imagenCat" : "imagenCatInac"
                }
              />
              <p>{t(`category:${c.title}`)}</p>
            </div>
          );
        })}
      </div>
      {dataModal ? (
        <Modal
          footer={false}
          title="Confirma tu ubicación"
          visible={visible}
          onCancel={() => setvisible(false)}
        >
          <SimpleMap
            lat={lat}
            lgn={lgn}
            width="100%"
            height={250}
            title="Confirma tu ubicación"
          />

          <div style={{ marginBottom: 70, height: 350 }}>
            <div className="info_map">
              <h3>Confirma tu ubicación</h3>
            </div>

            <div className="info_map">
              <EnvironmentOutlined
                style={{ marginRight: 10, fontSize: 22, color: "#90c33c" }}
              />
              <div>
                <h2>Ahora estás en:</h2>
                <p>{city}</p>
              </div>
            </div>
            <div className="info_map">
              <SearchOutlined
                style={{
                  marginRight: 10,
                  fontSize: 22,
                  color: "#90c33c",
                  marginBottom: 10,
                }}
              />
              <div>
                <p style={{ lineHeight: 1 }}>
                  Buscar {dataModal.title} en {city}
                </p>
              </div>
            </div>
            <div className="info_map">
              <button
                onClick={() =>
                  history.push(`/search/category/${dataModal._id}/${city}`)
                }
              >
                Continuar {city ? `en ${city}` : ""}
              </button>
            </div>
          </div>
        </Modal>
      ) : null}
    </div>
  );
}

export default withRouter(Category);
