import React from "react";
import "./index.css";
import { CheckCircleTwoTone } from "@ant-design/icons";
import Chica from "../../../Assets/images/chica.png";

export default function ContainerCard() {
  return (
    <div className="container-page2">
      <div className="sub-container-page2">
        <div className="sub-sec-1">
          <h2>
            Encuentra tus tiendas de confianza en Wilbby y realiza tus compras
            desde la app.
          </h2>
          <h3>
            <CheckCircleTwoTone twoToneColor="#90C33C" /> Protección de pago
            garantizado.
          </h3>
          <p>
            En Wilbby contamos con un sistema de pago 100% seguro y cifrado para
            que puedas realizar tus compras con total tranquilidad.
          </p>
          <h3>
            <CheckCircleTwoTone twoToneColor="#90C33C" /> {""}
            Ofertas exclusivas.
          </h3>
          <p>
            Encuentra miles de tiendas en tu ciudad con las mejores ofertas sólo
            en Wilbby .
          </p>
          <h3>
            <CheckCircleTwoTone twoToneColor="#90C33C" /> Estamos aquí para
            ayudarte 24/7.
          </h3>
          <p>
            Wilbby está aquí para responder cualquier pregunta y resolver
            cualquier problema con tu pedido en todo momento.
          </p>
        </div>
        <div className="sub-sec-1">
          <img src={Chica} className="chica" />
        </div>
      </div>
    </div>
  );
}
