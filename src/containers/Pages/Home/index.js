import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import CookieConsent from "react-cookie-consent";
import Lottie from "react-lottie";
import "./index.css";
import animationData from "./chica.json";
import { Modal } from "antd";
import { EnvironmentOutlined, SearchOutlined } from "@ant-design/icons";
import SimpleMap from "../../Components/Maps";
import Page1 from "./page1";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import Page3 from "./page3";
import Page4 from "./page4";
import Page5 from "./page5";
import Category from "./Categoris";
import Offert from "./Offert/index";
import { Query } from "react-apollo";
import { GET_CITY } from "../../GraphQL/query";

const curve = require("./curve.svg");
const Burger = require("./burgers-1.png");

const Home = (props) => {
  const [city] = useState(localStorage.getItem("city"));
  const [search, setSearch] = useState("");
  const [visible, setvisible] = useState(false);
  const [lat, setlat] = useState();
  const [lgn, setlgn] = useState();
  const { history } = props;

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  useEffect(() => {
    document.title = `Wilbby | comida a domicilio`;
  }, []);

  function getGeoLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          setlat(latitude);
          setlgn(longitude);
          let apiUrlWithParams = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyDy80slwpOlrhOhU-qLYFmGKN3FWgAsmGA`;
          fetch(apiUrlWithParams)
            .then((response) => response.json())
            .then((data) => {
              for (let index = 0; index < data.results.length; index++) {
                for (
                  let i = 0;
                  i < data.results[index].address_components.length;
                  i++
                ) {
                  if (
                    data.results[index].address_components[i].types.includes(
                      "locality"
                    )
                  ) {
                    localStorage.setItem(
                      "city",
                      data.results[index].address_components[i].long_name
                    );
                  }
                }
              }
            })
            .catch((error) => {
              console.log("Error en obtener ciudad", error);
            });
        },
        (error) => console.log("geolocation error", error)
      );
    } else {
      console.log("this browser not supported HTML5 geolocation API");
    }
  }

  var location = navigator.language;
  localStorage.setItem("language", location);
  localStorage.setItem("currency", "EUR");

  useEffect(() => {
    getGeoLocation();
  }, [city]);

  return (
    <>
      <GetApp />
      <Header fromHome={true} />
      <Query query={GET_CITY} variables={{ city: city }}>
        {(response) => {
          if (response) {
            const ciudada =
              response && response.data && response.data.getCity
                ? response.data.getCity.data
                : {};
            return (
              <div className="Constiner-home">
                <div className="Barner-home-full">
                  {ciudada && ciudada.close ? (
                    <div className="no_available">
                      <h1>{ciudada.title}</h1>
                      <img src={ciudada.imagen} />
                      <p>{ciudada.subtitle}</p>
                    </div>
                  ) : (
                    <div>
                      <div className="Barner-home">
                        <div>
                          <div className="search">
                            <h1>{`Con Wilbby lo tienes todo ${
                              city ? `en ${city}` : ""
                            }`}</h1>
                            <p>
                              Entrega de productos de las tiendas y restaurantes
                              de tu barrio en unos minutos
                            </p>
                            <input
                              type="text"
                              placeholder="Restaurantes, Supermercados, farmacias, tiendas y más..."
                              onChange={(e) => setSearch(e.target.value)}
                            />
                            <button onClick={() => setvisible(true)}>
                              Buscar
                            </button>
                          </div>
                        </div>
                        <div className="lotie">
                          <img
                            src={Burger}
                            alt="Burger wilbby"
                            className="burguer_img"
                          />
                        </div>
                      </div>
                      <Category city={city} lat={lat} lgn={lgn} />
                    </div>
                  )}

                  <img src={curve} style={{ marginBottom: -1 }} />
                </div>

                {ciudada && ciudada.close ? null : <Offert city={city} />}

                <Page1 onClick={() => setvisible(true)} />
                <Page3 />
                <Page4 />
                <Page5 />
                <CookieConsent
                  location="bottom"
                  buttonText="Aceptar"
                  cookieName="myAwesomeCookieName2"
                  style={{
                    background: "#eeeeee",
                    color: "black",
                  }}
                  buttonStyle={{
                    color: "white",
                    fontSize: "14px",
                    backgroundColor: "#90C33C",
                    borderRadius: 100,
                    width: 150,
                    outline: "none",
                  }}
                  expires={150}
                >
                  Este sitio web utiliza cookies para garantizar que obtenga la
                  mejor experiencia en nuestro sitio web.{" "}
                  <span style={{ fontSize: "10px" }}>
                    Más detalles en https://Wilbby.com/cookies
                  </span>
                </CookieConsent>

                <Modal
                  footer={false}
                  title="Confirma tu ubicación"
                  visible={visible}
                  onCancel={() => setvisible(false)}
                >
                  <SimpleMap
                    lat={lat}
                    lgn={lgn}
                    width="100%"
                    height={250}
                    title="Confirma tu ubicación"
                  />

                  <div style={{ marginBottom: 70, height: search ? 350 : 300 }}>
                    <div className="info_map">
                      <h3>Confirma tu ubicación</h3>
                    </div>

                    <div className="info_map">
                      <EnvironmentOutlined
                        style={{
                          marginRight: 10,
                          fontSize: 22,
                          color: "#90c33c",
                        }}
                      />
                      <div>
                        <h2>Ahora estás en:</h2>
                        <p>{city}</p>
                      </div>
                    </div>
                    {search ? (
                      <div className="info_map">
                        <SearchOutlined
                          style={{
                            marginRight: 10,
                            fontSize: 22,
                            color: "#90c33c",
                            marginBottom: 10,
                          }}
                        />
                        <div>
                          <p style={{ lineHeight: 1 }}>
                            Buscar {search} en {city}
                          </p>
                        </div>
                      </div>
                    ) : null}
                    <div className="info_map">
                      <button
                        onClick={() =>
                          search
                            ? history.push(`/search/${city}/${search}`)
                            : history.push(`/search/${city}`)
                        }
                      >
                        Continuar {city ? `en ${city}` : ""}
                      </button>
                    </div>
                  </div>
                </Modal>
              </div>
            );
          }
        }}
      </Query>
    </>
  );
};

export default withRouter(Home);
