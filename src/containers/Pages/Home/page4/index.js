import React from "react";
import "./index.css";

const apple = require("../../../Assets/images/apple.svg");
const google = require("../../../Assets/images/google.png");
const icons = require("./icon.png");

export default function BusineInfo() {
  return (
    <div className="bussines__containes">
      <div className="Busines__intem">
        <h1 className="afiction_conatiner_title">Únete a Wilbby</h1>
        <div className="card_busines">
          <div className="imgs__bussines" />
          <div className="info_bussines">
            <h2>Wilbby para restaurantes </h2>
            <p>
              Empieza a aceptar pedidos online y a domicilio, llega a más
              clientes con nuestra herramienta.
            </p>
            <a href="https://stores.wilbby.com" target="_black">
              ¡Empieza gratis!
            </a>
          </div>
        </div>

        <div className="card_busines">
          <div className="imgs__app responsive">
            <img src={icons} alt="Wilbby" />
          </div>
          <div className="info_bussines">
            <h2>¿Ya tienes nuestra app?</h2>
            <p>
              Descarga nuestra app para ios y android y empieza a disfrutar de
              las ventajas que ofrece Wilbby.
            </p>
            <img
              src={apple}
              alt="Apple Store"
              className="apple_logo"
              style={{ cursor: "pointer" }}
              onClick={() =>
                (window.location.href =
                  "https://apps.apple.com/es/app/wilbby-comida-a-domicilio/id1553798083")
              }
            />
            <img
              src={google}
              alt="Google Play"
              className="google_logo"
              style={{ cursor: "pointer" }}
              onClick={() =>
                (window.location.href =
                  "https://play.google.com/store/apps/details?id=com.foodyapp")
              }
            />
          </div>
          <div className="imgs__app no_responsive" />
        </div>
      </div>
    </div>
  );
}
