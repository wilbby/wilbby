import React from "react";
import "./index.css";
import Chica from "./9672.jpg";

export default function Afiction() {
  return (
    <div className="container-page4">
      <div className="sub-container-page4">
        <div className="sec-container-page4">
          <div className="tex-sec-4">
            <h1>¿Con qué pueden ayudar nuestros veterinarios?</h1>
            <p>
              Nuestro equipo de veterinarios está listo para ayudar a tu mascota
              si no se encuentra bien, o si necesitas consejos generales sobre
              su salud y bienestar.
            </p>

            <a href="/te-ayudamos" className="btn-conocer">
              Conoce nuestros veterinarios
            </a>
          </div>
          <div className="immm">
            <img src={Chica} className="imagen-chica" />
          </div>
        </div>
      </div>
    </div>
  );
}
