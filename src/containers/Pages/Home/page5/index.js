import React from "react";

import "./index.css";

const img = require("./mamataco.jpg");
const img1 = require("./riders.png");

export default function CardContact() {
  return (
    <div className="card_contact_container">
      <div className="card_contact_container_childerm">
        <h1 className="afiction_conatiner_title">Se parte de Wilbby</h1>
        <div className="card_contact_container_items">
          <div className="card_contact_container_items_content">
            <img src={img1} alt="" />
            <div className="info_container_card">
              <h1>Repartidores</h1>
              <h4>Total libertad</h4>
              <p>
                Sé tu propio jefe. Flexibilidad de horarios, ingresos
                competitivos y conoce tu ciudad repartiendo al aire libre.
              </p>
              <a href="/riders">Repartir en Wilbby</a>
            </div>
          </div>
          <div className="card_contact_container_items_content">
            <img src={img} alt="" />
            <div className="info_container_card">
              <h1>Únete a nosotros</h1>
              <h4>Atrae nuevos clientes</h4>
              <p>
                Ponemos todos nuestros recursos y medios a tu servicio para
                ayudarte a reducir costes e impulsar tu negocio.
              </p>
              <a href="https://stores.wilbby.com" target="_black">
                Asociate a Wilbby
              </a>
            </div>
          </div>
          <div className="card_contact_container_items_content">
            <img
              src="https://image.freepik.com/foto-gratis/mujer-gerente-dirigiendo-reunion-intercambio-ideas_58466-11714.jpg"
              alt=""
            />
            <div className="info_container_card">
              <h1>Se parte del equipo</h1>
              <h4>Ayudanos a crear este sueño</h4>
              <p>
                Únete a nuestro equipo y empecemos a construir la mejor
                herramienta para digitalizar las tiendas de los barrios.
              </p>
              <a href="/team">Únete a Wilbby</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
