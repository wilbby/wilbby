import React from "react";
import "./index.css";
import {
  ClockCircleTwoTone,
  ShopTwoTone,
  ArrowRightOutlined,
  ShoppingTwoTone,
} from "@ant-design/icons";
import { formaterPrice } from "../../../Utils/formatePrice";

const Page1 = (props) => {
  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  return (
    <div className="container-page1" id="ventajas">
      <div className="sub-container-page1">
        <h1>Descubre nuestras ventajas</h1>
        <p>
          Tienes una y mil razones para hacer tus pedidos en las tiendas de tu
          barrio con Wilbby.
        </p>
      </div>
      <div className="constainer-box">
        <div className="card-items">
          <div className="icon-sec">
            <ShopTwoTone twoToneColor="#90C33C" style={{ fontSize: 25 }} />
          </div>
          <h2>Pedidos desde la app</h2>
          <p>
            Ahora puedes hacer tus pedidos de las tiendas de tu barrio a
            domicilio, con envío a tan sólo{" "}
            {formaterPrice(1.95, language, currency)} y entregas en menos de 30
            minutos.{" "}
          </p>
          <a onClick={props.onClick} className="link-footer">
            Ver tiendas <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>

        <div className="card-items">
          <div className="icon-sec">
            <ShoppingTwoTone twoToneColor="#90C33C" style={{ fontSize: 25 }} />
          </div>
          <h2>Ofertas exclusivas</h2>
          <p>
            Otra de las ventajas que puedes disfrutar con la app, es poder
            acceder a todas nuestras ofertas exclusivas y pedir que te lo
            llevemos donde estés.
          </p>
          <a onClick={props.onClick} className="link-footer">
            Ver ofertas <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>

        <div className="card-items">
          <div className="icon-sec">
            <ClockCircleTwoTone
              twoToneColor="#90C33C"
              style={{ fontSize: 25 }}
            />
          </div>
          <h2>Programa pedidos</h2>
          <p>
            Programa pedidos desde la app de Wilbby y olvídate del resto, sólo
            di dónde quieres que te llevamos a la hora seleccionada.
          </p>
          <a onClick={props.onClick} className="link-footer">
            Programar un pedido <ArrowRightOutlined style={{ marginTop: 5 }} />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Page1;
