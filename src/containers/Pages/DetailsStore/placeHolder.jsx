import React from "react";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

export default function placeHolder() {
  return (
    <div>
      <>
        <div>
          <div>
            <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
              <Skeleton
                height={500}
                width="100%"
                style={{ marginBottom: 20 }}
              />
            </SkeletonTheme>
          </div>
        </div>
      </>
    </div>
  );
}
