import React, { useState, useEffect } from "react";
import { withRouter, Link } from "react-router-dom";
import {
  RESTAURANT_SLUG_FOR_DEATILS,
  GET_MENU,
  GET_ADRESS_STORE,
} from "../../GraphQL/query";
import {
  ELIMINAR_RESTAURANT_FAVORITE,
  ANADIR_RESTAURANT_FAVORITE,
} from "../../GraphQL/mutation";
import { Query, useMutation } from "react-apollo";
import { Button, Breadcrumb, Affix, message, Modal } from "antd";
import {
  EnvironmentOutlined,
  ShoppingOutlined,
  HeartOutlined,
  HeartFilled,
  StarFilled,
  EllipsisOutlined,
  CustomerServiceOutlined,
  ShareAltOutlined,
} from "@ant-design/icons";
import MenuRestaurant from "../../Components/Menu";
import ShoppintCart from "../../Components/ShoppingCart";
import Valoraciones from "../../Components/Opiniones";
import Mapa from "../../Components/Maps";
import "moment/locale/es";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import { formaterPrice } from "../../Utils/formatePrice";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import "./index.css";
import "./index2.css";
import Lottie from "react-lottie";
import nodata from "../Home/chica.json";
import "./header.css";
import Scooter from "../../Assets/images/scooter.png";
import Run from "../../Assets/images/run.png";
import $ from "jquery";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";
import PlaceHolder from "./placeHolder";

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 30) {
      $(".nav_details").addClass("shadow_details");
      $(".logo_details").addClass("logo_details_black");
    } else {
      $(".nav_details").removeClass("shadow_details");
      $(".logo_details").removeClass("logo_details_black");
    }
  });
});

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 460) {
      $(".no_visible_items").addClass("visible_items");
    } else {
      $(".no_visible_items").removeClass("visible_items");
    }
  });
});

function DetailsStore(props) {
  const { history } = props;
  const slug = props.match.params.slug;
  const ciudad = props.match.params.city;
  const user = localStorage.getItem("id");
  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");
  let city = localStorage.getItem("city");
  const [shoModal, setShoModal] = useState(false);
  const [llevar, setLlevar] = useState(false);
  const [distacia] = useState(1200);
  const [title, setTitle] = useState("");
  const [categoria, setcategoria] = useState("");
  const [titls, setTitls] = useState("");
  const [contentModal, setcontentModal] = useState(null);
  const [crearFavorito] = useMutation(ANADIR_RESTAURANT_FAVORITE);
  const [eliminarFavorito] = useMutation(ELIMINAR_RESTAURANT_FAVORITE);

  const dist = distacia / 1609;
  const kms = dist * 1.6;

  useEffect(() => {
    document.title = `${title}`;
  }, [title]);

  const anadirFavorite = (restaurant, id, refetch, slug, city) => {
    if (!user) {
      message.warning("Debes iniciar sesión para añadir a favorito");
      setTimeout(() => {
        props.history.push(`/login?store=${slug}&city=${city}`);
      }, 2000);
    } else {
      crearFavorito({ variables: { restaurantID: restaurant, usuarioId: id } })
        .then(() => {
          message.success("Tienda añadido a la lista de deseos");
          refetch();
        })
        .catch((err) => {
          message.error("Algo salió mal intentalo de nuevo por favot");
          console.log(err);
        });
    }
  };

  const eliminarFavorite = (restaurant, refetch) => {
    eliminarFavorito({ variables: { id: restaurant } })
      .then(() => {
        message.success("Tienda eliminado a la lista de deseos");
        refetch();
      })
      .catch((err) => {
        message.error("Algo salió mal intentalo de nuevo por favot");
        console.log(err);
      });
  };

  function SetValoraciones(id, title) {
    return (
      <div style={{ padding: 20 }}>
        <Valoraciones id={id} title={title} />
      </div>
    );
  }

  function SetAutoShipping() {
    return (
      <div style={{ padding: 20 }}>
        <h1 style={{ fontWeight: "900", textAlign: "center" }}>
          SIN SEGUIMIENTO
        </h1>
        <Lottie options={defaultOptionsBell} height={150} width={150} />
        <div className="autoshipping_content">
          <div>
            <ShoppingOutlined style={{ fontSize: 48, color: "gray" }} />
            <p>
              Este establecimiento entrega sus pedidos, por lo que no podrás
              realizar el seguimiento.
            </p>
          </div>
          <div>
            <EnvironmentOutlined style={{ fontSize: 48, color: "gray" }} />
            <p>El seguimiento en vivo no está disponible para tu pedido.</p>
          </div>
          <div>
            <CustomerServiceOutlined style={{ fontSize: 48, color: "gray" }} />
            <p>
              Para cualquier cuestión relacionada con este pedido, contacta con
              la asistencia de Wilbby.
            </p>
          </div>
        </div>

        <Button
          type="primary"
          shape="round"
          onClick={() => setShoModal(false)}
          style={{ width: "100%", height: 50 }}
        >
          !De acuerdo¡
        </Button>
      </div>
    );
  }

  function Setshare(title) {
    return (
      <div style={{ textAlign: "center", padding: 20 }}>
        <div style={{ marginTop: 50, paddingBottom: 50 }}>
          <p>
            Comparte Wilbby con tus amigo y cuéntale todo lo que hay en {title}
          </p>
          <FacebookShareButton
            url={`https://wilbby.com/store/${city}/${slug}`}
            title={`Mira el oretón que hay en ${title} con wilbby te lo llevamos a casa`}
          >
            <FacebookIcon
              size={40}
              round={true}
              style={{ marginLeft: 10, outline: "none" }}
            />
          </FacebookShareButton>

          <TwitterShareButton
            url={`https://wilbby.com/store/${city}/${slug}`}
            title={`Mira el oretón que hay en ${title} con wilbby te lo llevamos a casa`}
          >
            <TwitterIcon
              size={40}
              round={true}
              style={{ marginLeft: 10, outline: "none" }}
            />
          </TwitterShareButton>

          <LinkedinShareButton
            url={`https://wilbby.com/store/${city}/${slug}`}
            title={`Mira el oretón que hay en ${title} con wilbby te lo llevamos a casa`}
          >
            <LinkedinIcon
              size={40}
              round={true}
              style={{ marginLeft: 10, outline: "none" }}
            />
          </LinkedinShareButton>

          <WhatsappShareButton
            url={`https://wilbby.com/store/${city}/${slug}`}
            title={`Mira el oretón que hay en ${title} con wilbby te lo llevamos a casa`}
          >
            <WhatsappIcon
              size={40}
              round={true}
              style={{ marginLeft: 10, outline: "none" }}
            />
          </WhatsappShareButton>
        </div>
      </div>
    );
  }

  function SetInfo(data, adress) {
    return (
      <div>
        <Mapa
          lat={Number.parseFloat(adress.lat)}
          lgn={Number.parseFloat(adress.lgn)}
          width="100%"
          height={200}
          title={data.title}
        />
        <div className="info-rest">
          <img src={data.logo} alt={data.title} />
          <div style={{ marginLeft: 10 }}>
            <h2>{data.title}</h2>
            <p>
              {adress.calle}, {adress.numero}, {adress.codigoPostal},{" "}
              {adress.ciudad}, pedido minimo{" "}
              {formaterPrice(data.minime, language, currency)}
            </p>
          </div>
        </div>
        <div style={{ marginTop: 30, marginBottom: 40, padding: 20 }}>
          <h3>¿Necesitas alguna sugerencia?</h3>
          <p>Pregunta a la tienda por más productos o recomendaciones</p>
          <a href={`tel:${data.phone}`}>Llamar a {data.title}</a>
        </div>
      </div>
    );
  }

  const handleModal = (type, id, datas, title, adress) => {
    switch (type) {
      case "Opiniones de clientes":
        setTitls("Opiniones de clientes");
        setcontentModal(SetValoraciones(id, title));
        setShoModal(true);
        break;

      case "Info del tienda":
        setTitls("Info de la tienda");
        setcontentModal(SetInfo(datas, adress));
        setShoModal(true);
        break;

      case "Compartir":
        setTitls("Compartir");
        setcontentModal(Setshare(title));
        setShoModal(true);
        break;

      case "Sin seguimiento":
        setTitls("Sin seguimiento");
        setcontentModal(SetAutoShipping());
        setShoModal(true);
        break;
      default:
        break;
    }
  };

  return (
    <div className="detail_store">
      {ciudad === city ? (
        <>
          {city ? (
            <Query
              query={RESTAURANT_SLUG_FOR_DEATILS}
              variables={{ slug: slug, city: city }}
            >
              {(response) => {
                if (response.loading) {
                  return <PlaceHolder />;
                }
                if (response) {
                  const res =
                    response &&
                    response.data &&
                    response.data.getRestaurantForDetails
                      ? response.data.getRestaurantForDetails.data
                      : {};
                  response.refetch();
                  setTitle(res.title);

                  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
                  res.Valoracion.forEach((start) => {
                    if (start.value === 1) rating["1"] += 1;
                    else if (start.value === 2) rating["2"] += 1;
                    else if (start.value === 3) rating["3"] += 1;
                    else if (start.value === 4) rating["4"] += 1;
                    else if (start.value === 5) rating["5"] += 1;
                  });

                  const ar =
                    (5 * rating["5"] +
                      4 * rating["4"] +
                      3 * rating["3"] +
                      2 * rating["2"] +
                      1 * rating["1"]) /
                    res.Valoracion.length;
                  let averageRating = 0;
                  if (res.Valoracion.length) {
                    averageRating = Number(ar.toFixed(1));
                  }

                  return (
                    <div className="details_container">
                      <div
                        className="details_header"
                        style={{
                          backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.3)), url(${res.image})`,
                        }}
                      >
                        <div className="nav_details">
                          <div className="header_items">
                            <Link to="/">
                              <div className="logo_details" />
                            </Link>
                            <div className="locations">
                              <div className="locations_active">
                                <EnvironmentOutlined
                                  style={{ marginRight: 2 }}
                                />
                                <p>{city}</p>
                              </div>
                              <div>
                                <p>Ahora</p>
                              </div>
                            </div>
                          </div>
                        </div>

                        <Affix
                          className="no_visible_items"
                          offsetTop={80}
                          onChange={(affixed) => {}}
                        >
                          <div className="stiky-contenedor_details">
                            <div className="stiky-items_details">
                              <Query
                                query={GET_MENU}
                                variables={{ id: res._id }}
                              >
                                {(response) => {
                                  if (response) {
                                    const menus =
                                      response &&
                                      response.data &&
                                      response.data.getMenu
                                        ? response.data.getMenu.list
                                        : [];
                                    return (
                                      <>
                                        {menus.map((c, i) => {
                                          return (
                                            <Button
                                              onClick={() => {
                                                if (categoria) {
                                                  setcategoria("");

                                                  if (categoria != c._id) {
                                                    setcategoria(c._id);
                                                  }
                                                } else {
                                                  setcategoria(c._id);
                                                }
                                              }}
                                              type={
                                                categoria === c._id
                                                  ? "primary"
                                                  : "default"
                                              }
                                              key={i}
                                              shape="round"
                                              className="cat_item-smoot_details"
                                              href={`#${c.title}`}
                                            >
                                              {c.title}
                                            </Button>
                                          );
                                        })}
                                      </>
                                    );
                                  }
                                }}
                              </Query>
                            </div>
                          </div>
                        </Affix>

                        <div className="breadcrumb_cont">
                          <div className="breadcrumb">
                            <Breadcrumb className="pagin">
                              <Breadcrumb.Item>
                                <a href="/" style={{ color: "white" }}>
                                  Inicio
                                </a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item>
                                <a href="" style={{ color: "white" }}>
                                  Categorías
                                </a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item>
                                <a
                                  href={`/search/${city}`}
                                  style={{ color: "white" }}
                                >
                                  {res.categoryName}
                                </a>
                              </Breadcrumb.Item>
                              <Breadcrumb.Item>
                                <span style={{ color: "#90C33C" }}>
                                  {res.title}
                                </span>
                              </Breadcrumb.Item>
                            </Breadcrumb>
                          </div>
                        </div>
                      </div>
                      <div className="container_info">
                        <div className="container_info_content">
                          <h1>{res.title}</h1>
                          <div className="container_info_shipping">
                            <div>
                              {!llevar ? (
                                <img src={Scooter} alt="Scooter" />
                              ) : (
                                <img src={Run} alt="Run" />
                              )}
                              {!llevar ? (
                                <span>
                                  {formaterPrice(
                                    res.shipping,
                                    language,
                                    currency
                                  )}
                                </span>
                              ) : (
                                <span>Gratis</span>
                              )}
                            </div>
                            <div>
                              <EnvironmentOutlined style={{ fontSize: 16 }} />
                              <span>{kms.toFixed(1)} km</span>
                            </div>
                            <div>
                              <ShoppingOutlined style={{ fontSize: 16 }} />
                              <span>
                                {formaterPrice(res.minime, language, currency)}{" "}
                                Mín
                              </span>
                            </div>
                          </div>
                          <div className="info_btns">
                            {res.anadidoFavorito ? (
                              <button
                                onClick={() =>
                                  eliminarFavorite(res._id, response.refetch)
                                }
                              >
                                <HeartFilled
                                  style={{
                                    fontSize: 20,
                                    paddingTop: 5,
                                    color: "#F5365C",
                                  }}
                                />
                              </button>
                            ) : (
                              <button
                                onClick={() =>
                                  anadirFavorite(
                                    res._id,
                                    user,
                                    response.refetch,
                                    res.slug,
                                    city
                                  )
                                }
                              >
                                <HeartOutlined
                                  style={{
                                    fontSize: 20,
                                    paddingTop: 5,
                                    color: "gray",
                                  }}
                                />
                              </button>
                            )}

                            <button
                              onClick={() =>
                                handleModal(
                                  "Opiniones de clientes",
                                  res._id,
                                  "",
                                  res.title,
                                  ""
                                )
                              }
                            >
                              <div style={{ paddingTop: -10 }}>
                                <StarFilled
                                  style={{
                                    fontSize: 16,
                                    color: "#FFA500",
                                  }}
                                />
                                <p
                                  style={{
                                    lineHeight: 0,
                                    margin: 0,
                                    fontSize: 10,
                                  }}
                                >
                                  {averageRating.toFixed(1)}
                                </p>
                              </div>
                            </button>
                            <button
                              onClick={() =>
                                handleModal(
                                  "Info del tienda",
                                  "",
                                  res,
                                  "",
                                  res.adress
                                )
                              }
                            >
                              <EllipsisOutlined
                                style={{
                                  fontSize: 20,
                                  paddingTop: 5,
                                  color: "#90C33C",
                                }}
                              />
                            </button>
                            {res.autoshipping ? (
                              <button
                                onClick={() =>
                                  handleModal("Sin seguimiento", "", "", "", "")
                                }
                              >
                                <EnvironmentOutlined
                                  style={{
                                    fontSize: 20,
                                    paddingTop: 5,
                                    color: "#90C33C",
                                  }}
                                />
                              </button>
                            ) : null}
                            <button
                              onClick={() =>
                                handleModal("Compartir", "", res, res.title, "")
                              }
                            >
                              <ShareAltOutlined
                                style={{
                                  fontSize: 20,
                                  paddingTop: 5,
                                  color: "#90C33C",
                                }}
                              />
                            </button>
                          </div>
                          <div>
                            <div className="switch">
                              <button
                                className={llevar ? "active" : null}
                                onClick={() =>
                                  res.llevar ? setLlevar(false) : null
                                }
                              >
                                <img src={Scooter} alt="Scooter" /> Entrega
                              </button>
                              <button
                                className={!llevar ? "active" : null}
                                onClick={() =>
                                  res.llevar ? setLlevar(true) : null
                                }
                              >
                                <img src={Run} alt="Run" />{" "}
                                {res.llevar ? "Para recoger" : "No disponible"}
                              </button>
                            </div>
                            {llevar ? (
                              <div className="adress_content">
                                <div>
                                  <h3>
                                    {res.adress.calle} {res.adress.numero}
                                  </h3>
                                  <h3>
                                    {res.adress.codigoPostal}{" "}
                                    {res.adress.ciudad}
                                  </h3>
                                </div>
                                <div>
                                  <Button
                                    type="dashed"
                                    shape="round"
                                    href={`https://www.google.com/maps/search/?api=1&query=${res.adress.lat},${res.adress.lgn}`}
                                    target="_black"
                                  >
                                    Ver en el mapa
                                  </Button>
                                </div>
                              </div>
                            ) : null}
                          </div>

                          <div className="shipping_tipo">
                            <div className="shipping_tipo_items">
                              <Lottie
                                options={defaultOptionsBell}
                                height={30}
                                width={30}
                              />
                              <div style={{ marginLeft: 10 }}>
                                <h4>Lo antes posible</h4>
                                <p>
                                  {res.llevar
                                    ? llevar
                                      ? "Para recoger"
                                      : "Entrega a domicilio y recoger"
                                    : "Solo entrega a domicilio"}
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="body__details">
                        <div className="one">
                          <Query query={GET_MENU} variables={{ id: res._id }}>
                            {(response) => {
                              if (response.loading) {
                                return (
                                  <SkeletonTheme
                                    color="#E1E9EE"
                                    highlightColor="#F2F8FC"
                                  >
                                    <h1>
                                      <Skeleton
                                        count={1}
                                        style={{ borderRadius: 50 }}
                                      />
                                    </h1>
                                  </SkeletonTheme>
                                );
                              }

                              if (response) {
                                const menus =
                                  response &&
                                  response.data &&
                                  response.data.getMenu
                                    ? response.data.getMenu.list
                                    : [];
                                return (
                                  <>
                                    {menus.map((m, i) => {
                                      return (
                                        <MenuRestaurant
                                          deliverect={res.isDeliverectPartner}
                                          id={m._id}
                                          title={m.title}
                                          subtitle={m.subtitle}
                                          limit={Number(res.rating)}
                                        />
                                      );
                                    })}
                                  </>
                                );
                              }
                            }}
                          </Query>
                        </div>
                        <div className="two">
                          <ShoppintCart
                            id={user}
                            restaurant={res._id}
                            restaurants={res}
                            llevar={llevar}
                            city={city}
                          />
                        </div>
                      </div>
                      <Modal
                        title={titls}
                        visible={shoModal}
                        footer={false}
                        onCancel={() => setShoModal(false)}
                      >
                        {contentModal}
                      </Modal>
                    </div>
                  );
                }
              }}
            </Query>
          ) : (
            <div>
              <GetApp />
              <Header />
              <div className="no_city_con">
                <Lottie options={defaultOptionsBell} height={300} width={300} />
                <h1>Parece que algo no va bien</h1>
                <p>
                  Estamos teniendo algunos problemas intentalo de nuevo por
                  favor en un momento
                </p>
                <button onClick={() => history.push("/")}>
                  Volver al inicio
                </button>
              </div>
            </div>
          )}
        </>
      ) : (
        <div>
          <GetApp />
          <Header />
          <div className="no_city_con">
            <Lottie options={defaultOptionsBell} height={300} width={300} />
            <h1>Parece que algo no va bien</h1>
            <p>
              Estamos teniendo algunos problemas intentalo de nuevo por favor en
              un momento
            </p>
            <button onClick={() => history.push("/")}>Volver al inicio</button>
          </div>
        </div>
      )}
    </div>
  );
}

export default withRouter(DetailsStore);
