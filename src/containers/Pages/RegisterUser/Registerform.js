import React, { useState, useEffect } from "react";
import { Form, Input, Checkbox, message } from "antd";
import { NUEVO_USUARIO } from "../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { useHistory } from "react-router-dom";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import LoginForm from "../Login/Loginform";
import ReCAPTCHA from "react-google-recaptcha";

const TEST_SITE_KEY = "6LesEuAZAAAAADNLYIYpkm_hdF9zIMvRhBupo4ou";
const DELAY = 1000;

const RegistrationForm = () => {
  const [values, setValues] = useState("");
  const [load, setLoad] = useState(false);
  const [expired, setExpired] = useState(false);
  const history = useHistory();
  const [form] = Form.useForm();

  const _reCaptchaRef = React.createRef();

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoad(true);
    }, DELAY);
    return () => clearTimeout(timer);
  }, []);

  const handleChange = (value) => {
    console.log("Captcha value:", value);
    setValues(value);
    // if value is null recaptcha expired
    if (values === null) setExpired(true);
  };

  return (
    <Mutation mutation={NUEVO_USUARIO}>
      {(crearUsuario) => {
        return (
          <Form
            form={form}
            name="register"
            onFinish={(values) => {
              const handleSubmit = async () => {
                await crearUsuario({
                  variables: {
                    input: {
                      nombre: values.name,
                      apellidos: values.lastName,
                      email: values.email,
                      password: values.password,
                      city: localStorage.getItem("city"),
                      termAndConditions: true,
                    },
                  },
                }).then(async ({ data: res }) => {
                  if (res && res.crearUsuario && res.crearUsuario.success) {
                    message.success(res.crearUsuario.message);
                    const datss =
                      res && res.crearUsuario && res.crearUsuario.data;
                    history.push(`/verify-phone/${datss._id}`);
                    if (datss) {
                      const createclient = async () => {
                        let res = await fetch(
                          LOCAL_API_URL +
                            `/create-client?userID=${datss._id}&nameclient=${
                              datss.name + datss.lastName
                            }&email=${datss.email}`
                        );
                        const deletec = await res.json();
                        console.log(deletec);
                      };
                      createclient();
                    } else {
                      return null;
                    }
                  } else if (
                    res &&
                    res.crearUsuario &&
                    !res.crearUsuario.success
                  )
                    message.error(res.crearUsuario.message);
                });
              };
              handleSubmit();
            }}
            scrollToFirstError
          >
            <Form.Item
              name="name"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese su nombre!",
                },
              ]}
            >
              <Input placeholder="Nombre" className="form-control" />
            </Form.Item>

            <Form.Item
              name="lastName"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese su apellidos!",
                },
              ]}
            >
              <Input placeholder="Apellidos" className="form-control" />
            </Form.Item>

            <Form.Item
              name="email"
              rules={[
                {
                  type: "email",
                  message: "Email incorrecto!",
                },
                {
                  required: true,
                  message: "Por favor ingrese su Email!",
                },
              ]}
            >
              <Input placeholder="Email" className="form-control" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Por favor ingrese una contraseña!",
                },
              ]}
              hasFeedback
            >
              <Input.Password
                placeholder="Contraseña"
                className="form-control"
              />
            </Form.Item>

            <Form.Item
              name="confirm"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Por favor consfirme su contraseña!",
                },
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }

                    return Promise.reject(
                      "Las dos contraseñas que ingresaste no coinciden!"
                    );
                  },
                }),
              ]}
            >
              <Input.Password
                placeholder="Contraseña"
                className="form-control"
              />
            </Form.Item>
            <Form.Item
              name="agreement"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject(
                          "Por favor aceptes los términos y condiciones"
                        ),
                },
              ]}
            >
              <Checkbox>
                Acepto los términos y condiciones de Wilbby{" "}
                <a style={{ color: "#90C33C" }} href="/condiciones-de-uso">
                  Téminos y condiciones
                </a>
              </Checkbox>
            </Form.Item>
            {load && (
              <ReCAPTCHA
                style={{ display: "inline-block" }}
                ref={_reCaptchaRef}
                sitekey={TEST_SITE_KEY}
                onChange={handleChange}
              />
            )}
            <Form.Item>
              <button
                type="submit"
                className="btn-btn-primary"
                htmlType="submit"
                disabled={!values ? true : false}
              >
                ¡Regístrarme ahora!
              </button>
              <Form.Item>
                ¿tienes una cuenta?{" "}
                <a style={{ color: "#90C33C" }} href="/login">
                  Inicia sesión ahora!
                </a>
              </Form.Item>
            </Form.Item>
            <div style={{ paddingTop: 50 }}>
              <LoginForm fromSignup={true} />
            </div>
          </Form>
        );
      }}
    </Mutation>
  );
};

export default RegistrationForm;
