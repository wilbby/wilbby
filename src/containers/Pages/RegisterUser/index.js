import React, { useEffect } from "react";
import "./Register.css";
import RegistrationForm from "./Registerform";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";

function RegisterClient() {
  useEffect(() => {
    document.title = "Regístrarme";
  }, []);

  return (
    <>
      <GetApp />
      <Header />
      <div className="containers">
        <div className="ContainerformRegister">
          <p>Bienvenido a Wilbby!</p>
          <RegistrationForm />
        </div>
      </div>
    </>
  );
}

export default RegisterClient;
