import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import {
  Button,
  Space,
  DatePicker,
  TimePicker,
  Modal,
  Input,
  Switch,
  message,
  Spin,
  Tooltip,
} from "antd";
import {
  CheckCircleFilled,
  PlusCircleOutlined,
  PlusOutlined,
  MinusOutlined,
} from "@ant-design/icons";
import moment from "moment";
import "moment/locale/es";
import locale from "antd/es/locale/es_ES";
import ShoppintCart from "../../Components/ShoppingCart";
import { LOCAL_API_URL } from "../../Utils/UrlConfig";
import { Query, useMutation, useQuery } from "react-apollo";
import { USER_DETAIL, GET_ADRESS_STORE } from "../../GraphQL/query";
import {
  CREAR_MODIFICAR_ORDEN,
  CREATE_NOTIFICATION,
} from "../../GraphQL/mutation";
import CheckoutForm from "../Profile/wallet";
import Direccion from "../Profile/Direccion";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import { formaterPrice } from "../../Utils/formatePrice";
import "./index.css";

const { TextArea } = Input;

const visa = require("../../Assets/images/visa.jpg");
const mastercar = require("../../Assets/images/master.png");
const americam = require("../../Assets/images/americam.png");
const orther = require("../../Assets/images/Card3.png");
const paypal = require("../../Assets/images/paypal.png");

const opciones = [
  {
    id: 2,
    title: "Lo antes posible",
    icon: "shopping-bag",
    type: "Feather",
  },
  {
    id: 3,
    title: "Programar envío",
    icon: "clockcircleo",
    type: "AntDesign",
  },
];

const propi = [
  {
    id: 1,
    cantidad: 1,
  },
  {
    id: 2,
    cantidad: 2.5,
  },
  {
    id: 3,
    cantidad: 5,
  },
  {
    id: 4,
    cantidad: 7.5,
  },
  {
    id: 5,
    cantidad: 10,
  },

  {
    id: 6,
    cantidad: 15,
  },
  {
    id: 7,
    cantidad: 20,
  },
];

function OrdenScreen(props) {
  const { history } = props;

  const id = props.match.params.id;
  const restauran = props.match.params.restaurant;
  const subtotals = props.match.params.total;
  const shippin = props.match.params.shipping;
  const extras = props.match.params.extras;
  const isRestaurent = props.match.params.tipo;
  const llevar = props.match.params.llevar;
  const autoshipping = Boolean(props.match.params.autoshipping);
  const tarifa = props.match.params.tarifa;
  const city = props.match.params.city;

  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  const [selected, setSelected] = useState("Lo antes posible");
  const [date, setDate] = useState(new Date());
  const [cupon, setCupon] = useState(false);
  const [shoModalOpcion, setShoModalOpcion] = useState(false);
  const [shoModaladress, setShoModaladress] = useState(false);
  const [show, setShow] = useState(false);
  const [cardNumber, setCardNumber] = useState("");
  const [customer, setCustomer] = useState("");
  const [isTermino, setIsTermino] = useState(false);
  const [propina, setPropina] = useState(0);
  const [cubiertos, setCubierto] = useState(false);
  const [nota, setNota] = useState("");
  const [Loading, setLoading] = useState(false);
  const [ordenID, setOrdenID] = useState("");
  const [descuento, setDescuento] = useState({
    clave: "",
    descuento: 0,
    tipo: "",
  });
  const [adress, setAdress] = useState("");
  const [calle, setcalle] = useState("");
  const [fromPaypal, setfromPaypal] = useState(false);

  const setAdresActive = () => {
    const adressID = localStorage.getItem("adressId");
    const adressNAME = localStorage.getItem("adressName");
    setAdress(adressID);
    setcalle(adressNAME);
  };

  const [crearModificarOrden] = useMutation(CREAR_MODIFICAR_ORDEN);
  const [createNotification] = useMutation(CREATE_NOTIFICATION);

  const timeResult = selected === "Lo antes posible" ? selected : date;

  const messageTexteNeworden = `Enhorabuena has recibido un nuevo pedido, a por todas.`;

  const valorDescuento = descuento.descuento > 0 ? descuento.descuento : "";
  const tipo = descuento.tipo ? descuento.tipo : "";
  const subtotal = subtotals;
  let displayDescuento = `${formaterPrice(0, language, currency)}`;
  let descuentoFinal = 0;
  let total = subtotal;
  if (valorDescuento && tipo) {
    switch (tipo) {
      case "dinero":
        displayDescuento = `${formaterPrice(
          valorDescuento,
          language,
          currency
        )}`;
        total = subtotal - valorDescuento;
        break;
      case "porcentaje":
        displayDescuento = `${valorDescuento}%`;
        descuentoFinal = valorDescuento / 100;
        total = subtotal - subtotal * descuentoFinal;
        break;
    }
  }

  let subtotales = Number(subtotal).toFixed(2);
  let descuentos = displayDescuento;
  let extrass = Number(extras).toFixed(2);
  let envio = Number(shippin).toFixed(2);
  let propinarider = propina.toFixed(2);
  let totalFinal =
    Number(total) +
    Number(extras) +
    Number(envio) +
    Number(propinarider) +
    Number(tarifa);

  const showModal = () => {
    setShow(true);
  };

  const hidenModal = () => {
    setShow(false);
  };

  useEffect(() => {
    document.title = "Completar pedido";
    setAdresActive();
  }, []);

  function onChangeCubierto(checked) {
    setCubierto(checked);
  }

  function onChangeTerm(checked) {
    setIsTermino(checked);
  }

  function onChangeDate(date, dateString) {
    console.log(date, dateString);
    setDate(dateString);
  }

  function onChangeTime(time, timeString) {
    console.log(timeString);
    setDate(date + "T" + timeString + ".000Z");
  }

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  function setSelectedOpcion() {
    return (
      <div style={{ padding: 20 }}>
        {opciones.map((d, i) => {
          return (
            <ul
              key={i}
              style={{
                listStyle: "none",
                borderBottomWidth: 2,
                borderBottomColor: "#f4f5f5",
              }}
            >
              <li
                style={{
                  display: "flex",
                }}
              >
                <p
                  onClick={() => {
                    setSelected(d.title);
                    setShoModalOpcion(false);
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {d.title}
                </p>
                {d.title === selected ? (
                  <CheckCircleFilled
                    style={{
                      marginLeft: "auto",
                      color: "#90C33C",
                      fontSize: 18,
                    }}
                  />
                ) : null}
              </li>
            </ul>
          );
        })}
      </div>
    );
  }

  const SendPushNotificationNeworden = async (OnesignalID) => {
    const notificate = {
      IdOnesignal: OnesignalID,
      textmessage: messageTexteNeworden,
    };
    await fetch(`${LOCAL_API_URL}/send-push-notification-restaurant`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(notificate),
    });
  };

  const createNotifications = (user, restaurant, usuario, ordenId) => {
    const NotificationInput = {
      user,
      restaurant,
      ordenId,
      usuario,
      type: "new_order",
    };
    createNotification({
      variables: { input: NotificationInput },
    })
      .then(async (results) => {
        console.log("results de notifications", results);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  const { data } = useQuery(GET_ADRESS_STORE, {
    variables: { id: restauran, city: city },
  });
  const adressData = data ? data.getAdressStore.data : {};
  const adresStores = adressData ? adressData._id : "";

  const Crear_Orden = () => {
    setLoading(true);
    if (!cupon) {
      message.warning("Upps debes añadir un cupón");
      setCupon("");
      return null;
    } else {
      if (!isTermino) {
        setLoading(false);
        message.error(
          "Para continuar debes aceptas los terminos y condiciones"
        );
      } else {
        const input = {
          id: ordenID ? ordenID : null,
          display_id: Number.parseInt(getRandomArbitrary(100, 1000000)),
          restaurant: restauran,
          time: timeResult,
          nota: nota,
          aceptaTerminos: isTermino,
          userID: id,
          propina: propina == 0 ? false : true,
          clave: String(cupon),
          city: String(city),
          adresStore: String(adresStores),
          adress: llevar === "true" ? null : adress,
          llevar: llevar === "true" ? true : false,
          subtotal: String(subtotales),
          complement: String(extrass),
          descuentopromo: String(descuentos),
          tarifa: String(tarifa),
          envio: String(envio),
          propinaTotal: String(propina),
          total: String(totalFinal),
          cubiertos: cubiertos,
          schedule: timeResult === "Lo antes posible" ? false : true,
        };
        crearModificarOrden({
          variables: {
            input: input,
          },
        })
          .then(async (results) => {
            setOrdenID(results.data.crearModificarOrden.id);
            if (results) {
              if (results.data.crearModificarOrden.descuento === null) {
                message.error(
                  "Parece que el cupón no es válido intenta con otro código de descuento"
                );
                setCupon("");
                return null;
              }
              setDescuento(results.data.crearModificarOrden.descuento);
              setCupon("");
              message.success("Cupón añadido con éxito");
              setLoading(false);
            }
          })
          .catch((err) => {
            console.log("err", err);
            setLoading(false);
          });
      }
    }
  };

  const Guardar_Orden = () => {
    setLoading(true);
    if (!isTermino && !!adress) {
      setLoading(false);
      message.error("Para continuar debes aceptas los terminos y condiciones");
    } else {
      const input = {
        id: ordenID ? ordenID : null,
        display_id: Number.parseInt(getRandomArbitrary(100, 1000000)),
        restaurant: restauran,
        time: timeResult,
        nota: nota,
        aceptaTerminos: isTermino,
        userID: id,
        propina: propina == 0 ? false : true,
        clave: String(cupon),
        city: String(city),
        adresStore: String(adresStores),
        adress: llevar === "true" ? null : adress,
        llevar: llevar === "true" ? true : false,
        subtotal: String(subtotales),
        complement: String(extrass),
        descuentopromo: String(descuentos),
        tarifa: String(tarifa),
        envio: String(envio),
        propinaTotal: String(propina),
        total: String(totalFinal),
        cubiertos: cubiertos,
        schedule: timeResult === "Lo antes posible" ? false : true,
      };
      crearModificarOrden({
        variables: {
          input: input,
        },
      })
        .then(async (results) => {
          console.log(results.data);
          if (results.data.crearModificarOrden.id) {
            setLoading(false);
            if (fromPaypal) {
              SendPushNotificationNeworden(
                results.data.crearModificarOrden.restaurants.OnesignalID
              );
              createNotifications(
                restauran,
                restauran,
                id,
                results.data.crearModificarOrden.id
              );

              window.location.href = `${LOCAL_API_URL}/paypal?price=${totalFinal.toFixed(
                2
              )}&order=${
                results.data.crearModificarOrden.id
              }&cart=${restauran}&plato=${id}&currency=${currency}`;
            } else {
              let response = await fetch(
                `${LOCAL_API_URL}/payment-existing-card?customers=${customer}&order=${
                  results.data.crearModificarOrden.id
                }&card=${cardNumber}&amount=${
                  totalFinal.toFixed(2) * 100
                }&cart=${restauran}&total=${totalFinal}&currency=${currency}`
              );
              const procederalpago = await response.json();
              if (procederalpago.status === "succeeded") {
                setLoading(false);
                SendPushNotificationNeworden();
                createNotifications(
                  restauran,
                  restauran,
                  id,
                  results.data.crearModificarOrden.id
                );
                history.push(
                  `/payment-success/${results.data.crearModificarOrden.id}`
                );
              } else {
                setLoading(false);
                history.push(`/payment-error`);
                message.error(
                  "Hubo un error con tu método de pago vuelve a intentarlo por favor"
                );
              }
            }
          }
        })
        .catch((err) => {
          console.log("err", err);
          setLoading(false);
          history.push(`/payment-error`);
        });
    }
  };

  const brands = localStorage.getItem("brand");
  const cardLast4 = localStorage.getItem("cardLast4");
  const tokenCard = localStorage.getItem("tokenCard");
  const customers = localStorage.getItem("customer");

  let images = "";
  let brand = "";
  switch (brands) {
    case "visa":
      images = visa;
      brand = "Visa";
      break;
    case "mastercard":
      images = mastercar;
      brand = "Master Card";
      break;
    case "amex":
      images = americam;
      brand = "American Express";
      break;
    default:
      images = orther;
      brand = "Mi tarjeta";
  }

  const continuar = () => {
    if ((fromPaypal && isTermino) || (cardNumber && isTermino)) {
      return true;
    } else {
      return false;
    }
  };

  return (
    <>
      <GetApp />
      <Header />
      <div className="container__orden">
        <div className="container__orden_childrem">
          <div className="items_childrem">
            <div style={{ display: "flex" }}>
              <div>
                <h3>{selected}</h3>
                {selected === "Programar envío" ? (
                  <p style={{ color: "gray", lineHeight: 1 }}>
                    {moment(date).format("llll")}
                  </p>
                ) : null}

                {selected === "Lo antes posible" ? (
                  <p style={{ color: "gray", lineHeight: 1 }}>
                    {selected === "Lo antes posible"
                      ? calle
                        ? calle
                        : "Añadir dirección"
                      : "Dirección"}
                  </p>
                ) : null}
              </div>
              <Button
                onClick={() => setShoModalOpcion(true)}
                type="link"
                shape="round"
                style={{ marginLeft: "auto", alignSelf: "center" }}
              >
                Cambiar
              </Button>
            </div>
            {selected === "Programar envío" ? (
              <div style={{ marginTop: 15 }}>
                <h4>Selecciona una fecha y hora</h4>
                <Space direction="horizontal">
                  <DatePicker
                    onChange={onChangeDate}
                    locale={locale}
                    placeholder="Selecciona un fecha"
                  />
                  <TimePicker
                    onChange={onChangeTime}
                    locale={locale}
                    placeholder="Selecciona un ahora"
                  />
                </Space>
              </div>
            ) : null}
          </div>

          {isRestaurent == "Restaurantes" ? (
            <div className="items_childrem">
              <h3>¿Necesitas cubiertos?</h3>
              <div style={{ display: "flex" }}>
                <p>
                  Ayúdanos a reducir los residuos: pide cubiertos sólo cuando
                  los necesites.
                </p>
                <Switch
                  onChange={onChangeCubierto}
                  style={{ marginLeft: "auto" }}
                />
              </div>
            </div>
          ) : null}

          <div className="items_childrem">
            <h3>Añadir nota</h3>
            <TextArea
              rows={4}
              onChange={(e) => setNota(e.target.value)}
              placeholder="Añadir nota"
            />
          </div>
          {llevar == "false" ? (
            <div className="items_childrem">
              <h3>Diercción de envío</h3>
              <div style={{ display: "flex" }}>
                {calle ? (
                  <p>{calle}</p>
                ) : (
                  <p style={{ color: "red" }}>Añadir dirección</p>
                )}
                <Tooltip title="Añadir y editar dirección">
                  <PlusCircleOutlined
                    onClick={() => setShoModaladress(true)}
                    style={{
                      marginLeft: "auto",
                      color: "#90C33C",
                      fontSize: 24,
                      cursor: "pointer",
                    }}
                  />
                </Tooltip>
              </div>
            </div>
          ) : null}
          <Modal
            title="Añadir dirección"
            visible={shoModaladress}
            footer={false}
            onCancel={() => setShoModaladress(false)}
          >
            <div style={{ margin: 30, paddingBottom: 50 }}>
              <Direccion
                setShoModaladress={setShoModaladress}
                setAdresActive={setAdresActive}
              />
            </div>
          </Modal>

          <div className="items_childrem">
            <h3>Articulos</h3>
            <ShoppintCart id={id} restaurant={restauran} fromPedido={true} />
          </div>
          <div className="items_childrem">
            <h3>Términos y condiciones</h3>
            <div style={{ display: "flex" }}>
              <p>
                Confirmo que he leído y acepto el aviso legal sobre protección
                de datos personales y los términos y condiciones.
              </p>
              <Switch onChange={onChangeTerm} style={{ marginLeft: "auto" }} />
            </div>
          </div>

          <div className="items_childrem">
            <h3>Dejar propina al rider</h3>
            <div className="cont_propi">
              {propi.map((e, i) => {
                return (
                  <div
                    key={i}
                    className="card_prop"
                    onClick={() => {
                      if (propina) {
                        setPropina(0);
                        if (propina != e.cantidad) {
                          setPropina(e.cantidad);
                        }
                      } else {
                        setPropina(e.cantidad);
                      }
                    }}
                  >
                    <h4>{formaterPrice(e.cantidad, language, currency)}</h4>

                    {propina === e.cantidad ? (
                      <div style={{ display: "flex" }}>
                        <MinusOutlined
                          style={{
                            fontSize: 12,
                            marginRight: 5,
                            marginTop: 5,
                            color: "#F5365C",
                          }}
                        />
                        <p style={{ color: "#F5365C" }}>Eliminar</p>
                      </div>
                    ) : (
                      <div style={{ display: "flex" }}>
                        <PlusOutlined
                          style={{
                            fontSize: 12,
                            marginRight: 5,
                            marginTop: 5,
                            color: "#90C33C",
                          }}
                        />
                        <p style={{ color: "#90C33C" }}>Añadir</p>
                      </div>
                    )}
                  </div>
                );
              })}
            </div>
          </div>

          <div className="items_childrem">
            <h3>¿Tienes un cupón de decuento?</h3>
            <p>
              Los cupones sólo se aplican al precio de los poductos no incluye
              el precio del envío.
            </p>
            {descuento.descuento ? (
              <div className="cupon_aplied">Cupón aplicado con éxito</div>
            ) : (
              <div style={{ display: "flex" }}>
                <Input
                  placeholder="Añadir cupón"
                  onChange={(e) => setCupon(e.target.value)}
                  style={{ borderRadius: 100 }}
                />
                <Button
                  shape="round"
                  type="dashed"
                  style={{ marginLeft: 10, height: 50 }}
                  onClick={() => Crear_Orden()}
                >
                  Aplicar
                </Button>
              </div>
            )}
          </div>

          {autoshipping == "true" ? (
            <>
              <div className="items_childrem">
                <h3>Seguimiento en vivo no disponible</h3>
                <p>Este establecimiento entrega sus pedidos directamente.</p>
              </div>

              <div className="items_childrem">
                <h3>Datos de contacto compartido con la tienda</h3>
                <p>
                  Compartiremos tus datos con el establecimiento para realizar
                  la entrega.
                </p>
              </div>
            </>
          ) : null}

          <div className="items_childrem">
            <h3>Resumen del pedido</h3>
            <div style={{ display: "flex" }}>
              <div>
                <p>Subtotal:</p>
                <p>Descuento:</p>
                <p>Complementos extras:</p>
                <p>Propina:</p>
                <p>Envío:</p>
                <p>Tarifa de servicio:</p>
                <p>Total:</p>
              </div>
              <div style={{ marginLeft: "auto" }}>
                <p>{formaterPrice(subtotales, language, currency)}</p>
                <p>{descuentos}</p>
                <p>{formaterPrice(extrass, language, currency)}</p>
                <p>{formaterPrice(propinarider, language, currency)}</p>
                <p>{formaterPrice(envio, language, currency)}</p>
                <p>{formaterPrice(tarifa, language, currency)}</p>
                <p>{formaterPrice(totalFinal, language, currency)}</p>
              </div>
            </div>
          </div>
          <div className="items_childrem">
            <Query query={USER_DETAIL}>
              {(response) => {
                if (response) {
                  response.refetch();
                  const user =
                    response && response.data && response.data.getUsuario
                      ? response.data.getUsuario.data
                      : {};
                  return (
                    <>
                      <div style={{ display: "flex", marginBottom: 30 }}>
                        <h3>Tarjeta de crédito</h3>
                        <PlusCircleOutlined
                          onClick={() => showModal()}
                          style={{
                            marginLeft: "auto",
                            color: "#90C33C",
                            fontSize: 28,
                            cursor: "pointer",
                          }}
                        />
                      </div>
                      <Modal
                        title="Añadir tarjeta al wallet"
                        visible={show}
                        footer={false}
                        onCancel={hidenModal}
                      >
                        <div className="conten-anadir">
                          <CheckoutForm
                            user={user}
                            hidenModals={hidenModal}
                            setfromPaypal={setfromPaypal}
                          />
                        </div>
                      </Modal>

                      {!tokenCard ? (
                        <div style={{ textAlign: "center" }}>
                          <p style={{ marginTop: 0 }}>
                            Añade una tarjeta de crédito o débito para pagar
                          </p>
                        </div>
                      ) : (
                        <div
                          className="card__payment"
                          onClick={() => {
                            if (fromPaypal) {
                              setfromPaypal(false);
                              setCardNumber(tokenCard);
                              setCustomer(customers);
                            } else {
                              setCardNumber(tokenCard);
                              setCustomer(customers);
                            }
                          }}
                        >
                          <img src={images} alt={brand} />
                          <div
                            style={{
                              marginLeft: 15,
                              alignItems: "center",
                              marginTop: 20,
                            }}
                          >
                            <h3 style={{ lineHeight: 0 }}>
                              Termina en {cardLast4}
                            </h3>
                            <p style={{ marginBottom: -5 }}>{brand}</p>
                            <span style={{ color: "#90C33C" }}>Princial</span>
                          </div>
                          <div style={{ marginLeft: "auto" }}>
                            {tokenCard === cardNumber ? (
                              <CheckCircleFilled
                                style={{
                                  color: "#90C33C",
                                  fontSize: 18,
                                }}
                              />
                            ) : null}
                          </div>
                        </div>
                      )}
                    </>
                  );
                }
                return null;
              }}
            </Query>
          </div>

          <div className="items_childrem">
            <h3>Paypal</h3>
            <div
              className="card__payment"
              onClick={() => {
                if (cardNumber) {
                  setfromPaypal(true);
                  setCardNumber("");
                  setCustomer("");
                } else {
                  setfromPaypal(true);
                }
              }}
            >
              <img src={paypal} alt="Paypal" />
              <div
                style={{
                  marginLeft: 15,
                  alignItems: "center",
                  marginTop: 20,
                }}
              >
                <h3 style={{ lineHeight: 0 }}>Continua con Paypal</h3>
                <p style={{ lineHeight: 1 }}>Paypal</p>
              </div>
              <div style={{ marginLeft: "auto" }}>
                {fromPaypal ? (
                  <CheckCircleFilled
                    style={{
                      color: "#90C33C",
                      fontSize: 18,
                    }}
                  />
                ) : null}
              </div>
            </div>
          </div>

          <a
            onClick={() => {
              if (continuar()) {
                Guardar_Orden();
              } else {
                message.warning(
                  "Debes seleccionar una de tus tarjetas o añadir una"
                );
              }
            }}
            className={continuar() ? "btn-finaly" : "btn-finaly-inactive"}
          >
            {Loading ? (
              <Spin size="small" />
            ) : (
              `Pagar pedido total ${formaterPrice(
                totalFinal,
                language,
                currency
              )}`
            )}
          </a>
        </div>
      </div>
      <Modal
        title="Selecciona una opción"
        visible={shoModalOpcion}
        footer={false}
        onCancel={() => setShoModalOpcion(false)}
      >
        {setSelectedOpcion()}
      </Modal>
    </>
  );
}

export default withRouter(OrdenScreen);
