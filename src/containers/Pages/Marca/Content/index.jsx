import React from "react";
import "./index.css";
import { Query } from "react-apollo";
import { GET_MENU_MARCA, GET_PLATO } from "../../../GraphQL/query";
import { formaterPrice } from "../../../Utils/formatePrice";
import { StarFilled } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { scheduleTime } from "../../../Utils/scheduledTime";
import { message } from "antd";

const MainLogo = require("../../../Assets/images/icon.png");
const Android = require("../../../Assets/images/google.png");
const Apple = require("../../../Assets/images/apple.svg");

export default function Content(props) {
  const { res, city } = props;
  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  const isOK = () => {
    if (scheduleTime(res.schedule) && res.open && city) {
      return true;
    }
    return false;
  };

  return (
    <div className="content_marca">
      <Query query={GET_MENU_MARCA} variables={{ id: res._id }}>
        {(response) => {
          if (response.loading) {
            return null;
          }
          if (response) {
            const data =
              response && response.data && response.data.getMenumarca
                ? response.data.getMenumarca.list
                : [];
            return (
              <>
                <h2>{data.title}</h2>
                <div className="menu_cont">
                  <Query
                    query={GET_PLATO}
                    variables={{ id: data._id, page: 1, limit: 100 }}
                  >
                    {(response) => {
                      if (response.loading) {
                        return null;
                      }

                      if (response) {
                        const plato =
                          response && response.data && response.data.getPlato
                            ? response.data.getPlato.list
                            : [];

                        return (
                          <>
                            {plato &&
                              plato.map((data, i) => {
                                return (
                                  <div key={i}>
                                    <h3>
                                      {data.title}{" "}
                                      <StarFilled
                                        style={{ color: "#FFA500" }}
                                      />
                                    </h3>
                                    <p>{data.ingredientes}</p>
                                    <h4>
                                      {formaterPrice(
                                        data.price,
                                        language,
                                        currency
                                      )}
                                    </h4>
                                  </div>
                                );
                              })}
                          </>
                        );
                      }
                    }}
                  </Query>
                </div>
                <div className="App_download">
                  <img
                    src={MainLogo}
                    style={{ width: 60, marginBottom: 15, borderRadius: 15 }}
                  />
                  <div>
                    <p className="text">
                      Descárgate la app para ver la carta completa de{" "}
                      {res.title}.
                    </p>
                  </div>
                  <a
                    target="_blank"
                    href="https://play.google.com/store/apps/details?id=com.foodyapp"
                  >
                    <img
                      src={Android}
                      className="logos googl"
                      style={{ marginRight: 15 }}
                    />
                  </a>
                  <a
                    target="_blank"
                    href="https://apps.apple.com/es/app/wilbby-comida-a-domicilio/id1553798083"
                  >
                    <img src={Apple} className="logos googl" />
                  </a>
                </div>
                <div className="foot">
                  <Link to={isOK() ? `/store/${city}/${res.slug}` : "/"}>
                    Ver Carta completa
                  </Link>
                </div>
              </>
            );
          }
        }}
      </Query>
    </div>
  );
}
