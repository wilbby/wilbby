import React, { useEffect } from "react";
import "./index.css";
import Header from "../../../Components/HeaderTransparent";
import { StarFilled } from "@ant-design/icons";
import Search from "../search/seacrh";
import Content from "../Content";

export default function Hero(props) {
  const { res, city, adress } = props;

  useEffect(() => {
    document.title = `${res.title}`;
  }, []);

  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
  res.Valoracion.forEach((start) => {
    if (start.value === 1) rating["1"] += 1;
    else if (start.value === 2) rating["2"] += 1;
    else if (start.value === 3) rating["3"] += 1;
    else if (start.value === 4) rating["4"] += 1;
    else if (start.value === 5) rating["5"] += 1;
  });

  const ar =
    (5 * rating["5"] +
      4 * rating["4"] +
      3 * rating["3"] +
      2 * rating["2"] +
      1 * rating["1"]) /
    res.Valoracion.length;
  let averageRating = 0;
  if (res.Valoracion.length) {
    averageRating = Number(ar.toFixed(1));
  }

  return (
    <div className="heroMarca">
      <Header fromHome={true} />
      <div className="hero_row">
        <div className="hero_row_1">
          <div className="info_marca">
            <h1>{res.title}</h1>
            <p>
              {res.categoryName} · {res.type} ·{" "}
              <StarFilled style={{ color: "#FFA500" }} />
              {averageRating.toFixed(1)} ({res.Valoracion.length} +)
              Puntuaciones
            </p>
            <p>{res.description}</p>
          </div>
        </div>
        <div
          className="hero_row_2"
          style={{
            backgroundImage: `url(${res.image})`,
          }}
        ></div>
      </div>
      <Search adress={adress} city={city} res={res} />
      <Content res={res} city={city} />
    </div>
  );
}
