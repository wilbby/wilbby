import React from "react";
import "./index.css";
import { EnvironmentOutlined, FlagOutlined } from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { scheduleTime } from "../../../Utils/scheduledTime";
import { message } from "antd";

function Seacrh(props) {
  const { res, city, adress, history } = props;

  const isOK = () => {
    if (scheduleTime(res.schedule) && res.open && city) {
      return true;
    }
    return false;
  };

  return (
    <div className="search_cont">
      <div className="serc">
        <h3>Pedir a una dirección reciente</h3>
        <button
          onClick={() => {
            if (isOK()) {
              history.push(`/store/${city}/${res.slug}`);
            } else {
              message.warning(
                "Tienda cerrada te avisaremos cuendo vuelva a estar disponible"
              );
            }
          }}
        >
          <EnvironmentOutlined style={{ color: "#90C33C" }} />{" "}
          {adress ? adress : city}
        </button>
      </div>
      <div className="serc">
        <h3>Ver cerca de ti en {city}</h3>
        <button
          onClick={() => {
            if (isOK()) {
              history.push(`/store/${city}/${res.slug}`);
            } else {
              message.warning(
                "Tienda cerrada te avisaremos cuendo vuelva a estar disponible"
              );
            }
          }}
        >
          <FlagOutlined style={{ color: "#90C33C" }} /> {city}
        </button>
      </div>
    </div>
  );
}

export default withRouter(Seacrh);
