import React from "react";
import Lottie from "react-lottie";
import { withRouter } from "react-router-dom";
import Header from "../../../Components/Header";
import GetApp from "../../../Components/GetApp";

const nodata = require("../../Home/chica.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

function Nodata(props) {
  const { history } = props;
  return (
    <div>
      <div>
        <GetApp />
        <Header />
        <div className="no_city_con">
          <Lottie options={defaultOptionsBell} height={300} width={300} />
          <h1>Página No Encontrada</h1>
          <p>
            Lo Sentimos, Pero La Página Que Estás Buscando No Existe. ¡Pero Hay
            Mucho Más Que Ver!
          </p>
          <button onClick={() => history.push("/")}>Volver al inicio</button>
        </div>
      </div>
    </div>
  );
}

export default withRouter(Nodata);
