import React from "react";
import Hero from "./Hero";
import "./index.css";
import { Query } from "react-apollo";
import { RESTAURANT_SLUG } from "../../GraphQL/query";
import NoData from "./Nodata";
import { Spin } from "antd";

export default function Marca(props) {
  const city = localStorage.getItem("city");
  const adress = localStorage.getItem("adressName");
  const slug = props.match.params.slug;
  return (
    <div className="marca_cont">
      <Query query={RESTAURANT_SLUG} variables={{ slug: slug }}>
        {(response) => {
          if (response.loading) {
            return (
              <div className="spin">
                <Spin size="large" />
              </div>
            );
          }
          if (response) {
            const res =
              response && response.data && response.data.getRestaurantForSlugWeb
                ? response.data && response.data.getRestaurantForSlugWeb.data
                : {};
            return (
              <div>
                {res && res._id ? (
                  <Hero res={res} city={city} adress={adress} />
                ) : (
                  <NoData />
                )}
              </div>
            );
          }
        }}
      </Query>
    </div>
  );
}
