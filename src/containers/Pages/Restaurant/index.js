import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { Query } from "react-apollo";
import { EnvironmentOutlined } from "@ant-design/icons";
import { RESTAURANT_SEARCH } from "../../GraphQL/query";
import Cats from "../../Components/SliderCategory/CatText";
import CardRestaurant from "../../Components/CardRestaurant";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import Offert from "./Offert";
import Tipo from "../../Components/SliderCategory/tipo";
import TipoTienda from "../../Components/SliderCategory/tipoTienda";
import Lottie from "react-lottie";
import "./index.css";

const nodata = require("../Home/chica.json");

const defaultOptionsBell = {
  loop: true,
  autoplay: true,
  animationData: nodata,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

const Nodata = require("../../Assets/images/nodata.png");

function Restaurant(props) {
  const busqueda = props.match.params.search;
  const categoria = props.match.params.category;
  const city = props.match.params.city;

  const [search, setSearch] = useState(busqueda);
  const [cat, setCat] = useState(categoria ? categoria : "");
  const [catitle, setCattitle] = useState("Restaurantes");
  const [tipo, setTipo] = useState("");

  const deleteFilter = () => {
    setSearch("");
    setCat("");
  };

  useEffect(() => {
    document.title = `${catitle} en ${city}`;
  }, []);

  return (
    <>
      <GetApp />
      <Header fromHome={false} />
      <div className="constainer__restaurant">
        <div className="constainer__restaurant_childern">
          <div className="nav_bar_searc">
            <div className="locations sea">
              <div className="locations_active">
                <EnvironmentOutlined style={{ marginRight: 2 }} />
                <p>{city}</p>
              </div>
            </div>
            <div className="search_bar">
              <input
                type="text"
                placeholder="Busca lo que quieras"
                onChange={(e) => setSearch(e.target.value)}
                defaultValue={search}
              />
              <button onClick={() => {}}>Buscar</button>
            </div>
          </div>
          {/* <Offert city={city} /> */}
          <Cats
            onClick={(value) => {
              setCat(value._id);
              setCattitle(value.title);
            }}
            category={cat}
          />
          {catitle === "Restaurantes" ? (
            <Tipo
              onClick={(value) => {
                if (tipo) {
                  setTipo("");
                  if (tipo != value) {
                    setTipo(value);
                  }
                } else {
                  setTipo(value);
                }
              }}
              tipo={tipo}
            />
          ) : null}

          {catitle === "Tiendas" ? (
            <TipoTienda
              onClick={(value) => {
                if (tipo) {
                  setTipo("");
                  if (tipo != value) {
                    setTipo(value);
                  }
                } else {
                  setTipo(value);
                }
              }}
              tipo={tipo}
            />
          ) : null}

          <h3>
            {catitle} en <span style={{ color: "#90C33C" }}>{city}</span>
          </h3>
          <Query
            query={RESTAURANT_SEARCH}
            variables={{
              search: search,
              city: city,
              category: cat,
              tipo: tipo,
            }}
          >
            {(response) => {
              if (response.loading) {
                return (
                  <div className="container__cards">
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                    <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                      <div>
                        <div>
                          <Skeleton width="100%" height={130} />
                        </div>
                        <p>
                          <Skeleton count={1} width="100%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="60%" />
                        </p>
                        <p>
                          <Skeleton count={1} width="40%" />
                        </p>
                      </div>
                    </SkeletonTheme>
                  </div>
                );
              }

              if (response) {
                const res =
                  response && response.data && response.data.getRestaurantSearch
                    ? response.data.getRestaurantSearch.data
                    : [];
                return (
                  <>
                    <div
                      className={
                        res.length === 0
                          ? "container__cards-all"
                          : "container__cards"
                      }
                    >
                      {res.length === 0 ? (
                        <div className="nodata">
                          <Lottie
                            options={defaultOptionsBell}
                            height={200}
                            width={200}
                          />
                          <h1>
                            No hay tiendas que coincidan con tus preferencias
                          </h1>
                          <p>Inténtalo con uno o dos filtros menos</p>
                          <button onClick={() => deleteFilter()}>
                            Eliminar filtros
                          </button>
                        </div>
                      ) : null}

                      {res.map((da, i) => {
                        return (
                          <CardRestaurant
                            key={i}
                            res={da}
                            refetch={response.refetch}
                            city={city}
                          />
                        );
                      })}
                    </div>
                  </>
                );
              }
            }}
          </Query>
        </div>
      </div>
    </>
  );
}

export default withRouter(Restaurant);
