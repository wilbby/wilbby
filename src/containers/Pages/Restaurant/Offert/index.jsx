import React from "react";
import { OFFERT } from "../../../GraphQL/query";
import { Query } from "react-apollo";
import "./index.css";
import { validateDates } from "../../../Utils/calculateDate";
import { message } from "antd";
import { withRouter } from "react-router-dom";

function Offert(props) {
  const { city, history } = props;
  return (
    <div className="offer_contents">
      <Query query={OFFERT} variables={{ city: city }}>
        {(response) => {
          if (response.loading) {
            return null;
          }
          if (response) {
            response.refetch();
            const data =
              response && response.data && response.data.getOfferts
                ? response.data.getOfferts.data
                : [];
            return (
              <>
                {data &&
                  data.map((o, i) => {
                    const a = o.apertura;
                    const b = o.cierre;
                    const isOK = () => {
                      if (validateDates(a, 0, b, 0) && o.open && city) {
                        return true;
                      }
                      return false;
                    };

                    return (
                      <div
                        key={i}
                        onClick={() =>
                          isOK()
                            ? history.push(`/store/${city}/${o.slug}`)
                            : message.warning(
                                "Tienda cerrada te avisaremos cuando vuleva a abrir"
                              )
                        }
                      >
                        <img src={o.imagen} alt="Offert" />
                      </div>
                    );
                  })}
              </>
            );
          }
        }}
      </Query>
    </div>
  );
}

export default withRouter(Offert);
