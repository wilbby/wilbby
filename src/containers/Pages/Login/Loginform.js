import React, { Component, Fragment } from "react";
import { Input, Checkbox, message, Spin } from "antd";
import "./Login.css";
import { AUTENTICAR_USUARIO } from "../../GraphQL/mutation";
import { Mutation } from "react-apollo";
import { withRouter } from "react-router-dom";
import SocialLogin from "./socialLogin";
import ReCAPTCHA from "react-google-recaptcha";

const dataLogin = {
  date: "",
  divice: "",
  system: "",
  location: "",
  IPaddress: "",
};

const initialState = {
  email: "",
  password: "",
};

const TEST_SITE_KEY = "6LesEuAZAAAAADNLYIYpkm_hdF9zIMvRhBupo4ou";
const DELAY = 1000;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      dataLogin,
      callback: "not fired",
      value: "",
      load: false,
      expired: "false",
    };

    this._reCaptchaRef = React.createRef();
  }

  async componentDidMount() {
    setTimeout(() => {
      this.setState({ load: true });
    }, DELAY);

    var navInfo = window.navigator.appVersion.toLowerCase();
    var so = "Sistema Operativo";
    function retornarSO() {
      if (navInfo.indexOf("win") != -1) {
        so = "Windows";
      } else if (navInfo.indexOf("linux") != -1) {
        so = "Linux";
      } else if (navInfo.indexOf("mac") != -1) {
        so = "macOS";
      }
      return so;
    }

    retornarSO();

    const ip = await fetch(`https://api.ipify.org/?format=json`);
    var ips = await ip.json();

    var date = new Date();
    var divice = navigator.platform;
    var location = navigator.language;
    var system = so;
    var IPaddress = ips.ip;

    const datos = {
      date,
      divice,
      location,
      system,
      IPaddress,
    };

    this.setState({ dataLogin: datos });
  }

  handleChange = (value) => {
    this.setState({ value });
    // if value is null recaptcha expired
    if (value === null) this.setState({ expired: "true" });
  };

  asyncScriptOnLoad = () => {
    this.setState({ callback: "called!" });
  };

  actualizarState = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  limpiarState = () => {
    this.setState({ ...initialState });
  };

  onCompletedLogin = async (res) => {
    if (!res.autenticarUsuario.success) {
      message.error(res.autenticarUsuario.message);
    } else {
      localStorage.setItem("token", res.autenticarUsuario.data.token);
      localStorage.setItem("id", res.autenticarUsuario.data.id);

      await this.props.refetch();

      // Limpiar state
      this.limpiarState();
      this.setState({ id: res.autenticarUsuario.data.id });

      const params = new URLSearchParams(this.props.location.search);
      const store = params.get("store");
      const city = params.get("city");
      console.log(params);
      if (res.autenticarUsuario.data.verifyPhone) {
        if (store) {
          message.success(res.autenticarUsuario.message);
          this.props.history.push(`/store/${city}/${store}`);
        } else {
          message.success(res.autenticarUsuario.message);
          this.props.history.push("/profile");
        }
      } else {
        this.props.history.push(
          `/verify-phone/${res.autenticarUsuario.data.id}`
        );
      }
    }
  };

  iniciarSesion = (e, usuarioAutenticar) => {
    e.preventDefault();

    usuarioAutenticar().then(async ({ data: res }) => {
      if (res.autenticarUsuario.success) {
        localStorage.setItem("token", res.autenticarUsuario.data.token);
        localStorage.setItem("id", res.autenticarUsuario.data.id);

        // Limpiar state
        this.limpiarState();

        // Redireccionar el contenido
        const params = new URLSearchParams(this.props.location.search);
        const store = params.get("store");
        const city = params.get("city");
        if (store) {
          message.success(res.autenticarUsuario.message);
          this.props.history.push(`/store/${city}/${store}`);
        } else {
          message.success(res.autenticarUsuario.message);
          this.props.history.push("/profile");
        }
      } else {
        message.error(res.autenticarUsuario.message);
      }
    });
  };

  logon = async (email, password) => {
    this.setState(
      {
        email: email,
        password: password,
        value: "token validado",
      },
      () => {
        document.getElementById("loginSubmit").click();
      }
    );
  };
  validarForm = () => {
    const { email, password, value } = this.state;

    const noValido = !email || !password || !value;
    return noValido;
  };

  render() {
    const { email, password } = this.state;
    const { load } = this.state || {};
    return (
      <div className={this.props.fromSignup ? "" : "register-page-content"}>
        <Fragment>
          <Mutation
            mutation={AUTENTICAR_USUARIO}
            variables={{ email, password, input: this.state.dataLogin }}
          >
            {(usuarioAutenticar, { loading }) => {
              if (loading) {
                return (
                  <div className={this.props.fromSignup ? "" : "page-loader"}>
                    <Spin size={this.props.fromSignup ? "" : "large"}></Spin>
                  </div>
                );
              }

              return (
                <div className={this.props.fromSignup ? "" : "containerLogin"}>
                  <div className={this.props.fromSignup ? "" : "contLogin"}>
                    <form
                      onSubmit={(e) => this.iniciarSesion(e, usuarioAutenticar)}
                      className="col-md-8"
                      style={{
                        display: this.props.fromSignup ? "none" : "block",
                      }}
                    >
                      <p className="psol">Iniciar sesión</p>

                      <div className="form-group">
                        <Input
                          onChange={this.actualizarState}
                          value={email}
                          type="text"
                          name="email"
                          className="form-control"
                          placeholder="Correo electrónico"
                        />
                      </div>
                      <div className="form-group">
                        <Input.Password
                          onChange={this.actualizarState}
                          value={password}
                          type="password"
                          name="password"
                          className="form-control"
                          placeholder="Contraseña"
                        />
                      </div>

                      {load && (
                        <ReCAPTCHA
                          style={{ display: "inline-block" }}
                          ref={this._reCaptchaRef}
                          sitekey={TEST_SITE_KEY}
                          onChange={this.handleChange}
                          asyncScriptOnLoad={this.asyncScriptOnLoad}
                        />
                      )}

                      <div className="forgot">
                        <Checkbox>Recordarme</Checkbox>
                        <a
                          className="login-form-forgot"
                          href="/forgot-password"
                        >
                          ¿Lo olvidaste?
                        </a>
                      </div>
                      <div className="form-group">
                        <button
                          disabled={loading || this.validarForm()}
                          type="submit"
                          className="btn-btn-primary"
                          id="loginSubmit"
                          style={{
                            color: this.validarForm() ? "gray" : "white",
                          }}
                        >
                          Iniciar sesión
                        </button>
                      </div>

                      <div>
                        ¿No tienes una cuenta?{" "}
                        <a className="forgot" href="/register">
                          Regístrate ahora!
                        </a>
                      </div>
                    </form>

                    <SocialLogin logon={this.logon} fromSignup={true} />
                  </div>
                </div>
              );
            }}
          </Mutation>
        </Fragment>
      </div>
    );
  }
}

export default withRouter(Login);
