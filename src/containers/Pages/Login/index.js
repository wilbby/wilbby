import React, { useEffect } from "react";
import NormalLoginForm from "./Loginform";
import Header from "../../Components/Header";
import GetApp from "../../Components/GetApp";
import "./Login.css";

function LoginClient() {
  useEffect(() => {
    document.title = "Iniciar sesión";
  }, []);

  return (
    <>
      <GetApp />
      <Header />
      <div className="containers">
        <div className="Containerform">
          <NormalLoginForm />
        </div>
      </div>
    </>
  );
}

export default LoginClient;
