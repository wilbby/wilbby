import React, { Component } from "react";
import "./socialLogin.css";
import { Redirect, withRouter } from "react-router-dom";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GoogleLogin } from "react-google-login";
import {
  FACEBOOK_APP_ID,
  GOOGLE_CLIENT_ID,
  LOCAL_API_URL,
} from "../../Utils/UrlConfig";
import {
  FacebookFilled,
  GoogleOutlined,
  TwitterOutlined,
} from "@ant-design/icons";
import AppleSignin from "react-apple-signin-auth";
import TwitterLogin from "react-twitter-auth";
import { message } from "antd";
var sha256 = require("js-sha256");

class ScocialLogin extends Component {
  state = {
    isAuthenticated: false,
    user: null,
    token: "",
  };

  onFailure = (error) => {
    console.log(error);
  };

  twitterResponse = (response) => {
    const token = response.headers.get("x-auth-token");
    response.json().then((user) => {
      if (user.token) {
        this.props.logon(user.nuevoUsuario.email, user.token);
      }
    });
  };

  facebookResponse = (response) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    fetch(`${LOCAL_API_URL}/api/v1/auth/facebook`, options).then((r) => {
      //const token = r.headers.get("x-auth-token");
      r.json().then(async (user) => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  googleResponse = (response) => {
    const tokenBlob = new Blob(
      [JSON.stringify({ access_token: response.accessToken }, null, 2)],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      mode: "cors",
      cache: "default",
    };
    fetch(`${LOCAL_API_URL}/api/v1/auth/google`, options).then((r) => {
      console.log(r);
      r.json().then((user) => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        }
      });
    });
  };

  appleResponse = (response) => {
    const tokenBlob = new Blob(
      [
        JSON.stringify(
          {
            id_token: response.authorization.id_token,
            user: response.user ? response.user : {},
          },
          null,
          2
        ),
      ],
      { type: "application/json" }
    );
    const options = {
      method: "POST",
      body: tokenBlob,
      cache: "default",
    };
    fetch(`${LOCAL_API_URL}/api/auth/apple-web`, options).then((res) => {
      res.json().then(async (user) => {
        if (user.token) {
          this.props.logon(user.nuevoUsuario.email, user.token);
        } else {
          message.warning("Algo va mal intentalo de nuevo");
        }
      });
    });
  };

  render() {
    if (this.state.isAuthenticated) {
      return <Redirect to="/profile" />;
    } else
      return (
        <div className="contLogin1">
          <AppleSignin
            style={{
              marginBottom: 15,
              cursor: "pointer",
              backgroundColor: "black",
              width: "100%",
              padding: 5,
              borderRadius: 10,
            }}
            authOptions={{
              clientId: "wilbby.web",
              redirectURI: "https://wilbby.com/login",
              scope: "email name",
              state: "state",
              nonce: sha256("nonce"),
              usePopup: true,
            }}
            onSuccess={this.appleResponse}
          />

          <FacebookLogin
            appId={FACEBOOK_APP_ID}
            autoLoad={false}
            fields="name,email,picture"
            callback={this.facebookResponse}
            render={(renderProps) => (
              <div className="socia" onClick={renderProps.onClick}>
                <button className="btn-twitter">
                  <FacebookFilled style={{ marginRight: 10, marginTop: 5 }} />
                  <span> Continua con facebook</span>
                </button>
              </div>
            )}
          />
          <GoogleLogin
            clientId={GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={this.googleResponse}
            onFailure={this.onFailure}
            render={(renderProps) => (
              <div
                className="socia"
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                <button className="btn-twitter">
                  <GoogleOutlined style={{ marginRight: 10, marginTop: 5 }} />
                  <span> Continua con Google</span>
                </button>
              </div>
            )}
          />
          <TwitterLogin
            loginUrl={`${LOCAL_API_URL}/api/v1/auth/twitter`}
            onFailure={this.onFailure}
            onSuccess={this.twitterResponse}
            requestTokenUrl={`${LOCAL_API_URL}/api/v1/auth/twitter/reverse`}
            style={{ background: "transparent", border: "none", width: "100%" }}
          >
            <div className="socia">
              <button className="btn-twitter">
                <TwitterOutlined style={{ marginRight: 10, marginTop: 5 }} />
                <span> Continua con Twitter</span>
              </button>
            </div>
          </TwitterLogin>
        </div>
      );
  }
}

export default withRouter(ScocialLogin);
