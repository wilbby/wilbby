import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";
import { LinkedinOutlined, TwitterOutlined } from "@ant-design/icons";
import Page4 from "../../Home/page4";

import "../index.css";

const leudy = require("./leudy.jpeg");

export default function Team() {
  useEffect(() => {
    document.title = "Nuestro equipo";
  }, []);
  return (
    <div className="containers_team">
      <Barner
        title="Nuestro equipo"
        subtitle="Detrás de Wilbby hay un gran equipo de personas que quieren revolucionar el sector de hostelero con una herramienta efectiva y práctica."
      />
      <div className="team_childrem">
        <div className="team_childrem_item">
          <div></div>
          <div>
            <img src={leudy} alt="Leudy Martes" />
            <h1>Leudy Martes</h1>
            <p>CEO Founder at Wilbby </p>
            <a
              href="https://www.linkedin.com/in/leudymartes/"
              style={{ marginRight: 10 }}
            >
              <LinkedinOutlined
                style={{
                  fontSize: 30,
                  color: "#90C33C",
                  paddingLeft: 20,
                  cursor: "pointer",
                }}
              />
            </a>

            <a
              href="https://twitter.com/leudymartes"
              style={{ marginLeft: 10 }}
            >
              <TwitterOutlined
                style={{ fontSize: 30, color: "#90C33C", cursor: "pointer" }}
              />
            </a>
          </div>
          <div></div>
        </div>
      </div>
      <Page4 />
    </div>
  );
}
