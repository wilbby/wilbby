import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";

import "../index.css";

export default function TermAndCondition() {
  useEffect(() => {
    document.title = "Términos y condiciones";
  }, []);
  return (
    <div>
      <Barner title="Términos y condiciones de uso de Wilbby" />
      <div className="containers_hepls">
        <div className="cookies">
          <h1>Condiciones Generales de Uso y Contratación</h1>
          <p>Última actualización: 24 de febrero 2021</p>
          <p>
            Las presentes condiciones generales de uso e información legal (en
            adelante, Condiciones Generales) se aplican al sitio web de
            Locatefit, S.L. (en adelante, Wilbby), cuyo dominio es
            www.wilbby.com y a la correspondiente aplicación móvil, así como a
            todos sus sitios relacionados o vinculados desde www.wilbby.com por
            Wilbby, así como a sus afiliadas y a asociados, según establecido en
            el Anexo I, incluidos los sitios web de Wilbby en todo el mundo (en
            adelante y de forma conjunta, el Sitio). El sitio es propiedad de
            Wilbby. Al utilizar el sitio, muestras tu conformidad y expresa
            aceptación a las presentes condiciones de uso. Si no estás de
            acuerdo, te rogamos que te abstengas de utilizarlo.
          </p>
          <p>
            A través de las presentes Condiciones Generales, Wibby pone a
            disposición de los usuarios (en adelante Usuario o Usuarios) el
            sitio web y la aplicación móvil Wilbby (en adelante denominadas
            conjuntamente, la Plataforma).
          </p>
          <p>
            En cumplimiento de lo establecido en la normativa reguladora, se
            exponen los siguientes datos identificativos del titular del sitio:
          </p>
          <ul>
            <li>
              <p>Denominación social: Locatefit, S.L.</p>
            </li>
            <li>
              <p>
                Domicilio social: C/ Francisco Sarmiento 13 8A, 09005 Burgos.
              </p>
            </li>
            <li>
              <p>
                Datos de inscripción en el Registro Mercantil: Protocolo
                2019/2502, Tomo: 780, Libro: 571, Folio: 210, Hoja: BU 18450,
                Inscripción: 1.
              </p>
            </li>
            <li>
              <p>Número de Identificación Fiscal (NIF): B09603440</p>
            </li>
          </ul>
          <h2>1. Objeto</h2>
          <p>
            Wilbby es una compañía tecnológica, cuya actividad principal es el
            desarrollo y gestión de una plataforma tecnológica mediante la que a
            través de una aplicación móvil o de una web (en adelante, la APP)
            permite a determinadas tiendas locales de algunas ciudades en
            diferentes territorios ofertar sus productos y/o servicios a través
            de la misma, y en su caso, si los Usuarios de la APP y consumidores
            de las citadas tiendas locales así lo solicitan a través de la APP,
            de forma accesoria, intermedia en la entrega inmediata o planificada
            de los productos.
          </p>
          <p>
            Wilbby ha desarrollado una Plataforma mediante la cual diferentes
            comercios, con los que Wilbby puede mantener un acuerdo comercial
            por el uso de la plataforma, ofrecen una serie de productos y/o
            servicios. El Usuario tiene la posibilidad de solicitar la
            adquisición y/o recogida de productos y servicios de estos
            comercios, de forma presencial, mediante el mandato que confiere a
            un tercero al solicitar un pedido a través de la Plataforma, en
            cuyos casos Wilbby actúa como mero intermediario y por lo tanto, no
            puede asumir ni asume responsabilidad alguna sobre la calidad de los
            productos o la correcta prestación de los servicios ofrecidos
            directamente por los comercios ni por dichos terceros.
          </p>
          <p>
            Asimismo, Wilbby es una Plataforma Tecnológica multicategoría de
            intermediación en la contratación telemática de servicios “on
            demand". La misma pretende facilitar que aquellas personas que
            necesiten ayuda con sus recados, servicios, compras presenciales y/o
            recogida de productos en comercios locales (en adelante,
            “Usuarios”), puedan realizar sus recados mediante y delegando en
            dichos terceros dispuestos a llevar a cabo voluntariamente el
            mandato que le confieran los Usuarios (en adelante, “Mandatarios”).
          </p>
          <p>
            Los Mandatarios por lo tanto, son una red de profesionales
            independientes que colaboran con Wilbby, cuando éstos están
            interesados en realizar la prestación de sus servicios, se conectan
            a la Plataforma de Wilbby y en un tiempo determinado se comprometen
            a realizar el servicio que le encomiende el Usuario mediante el
            mencionado mandato. Como consecuencia de ello, Wilbby no puede
            asumir la responsabilidad de los plazos de entrega en tanto
            dependerán del buen hacer de las tiendas locales, en la preparación
            de los pedidos, de los propios Mandatarios, así como de la
            información facilitada por los propios Usuarios al realizar el
            pedido o de su disponibilidad y respuesta a la hora de su entrega. A
            estos efectos, los plazos de entrega indicados a través del Sitio
            son meramente orientativos.
          </p>
          <p>
            Wilbby confirma que, para el desarrollo de su actividad comercial,
            ha obtenido todas las licencias necesarias en los países donde
            opera.
          </p>
          <p>
            A lo largo de las presentes Condiciones Generales de Uso nos
            referiremos a Usuarios y Mandatarios como Usuarios de la Plataforma.
          </p>
          <h2>2. Términos de Uso</h2>
          <p>
            El acceso a la Plataforma, y la creación voluntaria de un perfil por
            parte del Usuario, implica el conocimiento y la aceptación expresa e
            inequívoca de las presentes Condiciones Generales de Uso y
            Contratación y la Política de Privacidad y la Política de Cookies
            por parte de todos los Usuarios.
          </p>
          <h2>3. Acceso y Registro para Usuarios</h2>
          <p>
            Para poder ser Usuario de la Plataforma es indispensable que se
            cumplan los siguientes requisitos:
          </p>
          <ul>
            <li>
              <p>Haber cumplido o ser mayor de 18 años de edad.</p>
            </li>
            <li>
              <p>
                Cumplimentar de manera veraz los campos obligatorios del
                formulario de registro, en el que se solicitan datos de carácter
                personal como nombre de Usuario, correo electrónico, número de
                teléfono y número de tarjeta bancaria.
              </p>
            </li>
            <li>
              <p>Aceptar las presentes Condiciones de Uso y Contratación.</p>
            </li>
            <li>
              <p>Aceptar la Política de Privacidad y Protección de Datos.</p>
            </li>
            <li>
              <p>Aceptar la Política de Cookies.</p>
            </li>
          </ul>
          <p>
            El Usuario garantiza que todos los datos sobre su identidad y
            legitimidad facilitados a Wilbby en sus formularios de registro de
            la Plataforma son veraces, exactos y completos. Asimismo, se
            compromete a mantener actualizados sus datos.
          </p>
          <p>
            En el supuesto en que el Usuario facilite cualquier dato falso,
            inexacto o incompleto o si Wilbby considera que existen motivos
            fundados para dudar sobre la veracidad, exactitud e integridad de
            los mismos, Wilbby podrá denegarle el acceso y uso presente o futuro
            de la Plataforma o de cualquiera de sus contenidos y/o servicios.
          </p>
          <p>
            Al darse de alta en la Plataforma, el Usuario seleccionará un nombre
            de Usuario (username) y una clave de acceso (password). Tanto el
            username como el password son estrictamente confidenciales,
            personales e intransferibles. Wilbby recomienda no utilizar las
            mismas credenciales de acceso que en otras plataformas para aumentar
            la seguridad de las cuentas. En el supuesto que el Usuario utilice
            las mismas credenciales de acceso que en otras plataformas, Wilbby
            no podrá garantizar la seguridad de la cuenta ni asegurar que el
            Usuario es el único que accede a su perfil.
          </p>
          <p>
            El Usuario se compromete a no divulgar los datos relativos a su
            cuenta ni hacerlos accesibles a terceros. El Usuario será el único
            responsable en caso de uso de dichos datos o de los servicios del
            Sitio por terceros, incluidas las manifestaciones y/o contenidos
            vertidos en la Plataforma, o cualquier otra actuación que se lleve a
            cabo mediante el uso del username y/o password.
          </p>
          <p>
            El Usuario será el único propietario sobre el contenido vertido por
            el mismo en la Plataforma. Asimismo, a través del registro en la
            Plataforma y la aceptación de las presentes Condiciones, el Usuario
            concede a Wilbby, en relación con el contenido que pueda proveer,
            una licencia mundial, irrevocable, transferible, gratuita, con
            derecho a sublicencia, uso, copia, modificación, creación de obras
            derivadas, distribución, comunicación pública así como su
            explotación en cualquier forma que Wilbby considere apropiada, con o
            sin comunicación ulterior al Usuario y sin la necesidad de liquidar
            cantidad alguna por los mencionados usos.
          </p>
          <p>
            Wilbby no puede garantizar la identidad de los Usuarios registrados,
            por lo tanto, no será responsable del uso de la identidad de un
            Usuario registrado por terceros no registrados. Los Usuarios se
            obligan a poner inmediatamente en conocimiento de Wilbby la
            sustracción, divulgación o pérdida de sus credenciales,
            comunicándolo a través de los canales de comunicación ofrecidos por
            Wilbby.
          </p>
          <p> 3.1 Perfil</p>
          <p>
            Para poder completar el registro en la Plataforma el Usuario deberá
            proporcionar algunos datos como: nombre de Usuario, dirección de
            correo electrónico, teléfono, datos de la tarjeta bancaria, etc. Una
            vez completado el registro, todo Usuario podrá acceder a su perfil y
            completarlo, editarlo según estime conveniente. Wilbby no almacena
            los datos de pago de los usuarios, que serán tratados y almacenados
            por el prestador de servicios de pago según descrito en las
            presentes Condiciones y en la Política de Privacidad.
          </p>
          <p>3.2. Robo o sustracción de la tarjeta de crédito</p>
          <p>
            En tanto Wilbby no puede garantizar la identidad de los Usuarios
            registrados, los Usuarios tendrán la obligación de informar a Wilbby
            en aquellos supuestos en que tengan constancia que la tarjeta de
            crédito asociada a su perfil de Wilbby hubiera sido robada, y/o se
            estuviera utilizando por un tercero de forma fraudulenta. Por ello,
            toda vez que Wilbby y su plataforma de pago velan proactivamente por
            la protección de los Usuarios con las correspondientes medidas de
            seguridad, para el caso en que el Usuario no informara a Wilbby de
            dicha sustracción, Wilbby no será responsable del uso fraudulento
            que puedan hacer terceros de la cuenta del Usuario. Wilbby invita a
            los Usuarios, en caso de robo, sustracción o sospecha de un mal uso
            de su tarjeta de crédito a realizar la correspondiente denuncia ante
            la autoridad policial.
          </p>
          <p>
            Wilbby adquiere el compromiso de asistir al Usuario y colaborar con
            las autoridades competentes, si así fuera necesario, para facilitar
            la prueba fehaciente del cargo indebido. En caso de fraude, Wilbby
            se reserva el derecho a interponer las acciones que en su caso
            fueran menester cuando resulte perjudicado por el uso indebido del
            Sitio.
          </p>
          <p>4. Funcionamiento del servicio. Condiciones del Mandato.</p>
          <p>
            Cuando un Usuario registrado como Usuario necesite ayuda con un
            recado, deberá dirigirse a la plataforma web o a la correspondiente
            aplicación de Wilbby y solicitar el servicio mediante dichos medios
            telemáticos. El servicio de recadería básico consiste en la recogida
            de un producto y su posterior entrega, ambas direcciones
            establecidas por el Usuario siempre que se encuentren exclusivamente
            dentro del territorio de actuación de Wilbby. Asimismo, el Usuario
            puede solicitar al Mandatario que compre presencialmente productos
            en su nombre, los recoja y los entregue en las direcciones
            consignadas, a través de mandato el Mandatario se compromete a
            adquirir los productos encomendados por el Usuario en su nombre, y
            según las indicaciones y especificaciones brindadas por este último.
          </p>
          <p>
            El Usuario es el único responsable en la correcta consignación de
            las direcciones de entrega y recogida en la Plataforma, por lo que
            exime a Wilbby y al Mandatario de cualquier negligencia o error en
            la recogida o entrega del pedido derivada de la consignación errónea
            de las direcciones de entrega y recogida. Como consecuencia de ello,
            será el Usuario quien deba asumir el coste derivado de la incorrecta
            consignación de las direcciones de entrega y recogida en la
            Plataforma.
          </p>
          <p>
            El Usuario deberá proporcionar toda la información, lo más detallada
            posible, respecto al servicio objeto del recado, y en su caso, en
            relación con el producto que solicite comprar al Mandatario en su
            nombre en las tiendas físicas. Para ello, podrá introducir los
            comentarios que considere útiles en el apartado “comentarios”, así
            como en su caso, podrá compartir con el Mandatario una fotografía
            para identificar el pedido. El Usuario tendrá comunicación constante
            con el Mandatario, pudiéndose dirigir en todo momento al Mandatario
            a los efectos de que éste ejecute el mandato conferido según las
            propias indicaciones del Usuario.
          </p>
          <p>
            A efectos de facilitar la comunicación directa con el Mandatario y
            en el supuesto que exista alguna incidencia en la tramitación del
            encargo y/o para comunicar algún cambio en el mismo, Wilbby pone a
            disposición de los Usuarios un chat interno que permitirá el
            contacto directo e inmediato entre Usuario y Mandatario durante la
            ejecución del pedido. El chat dejará de estar activo en el momento
            en que el Usuario haya recibido el producto o se haya cancelado por
            cualesquiera de las causas previstas. En el supuesto que el Usuario
            quiera comunicarse con el Mandatario o con Wilbby tras la
            finalización del encargo, deberá utilizar el formulario de contacto
            presente en la Plataforma y contactar con el servicio de atención al
            Usuario.
          </p>
          <p>
            El Usuario exime a Wilbby y al Mandatario de cualquier negligencia o
            error en las indicaciones que consigne para la compra del producto
            que solicite comprar en su nombre a las tiendas físicas. Como
            consecuencia de ello, será el Usuario quien deba asumir el coste
            derivado de la incorrecta consignación de las indicaciones de los
            productos en la Plataforma (i.e. dirección incorrecta, producto
            incorrecto).
          </p>
          <p>
            En caso de realizar un pedido “Lo que sea”, en la medida en que en
            la plataforma no conste el precio del producto y/o servicio, el
            Usuario podrá fijar un precio aproximado. En este caso, el servicio
            o producto podrá ser adquirido presencialmente por el Mandatario de
            acuerdo con lo que el Usuario haya estimado y nunca por una cantidad
            superior al 30% de dicha estimación. En el caso que el precio sea
            más alto, el Mandatario se comunicará con el Usuario para informarle
            de esta situación y será este último quién tome la decisión final de
            proceder o no con la compra presencial en tienda. Wilbby, con la
            única finalidad de ayudar al Usuario, podrá facilitar una estimación
            aproximada del precio, sin que ningún caso dicha estimación pudiera
            resultar vinculante con el precio final que deba abonar el Usuario.
          </p>
          <p>
            En el caso de productos en los que sí consta el precio del producto
            y/o servicio en la Plataforma, los precios consignados en la
            Plataforma pueden ser orientativos, tal y como se establece en la
            cláusula 7 de estos Términos y Condiciones Generales de Uso de la
            Plataforma.
          </p>
          <p>
            Si el producto y/o servicio no se encuentra disponible, el
            Mandatario deberá llamar al Usuario para exponerle las opciones. En
            caso de que el Usuario no se encuentre de acuerdo con ninguna de las
            opciones expuestas por el Mandatario y, consecuentemente, no esté
            interesado en las opciones alternativas, deberá asumir la política
            de cancelación expuesta en las presentes Condiciones Generales de
            Uso (en el apartado nueve). Si el Usuario no responde a las
            llamadas, el Mandatario esperará 5 minutos antes de marcharse.
          </p>
          <p>
            En los supuestos en los que el Usuario no se encontrase en el lugar
            consignado para la entrega, el Mandatario conservará el producto
            durante 24 horas, o 5 minutos en el caso de productos perecederos.
            Asimismo, el Usuario deberá asumir el 100% del coste del servicio de
            recadería básico, así como del precio del producto en el caso de que
            se haya adquirido o contratado alguno a través del Mandatario por
            encargo del Usuario, y deberá volver a pagar otro servicio para
            recibir los productos no entregados. El Mandatario en ningún caso
            será responsable del deterioro o de la caducidad del producto objeto
            del recado.
          </p>
          <p>
            Una vez finalizado el encargo, en el caso de que se haya solicitado
            la adquisición presencial de un producto, el Mandatario entregará al
            Usuario el recibo físico correspondiente a dicho producto, y/ o a
            través de correo electrónico. Si el recado tiene por objeto la mera
            entrega de un producto, el Mandatario se lo entregará al Usuario en
            el lugar y momento exacto en que éste último le indique. Todo ello
            sin perjuicio del recibo electrónico por el servicio que recibirá el
            Usuario en la dirección de correo asociado a su cuenta.
          </p>
          <p>
            En ese momento, el Usuario, destinatario del servicio identificado
            por el usuario en el pedido, o cualquier tercero autorizado por el
            mismo, deberá ratificar, en su caso, el mandato conferido mediante
            su firma en el dispositivo del Mandatario. Wilbby no puede comprobar
            la veracidad de la firma realizada por el Usuario. En el supuesto
            que el Usuario reciba confirmación de la realización del recado sin
            haber procedido a la ratificación del mandato por sí mismo o
            mediante un receptor autorizado por éste, deberá comunicarlo a
            Wilbby de forma inmediata para que la Plataforma pueda tomar las
            medidas oportunas. Wilbby se reserva el derecho a comprobar las
            comunicaciones realizadas por el Usuario a efectos de su
            comprobación y control.
          </p>
          <p>
            ALIMENTACIÓN/PRODUCTOS ENVASADOS: Wilbby recomienda al Usuario que
            cuando encargue productos alimenticios por mandato, solicite al
            Mandatario y/o al establecimiento ofertante, a través de los medios
            facilitados por la Plataforma, la información relativa al contenido
            y composición de los productos alimenticios encargados.
          </p>
          <p>
            Así mismo, Wilbby se exime de cualquier responsabilidad cuando los
            pedidos alimentarios se entreguen al Usuario en bolsas debidamente
            cerradas, idealmente con etiquetas o sellos.
          </p>
          <h2>5. Devolución de productos no perecederos</h2>
          <p>
            En caso de que el Usuario desee proceder a la devolución de un
            producto no perecedero o desee realizar una reclamación acerca de la
            realización de un servicio, sin perjuicio de que Wilbby, como
            intermediario, asuma la gestión de la misma, el responsable final
            siempre será el comercio en el cual se realizó la compra. En caso de
            reembolso del importe de la compra al Usuario, será el comercio el
            que tendrá potestad para decidir el método de devolución (metálico,
            tarjeta de crédito, vale de compra…) con independencia de que sea
            Wilbby el que por cuenta del comercio local proceda a la devolución.
            Por lo tanto, la devolución de un producto no perecedero adquirido
            por el Mandatario en ejecución del mandato conferido por el Usuario
            quedará sujeta en todo caso a la política de devoluciones del
            comercio.
          </p>
          <p>
            Determinados establecimientos podrían no disponer de espacios
            abiertos al público, con lo que el Usuario no podría acceder a los
            mismos a efectos de tramitar la devolución o reclamación. En estos
            supuestos, el Usuario deberá contactar con el servicio de atención
            al Usuario de Wilbby, a través de los canales presentes en la
            Plataforma, con la finalidad de obtener la ayuda y soporte
            necesarios.
          </p>
          <p>
            En el caso que el Usuario quiera tramitar la devolución de algún
            producto no perecedero por no adecuarse a lo que ha solicitado a
            través de la plataforma, el Usuario deberá aportar una fotografía
            del pedido total junto con un listado de los productos incorrectos o
            que no se hayan entregado, así como otras evidencias que demuestren
            la no adecuación del producto solicitado.
          </p>
          <p>
            El Usuario deberá comprobar los productos no perecederos que el
            Mandatario le entregue en la dirección de entrega antes de proceder
            a firmar, en su caso, y ratificar el mandato. Con la firma, el
            Usuario confirma y ratifica el mandato, la compra presencial o
            servicio realizado en su nombre. Asimismo, el Usuario manifiesta que
            un tercero podrá ratificar el mandato en su nombre, por ejemplo en
            aquellos supuestos en que el Usuario no se encuentre en la dirección
            final de entrega o haya designado a un tercero para la recogida y
            firma, en su caso. Así, recae sobre el Usuario y/o dicho tercero la
            responsabilidad de verificar la adecuación del servicio, así como en
            su caso, recabar las evidencias suficientes para justificar lo
            contrario.
          </p>
          <p>
            En todo caso, será potestad del comercio determinar en cada caso la
            procedencia de la devolución por lo que, en caso de disputa, el
            Usuario deberá ponerlo en conocimiento de Wilbby a través de los
            medios facilitados por Wilbby a tal efecto.
          </p>
          <h2>6. Tarifas de los servicios y facturación</h2>
          <p>
            El alta y uso de la Plataforma para los Usuarios no tiene coste
            asociado, pudiendo estos ser revisados por parte de Wilbby en
            cualquier momento. Determinados servicios de la Plataforma pueden
            tener un coste para el Usuario, según estos Términos y Condiciones
            Generales de Uso.
          </p>
          <p>
            El uso de la Plataforma por parte de los Mandatarios y los comercios
            puede tener un coste asociado en función del país desde el que
            deseen hacer uso de la Plataforma para proveer servicios.
          </p>
          <p>
            El Usuario únicamente deberá pagar por cada servicio solicitado a
            través de la Plataforma en concepto del uso de la misma para la
            solicitud de productos y comunicación a través de la Plataforma, y
            por los servicios de mensajería o recadería prestados por terceros.
          </p>
          <p>
            En caso de lluvia o malas condiciones meteorológicas, el Usuario
            deberá abonar una tarifa adicional por el servicio de mensajería o
            recadería prestado por terceros en dichas condiciones
            meteorológicas.
          </p>
          <p>
            Adicionalmente, en aquellos servicios que incluyan la compra de un
            producto, el Usuario deberá pagar el precio de dicho producto. El
            Usuario, al registrarse a través de la Plataforma y proporcionar la
            información bancaria requerida, autoriza expresamente a Wilbby a
            pasar los recibos correspondientes al pago de los servicios
            solicitados, incluyendo el precio y la entrega de los productos
            solicitados.
          </p>
          <p>
            El precio total de cada servicio puede componerse de un porcentaje
            variable en función de los kilómetros y tiempo que deba recorrer y
            utilizar el Mandatario, así como en su caso, del precio fijado por
            cada comercio para los casos en que el Usuario solicite la compra
            física de un producto o servicio. Wilbby se reserva el derecho de
            modificar el precio en función de la franja horaria y/o distancia en
            la que se realice el servicio. De acuerdo con estas condiciones, el
            Usuario tendrá derecho a saber la tarifa aproximada de su servicio
            antes de la contratación del mismo y formalización del pago,
            exceptuando cuando el Usuario no especifique la dirección de
            recogida. La tarifa del servicio de envío podrá variar cuando se
            dieran circunstancias de fuerza mayor que estuvieran fuera del
            alcance de Wilbby y que conlleven un aumento de dicha tarifa.
          </p>
          <p>
            La tarifa podrá incluir propinas para el Mandatario y/o la tienda
            local cuyo importe dependerá única y exclusivamente de la voluntad
            del Usuario.
          </p>
          <p>
            Wilbby se reserva el derecho de modificar precios en la plataforma
            en cualquier momento. Los cambios efectuados tendrán efecto
            inmediato tras su publicación. El Usuario autoriza expresamente a
            Wilbby para que le remita por medios telemáticos, a la dirección de
            correo electrónico facilitada por el mismo durante el proceso de
            registro, el recibo de los servicios contratados y/o las facturas
            generadas. En caso de requerir una factura, el Usuario deberá añadir
            los datos fiscales a la plataforma antes de hacer el pedido.
          </p>
          <p>
            La cancelación posterior del servicio por el Usuario, cuando ya haya
            habido confirmación de preparación del pedido por parte del comercio
            local y la misma haya sido comunicada al Usuario, facultará a Wilbby
            a cargar al Usuario la tarifa que, en su caso, corresponda.
            Asimismo, en el caso que el Usuario hubiera solicitado al Mandatario
            la compra de un producto en nombre suyo, si el Usuario cancelará el
            pedido cuando la adquisición ya estuviera realizada, asumirá los
            costes de los servicios de entrega realizados por el Mandatario, así
            como el coste íntegro del producto. Todo ello sin perjuicio de que
            el Usuario pueda solicitar un nuevo servicio a los efectos de
            devolver los productos adquiridos, o que se le entreguen en otra
            dirección. Para el caso de productos no perecederos, el Usuario
            podrá ejercitar su derecho de desistimiento ante el comercio que le
            ha vendido los productos. Si desea ejercer el derecho a través de
            Wilbby, deberá contratar de nuevo el servicio.
          </p>
          <p>6.1. Plataformas de pago</p>
          <p>
            El pago de los productos y/o servicios que se ofertan a través de la
            Plataforma, que se venden presencialmente en los restaurantes y/o
            tiendas, y que se entregan a los Usuarios de forma diferida, se
            efectúa transitoriamente a Wilbby y éste lo transmite a los
            restaurantes y/o tiendas con los que mantiene un acuerdo comercial.
            Los restaurantes y/o establecimientos asociados autorizan a Wilbby a
            aceptar el pago en su nombre, por lo que el pago del precio de
            cualquier producto (i.e. comida, bebida, regalo…) efectuado
            correctamente a favor de Wilbby descargará al Usuario de las
            obligaciones de abonar dicho precio al restaurante y/o
            establecimientos asociados.
          </p>
          <p>
            Asimismo, el pago del Usuario le descarga de cualquier obligación
            respecto al Mandatario, teniendo el cobro total al Usuario efecto
            liberatorio sobre cualquier obligación que pudiera tener con
            restaurantes y/o tiendas y/o Mandatarios.
          </p>
          <p>
            Asimismo, el pago del Usuario le descarga de cualquier obligación
            respecto al Mandatario, teniendo el cobro total al Usuario efecto
            liberatorio sobre cualquier obligación que pudiera tener con
            restaurantes y/o tiendas y/o Mandatarios.
          </p>
          <p>
            *En caso de no estar autorizadas dichas entidades, Wilbby se exime
            de toda responsabilidad por dicha falta de autorización o licencia,
            siendo única y exclusivamente responsabilidad de las Entidades de
            Dinero Electrónico.
          </p>
          <p>
            Wilbby, a través del proveedor de pagos que a tal efecto tiene
            contratado y con el único objeto de comprobar el medio de pago
            facilitado, se reserva el derecho, como medida antifraude, a
            solicitar la preautorización de cargo de los productos encargados a
            través de la plataforma. La citada preautorización no supondrá, en
            ningún caso, el pago total del encargo en tanto éste se realizará
            única y exclusivamente tras la puesta a disposición de los productos
            al Usuario o bien, por las causas descritas en los presentes
            términos y condiciones.
          </p>
          <p>
            Wilbby, para dar un mayor soporte a los Usuarios, actuará como
            primer punto de contacto con los mismos y asumirá la responsabilidad
            por los pagos realizados a través de la Plataforma. Esta
            responsabilidad incluye: reembolsos, devoluciones, cancelaciones y
            disputas de resolución, en un estadio inicial, sin perjuicio de las
            actuaciones que Wilbby pueda realizar ante los comercios locales
            como únicos vendedores físicos de los productos ordenados por los
            Usuarios.
          </p>
          <p>
            Siguiendo cuanto descrito, en el supuesto que exista alguna disputa,
            Wilbby ofrecerá la primera línea de soporte y reembolsará al Usuario
            si se considera apropiado.
          </p>
          <p>
            Si el Usuario tiene algún problema con el desarrollo de su pedido,
            podrá contactar al servicio de atención al Usuario de Wilbby a
            través de los medios puestos a disposición del mismo en la
            Plataforma.
          </p>
          <h2>
            7. Precio de los productos y/o servicios consignados en la
            Plataforma
          </h2>
          <p>
            Todos los precios que se indican en la Plataforma incluyen los
            impuestos que pudieran ser aplicables en función del territorio
            desde el que opere el Usuario y en todo caso se expresarán en la
            moneda vigente en función del territorio desde el que opere el
            Usuario.
          </p>
          <p>
            De acuerdo con la Cláusula 6 anterior, los precios aplicables en
            cada servicio serán los publicados en la Plataforma sujetos a las
            particularidades expuestas y aplicados de forma automática en el
            proceso de contratación en la última fase del mismo.
          </p>
          <p>
            No obstante, los precios de los productos de venta en restaurantes
            y/o tiendas mostrados en la Plataforma de Wilbby podrán ser
            orientativos. En todo caso, dichos precios corresponden a los
            productos de venta en restaurantes y/o tiendas y son exclusivamente
            fijados por ellos. El Usuario podrá comunicarse con el Mandatario a
            los efectos de confirmar el precio final de los productos
            solicitados.
          </p>
          <p>
            El Usuario asume que en todo caso la valoración económica de algunos
            de los productos podrá variar en tiempo real en función del
            establecimiento que los vende y el “stock” disponible. Para
            cualquier información sobre el pedido solicitado, el Usuario podrá
            contactar con Wilbby.
          </p>
          <p>
            De acuerdo con lo anterior, el Usuario mediante la solicitud de
            compra y envío a través de la Plataforma confiere al Mandatario un
            mandato para adquirir presencialmente los productos en su nombre por
            el precio fijado por los comercios. El Usuario podrá estar en
            contacto directo con el Mandatario durante la realización del
            pedido.
          </p>
          <h2>8. Códigos promocionales y/o otras ofertas o descuentos</h2>
          <p>
            Wilbby podrá facilitar, en cualquier momento y de forma unilateral,
            créditos para utilizar dentro de la Plataforma o envíos gratis a
            ciertos Usuarios. El Usuario reconoce y acepta que los créditos y
            envíos gratis deberán ser utilizados dentro de los seis (6) meses a
            contar desde la fecha en la que se pone el crédito o envío gratis a
            disposición del Usuario en la Plataforma.
          </p>
          <p>
            Los códigos promocionales y/o otras ofertas o descuentos ofertados a
            través de la Plataforma deberán ser correctamente consignados en la
            aplicación con anterioridad a la realización del pedido, en caso
            contrario su uso no será efectivo para el Usuario y no podrá
            disfrutar de los mismos.
          </p>
          <p>
            Siempre que una cancelación sea instada por Wilbby en los términos
            descritos en el apartado 9 siguiente, el Usuario mantendrá la
            vigencia del código promocional y/o otras ofertas o descuentos para
            su uso futuro. Cuando la cancelación sea instada por el Usuario se
            estará a lo previsto en el apartado 9 siguiente.
          </p>
          <p>
            Wilbby se reserva el derecho a cancelar los códigos promocionales
            y/o otras ofertas o descuentos ofertados de forma unilateral, cuando
            tenga conocimiento de un uso fraudulento de los mismos (i.e. canjear
            un código promocional cuando no se es el destinatario legítimo del
            mismo, transmisión masiva de códigos, vender descuentos o códigos,
            ....), cuando haya expirado el plazo anteriormente mencionado y,
            asimismo, se reserva el derecho a aplicar sanciones a los Usuarios
            por el importe del uso defraudado a Wilbby.
          </p>
          <p>
            Wilbby no asume ninguna responsabilidad si, por fuerza mayor o
            sucesos que escapen de su voluntad o cuya necesidad sea justificada,
            se viera obligado a anular, acortar, prorrogar o modificar las
            condiciones de las promociones.
          </p>
          <p>
            En particular, Wilbby no asume ninguna responsabilidad en caso de
            que la página web no estuviese disponible en algún momento durante
            las promociones o de un mal funcionamiento del sistema automatizado
            de las promociones.
          </p>
          <h2>9. Derecho de desistimiento y cancelación de pedidos</h2>
          <p>
            De acuerdo con la propia naturaleza del servicio que ofrece Wilbby,
            el Usuario es consciente de que una vez un Mandatario haya aceptado
            voluntariamente un pedido, se considera que la ejecución del mandato
            de compra ha comenzado y por tanto el Usuario podría no tener
            derecho a desistir en la solicitud del servicio de forma gratuita.
          </p>
          <p>
            Sin perjuicio de lo anterior, los costes de cancelación podrían
            depender de los siguientes factores;
          </p>
          <p>
            - Si el restaurante o establecimiento ha aceptado y ha empezado a
            preparar el pedido, se cobrará el importe de los productos. La
            aceptación por parte del restaurante se comunicará al Usuario a
            través de la Plataforma y/o a través del envío de correo electrónico
            a la dirección registrada por el Usuario.
          </p>
          <p>
            - Si el Usuario cancela el pedido una vez el Mandatario ha aceptado
            su tramitación, se cobrará el importe de la tasa de cancelación al
            Usuario. Se comunicará al Usuario la aceptación por parte del
            Mandatario a través de la Plataforma. El Usuario puede ver el coste
            de la cancelación en la Plataforma. En caso de que el pedido sea
            cancelado por parte de Wilbby, los agentes se pondrán en contacto
            con el Usuario para comunicarle el costo de cancelación si lo
            hubiera.
          </p>
          <p>
            En el supuesto que el restaurante o establecimiento y el Mandatario
            hayan aceptado la tramitación del pedido, se le cobrará al Usuario
            el precio de los productos y la tasa de cancelación. Los costes de
            cancelación totales aparecerán en la pantalla del Usuario en el
            momento de apretar el botón “Cancelar” presente en la Plataforma. De
            forma previa, se mostrará al Usuario el cargo que se aplicará según
            los factores anteriormente descritos. Para determinadas ciudades,
            los costes de cancelación podrían no ser visibles directamente desde
            la Plataforma. Para esos casos, los costes de cancelación que
            aplicarán serán los previstos en el Anexo II de las presentes
            Condiciones. En caso de productos no perecederos ni alimenticios, en
            el momento de cancelación del servicio, si el Mandatario ya hubiese
            realizado la compra encomendada del producto, el Usuario podrá
            encargar al Mandatario que realice la devolución del mismo. A tal
            efecto, el Usuario deberá satisfacer el coste total de adquisición
            de los productos y los costes de entrega, así como el coste del
            servicio de devolución. En el supuesto que el Mandatario haya podido
            realizar la devolución del producto, se le reintegrará al Usuario el
            valor del producto, debiendo éste, como se ha dicho, pagar el coste
            de los dos servicios de recogida y entrega, así como de devolución.
            La devolución en todo caso estará sujeta a las políticas de
            devolución del comercio, por lo que el Usuario manifiesta ser
            conocedor que en el caso de productos perecederos (i.e. comida), no
            se podrá proceder a la devolución y por lo tanto, Wilbby quedará
            facultado para cargar tanto el producto que el Mandatario ya hubiera
            adquirido bajo su mandato, como el servicio de envío incurrido. En
            el caso que el Usuario haya indicado de forma incorrecta la
            dirección de entrega de los productos, podrá consignar una nueva
            dirección en cualquier momento siempre que se encuentre dentro de la
            misma ciudad del encargo inicial y siempre y cuando esta sea una
            ciudad dónde Wilbby ofrece su servicio de intermediación. En ese
            caso, el Usuario estará encargando la contratación de un nuevo
            servicio y acepta que se le carguen los importes correspondientes a
            la nueva entrega. En caso de encontrarse en otra ciudad de la
            inicialmente indicada, ésta no podrá modificarse para entregarse en
            una nueva ciudad y el encargo se cancelará, debiendo el Usuario
            asumir los costes generados según se establece en esta cláusula.
            Wilbby se reserva el derecho a cancelar un pedido sin necesidad de
            alegar causa justa. En caso de cancelación instada por Wilbby sin
            causa justa, el Usuario tendrá derecho al reembolso de la cantidad
            abonada. Wilbby dispone de hojas de reclamación oficiales a
            disposición del Usuario, por el objeto del servicio ofrecido por
            Wilbby, en los idiomas oficiales de los países dónde Wilbby opera.
            Las mencionadas hojas de reclamación podrán solicitarse por parte
            del Usuario a través del servicio de atención al Usuario de Wilbby y
            será remitida de forma automática la opción para acceder a las
            mismas. El Usuario deberá especificar en el correo electrónico y la
            ubicación exacta desde la que realiza la solicitud que deberá
            coincidir con el lugar de realización del servicio, en caso de duda,
            será este último el lugar dónde deberá instarse la reclamación. 10.
            Condiciones especiales del servicio de encargo de compra de
            productos a través de Wilbby El Usuario tiene la opción de solicitar
            a través de la Plataforma la adquisición presencial por parte del
            Mandatario de una serie de productos y/o servicios que son ofrecidos
            por establecimientos con los que Wilbby puede mantener o no un
            acuerdo comercial. El Usuario puede seleccionar mediante un menú
            desplegable dentro de la Plataforma, una serie de opciones donde se
            puede indicar de forma orientativa las características, el precio,
            así como en ocasiones, hasta una fotografía del producto o servicio.
            En aquellos supuestos en los que el Usuario desee conocer la
            composición e información nutricional de los productos presentes en
            la Plataforma, éste deberá contactar directamente con cada
            establecimiento para que pueda acceder a la información completa de
            los productos. Una vez el Usuario seleccione una de las opciones,
            podrá completar además el pedido mediante un espacio de texto libre
            para incorporar información más detallada o instrucciones para el
            Mandatario que se encargue de llevar a cabo dicho pedido. A través
            del botón “Algo especial”, el Usuario manifiesta su voluntad de
            encargar la compra presencial de determinados productos, resultando
            el Mandatario un mero mandatario por cuenta de este. El Usuario
            podrá especificar los productos objeto del pedido mediante un
            espacio de texto libre para incorporar información más detallada o
            instrucciones para el Mandatario que se encargue de llevar a cabo
            dicho pedido. Como consecuencia de ello, será el Usuario el único
            responsable y quién, llegado el caso, asumirá cualquier incidencia
            derivada de la naturaleza de dichos productos (i.e. sanción,
            responsabilidad civil y/o penal, responsabilidad patrimonial…). A
            este respecto es importante comunicar al Usuario que Wilbby colabora
            activamente con las autoridades de cada país con la finalidad de
            luchar contra el fraude, tráfico de estupefacientes, blanqueo de
            capitales, terrorismo y suplantación de identidad, entre otros. Por
            lo tanto, el Usuario es conocedor que Wilbby podrá comunicar los
            datos de los mismos a las autoridades públicas, cuando así lo
            soliciten, con la finalidad de prevenir y evitar la comisión de
            estos u otros delitos. El Usuario es consciente y acepta que las
            descripciones y, en su caso, los precios o las fotografías de los
            productos y/o servicios ofertados en la Plataforma se realizan en
            base a la información y documentación proporcionada por los
            establecimientos por lo que Wilbby no puede ofrecer garantías frente
            a posibles inexactitudes en dichas descripciones, precios y/o
            fotografías. Asimismo, el Usuario acepta que todos los productos
            reflejados en la plataforma de Wilbby están sujetos a la
            disponibilidad de los mismos y en este sentido asume la posibilidad
            de que durante la realización del pedido se encuentre con la no
            disponibilidad del producto y/o servicio en el establecimiento .
            Adicionalmente, el precio del producto puede variar ligeramente a
            causa de modificaciones en el punto de venta. Wilbby se reserva el
            derecho de proceder con la compra ante variaciones en el precio de
            hasta un 30%, si supera el citado 30%, el Mandatario se pondrá en
            contacto con el Usuario para transmitirle la situación. Wilbby se
            reserva el derecho a retirar cualquier producto de su plataforma,
            así como de modificar el contenido de su ficha en cualquier momento,
            sin que le pueda ser atribuido ningún tipo de responsabilidad.
          </p>
          <p>10.1 Entregas</p>
          <p>
            Wilbby proporciona al Usuario un servicio de entrega de los
            productos adquiridos presencialmente por el Mandatario en
            determinadas ciudades. El pedido se podrá realizar siempre que se
            cumplan las siguientes condiciones en el momento de pago del pedido:
          </p>
          <ul>
            <li>
              <p>
                El servicio se encuentre operativo para la franja horaria
                escogida.
              </p>
            </li>
            <li>
              <p>
                La mercancía a entregar debe encontrarse dentro de alguna de las
                ciudades y dentro del espacio habilitado en las que opere
                Wilbby.
              </p>
            </li>
            <li>
              <p>
                El lugar de destino debe estar situado en la misma ciudad en la
                que se encuentre el producto.
              </p>
            </li>
            <li>
              <p>
                El tamaño del pedido no puede superar las medidas: 40cm x 40cm x
                30cm*
              </p>
            </li>
            <li>
              <p>
                El peso máximo a cargar en un pedido será, aproximadamente, de 9
                Kg*
              </p>
            </li>
          </ul>
          <p>
            *El tamaño y peso mencionados anteriormente no aplicarán para los
            supuestos de vehículos habilitados para dichos volúmenes.
          </p>
          <p>10.2 Precio y método de pago</p>
          <p>
            El precio del producto y/o servicio será el indicado por el
            establecimiento a través de la Plataforma. Sin embargo, el Usuario
            es conocedor que en todo caso la valoración económica de algunos de
            los productos podrá variar en tiempo real, debido a las
            disponibilidades de stock de los establecimientos que se ofertan en
            la plataforma, y que en todo caso el coste final será siempre
            comunicado al Usuario de forma previa al momento de pago. El Usuario
            podrá contactar, desde el momento en que el Mandatario se encuentra
            a 1000 metros del lugar de recogida del pedido hasta la entrega del
            mismo, con el Mandatario, que actuará en su nombre en la adquisición
            de productos y servicios, por lo que cualquier cambio o variación
            será comunicada por el Mandatario de forma previa a la ejecución del
            mandato para la aprobación del Usuario. En caso de querer realizar
            alguna apreciación sobre el pedido encargado, el Usuario tendrá
            siempre la posibilidad de contactar directamente con el Mandatario
            que ejecuta el mandato otorgado. De igual forma, durante la pasarela
            de pago, el Usuario será informado del precio final del envío y la
            hora aproximada de entrega según las condiciones del servicio de
            recadería de Wilbby expresadas más arriba. El Usuario podrá realizar
            el pago de los productos y/o servicios en efectivo o a través de su
            tarjeta de crédito. La opción de pago en efectivo podrá no estar
            disponible en todos los países donde opera Wilbby. Se informará al
            Usuario, cuando acceda a realizar el pedido, de las diferentes
            opciones de pago que tenga según el territorio desde el que solicite
            el servicio. Para el pago con tarjeta, el Usuario deberá facilitar
            los datos de las mismas a través de la plataforma como método de
            pago asociado a su cuenta. Wilbby no almacena el número de la
            tarjeta en sus servidores, y sólo podrá visualizar los cuatro
            últimos dígitos de ésta. La información completa será almacenada en
            los servidores del prestador de servicios de pago que realiza los
            pagos en nombre de Wilbby. El pago con tarjeta de crédito no
            supondrá ningún coste extra para el Usuario. Todo ello de acuerdo
            con los términos de la plataforma de pago expuestos en la Cláusula
            6.1. En caso de pago en efectivo, el Usuario deberá abonar el precio
            en el momento de la entrega del producto y/o cumplimiento del recado
            en el lugar de entrega. El Usuario no podrá negarse a abonar el
            coste del servicio de entrega y/o el precio del producto solicitado.
            El Usuario únicamente podrá negarse a abonar el coste del servicio
            si ha realizado una queja y ha recibido de parte de Wilbby una
            resolución favorable del mismo al momento de la entrega. Cuando el
            cobro del servicio al Usuario no pudiera realizarse por cualquier
            motivo, la cuenta del Usuario quedará bloqueada en tanto no se
            regularice y abone la deuda.
          </p>
          <p>
            10.3. Envío de muestras gratuitas a domicilio y otras acciones
            comerciales
          </p>
          <p>
            Wilbby se reserva el derecho a realizar acuerdos comerciales con
            Comercios, Grandes Superficies, Empresarios, Profesionales (i.e.
            empresas de gran consumo del sector alimentación, laboratorios,
            Grandes Almacenes, marcas de gran y pequeño consumo…) a los efectos
            de realizar comunicaciones promocionales, incluyendo el envío de
            muestras gratuitas a domicilio junto al pedido solicitado por el
            Usuario.
          </p>
          <h2>
            11. Condiciones particulares para el servicio “Wilbby Market”.
          </h2>
          <p>
            Wilbby, a través de la Plataforma, pone a disposición de los
            Usuarios una categoría dónde éstos pueden seleccionar entre una gama
            de productos generalmente presentes, entre otros, en supermercados,
            grandes almacenes y/o establecimientos de cuidado personal para que
            el Mandatario, que acepte la tramitación del recado, pueda
            entregarlo en la dirección introducida por el usuario. El Usuario
            deberá seleccionar los productos en base a sus preferencias y podrá
            solicitar, a través de los mecanismos previstos en la Plataforma, la
            información relativa a los alérgenos, intolerancias, composición y/o
            información nutricional de los mismos, que se facilitarán a través
            del chat de contacto o a través del correo electrónico del Usuario
            registrado en la Plataforma. Asimismo, Wilbby podrá poner a
            disposición de los Usuarios que lo soliciten los enlaces
            informativos dónde poder encontrar y revisar dicha información. Para
            los pedidos de “Wilbby Marke” aplicarán las previsiones previstas en
            el apartado 10.1 anterior. Las Condiciones Generales de Uso serán de
            aplicación al servicio “Wilbby Marke” en caso de no encontrarse
            expresamente reguladas en el presente apartado.
          </p>
          <h2>12. Compra de bebidas alcohólicas</h2>
          <p>
            Los Usuarios que realicen un pedido que incluya la adquisición y/o
            entrega de bebidas alcohólicas a través de la plataforma deben ser
            mayores de edad, habiendo cumplido los la edad establecida por la
            legislación local aplicable en el territorio en el que el Usuario
            realiza el pedido. Al realizar un pedido que incluya bebidas
            alcohólicas, el Usuario confirma que tiene al menos la edad
            establecida por la legislación local aplicable en el territorio en
            el que el Usuario realiza el pedido. Wilbby se reserva el derecho de
            negarse a permitir el pedido de compra y/o entrega de alcohol a
            cualquier persona que no pueda probar tener al menos la edad
            establecida por la legislación local aplicable en el territorio en
            el que el Usuario realiza el pedido. La presente cláusula será de
            idéntica aplicación a cualquier otro producto y/o servicio análogo
            reservado a la mayoría de edad según legislación vigente y que sea
            solicitado por un Usuario a través de la Plataforma. Asimismo, en
            los casos y en las ciudades en que se encuentre restringida la venta
            y/o entrega de bebidas alcohólicas en determinada franja horaria, el
            Usuario es responsable de realizar pedidos en los horarios
            permitidos según la normativa de aplicación. Wilbby se reserva el
            derecho a negarse a permitir el pedido de compra y/o entrega de
            alcohol fuera de los horarios permitidos.
          </p>
          <h2>13. Productos en oficinas de farmacia</h2>
          <p>
            Wilbby no realiza la venta ni publicidad de medicamentos de uso
            humano a través de la Plataforma cumpliendo con la normativa
            vigente. Los Mandatarios actúan como mandatarios de aquellos
            Usuarios que solicitan medicamentos de uso humano no sujetos a
            prescripción médica a través de la Plataforma para su recogida.
            Wilbby garantiza en todo caso a los Usuarios el consejo farmacéutico
            antes de solicitar sus medicamentos, para ello Wilbby ha habilitado
            una casilla en SOLICITUD ESPECIAL para que en caso de duda por parte
            del Usuario consulte al farmacéutico que dispensará el producto o
            productos a través del mismo todo ello con la finalidad que el
            Farmacéutico pueda dispensar el producto adecuado. En cualquier
            caso, Wilbby no se hace responsable del uso que hagan los Usuarios
            de los productos solicitados en la sección de Farmacia así como
            tampoco responderá por las cantidades y/o condiciones de los
            productos que se dispensen en las oficinas de farmacia.
            Adicionalmente, la Plataforma podrá ceder a la farmacia que dispense
            el medicamento solicitado por el Usuario, el nombre de usuario y
            teléfono de éste, a efectos de responderle a las cuestiones
            planteadas. Dicha cesión estará sujeta en todo caso a la
            autorización expresa e inequívoca del Usuario de acuerdo con la
            legislación aplicable en materia de protección de datos de carácter
            personal. Wilbby pone a disposición de todos los Usuarios de Wilbby
            en la Plataforma la ficha técnica oficial de los medicamentos
            publicada por la Agencia Española de Medicamentos y Productos
            Sanitarios del Ministerio de Sanidad, Política Social e Igualdad.
          </p>
          <h2>14. Política de materias reservadas en envíos</h2>
          <p>Lista de ejemplos no limitativa:</p>
          <p>
            {" "}
            Alcohol y Tabaco Las entregas de alcohol y tabaco pueden estar
            limitadas o restringidas en determinados países/y o ciudades donde
            opera la Plataforma. Animales y Especies Reguladas Las partes de
            animales, o fluidos; semillas prohibidas, plantas nocivas; plantas u
            otros organismos regulados (incluyendo sus derivados) en peligro de
            extinción o cuyo comercio es en cualquier caso según lo que prevea
            la ley. Pornografía infantil Material pornográfico involucrando a
            menores o contenido que puede ser percibido como pedofilia erótica.
            Copyright sobre Software y Media Copias no autorizadas de libros,
            música, películas y otros materiales protegidos o por licencia
            incluyendo copias sin atribución apropiada; y copias no autorizadas
            de software, videojuegos y otros materiales protegidos o con
            licencia, incluyendo OEM u otros productos activando mensajes no
            solicitados. Falsificaciones y productos desautorizados Réplicas o
            imitaciones de diseñadores u otros bienes; objetos de celebridades
            que normalmente requerirían autenticación; autógrafos falsos;
            divisas; sellos; tiques; u otros bienes no autorizados. Dispositivos
            o herramientas para desbloquear medidas de seguridad Módems, chips u
            otros dispositivos para desmantelar medidas técnicas de protección
            así como en dispositivos digitales, incluyendo para desbloquear
            iPhones. Drogas Sustancias controladas, narcóticos, drogas ilegales,
            y drogas accesorias, incluyendo psicoactivas y drogas vegetales como
            setas alucinógenas así como material promoviendo su uso; o
            sustancias legales como plantas y hierbas, en una forma que sugiera
            su ingesta, inhalación, extracción, u otro uso que pueda provocar el
            mismo uso que un componente, droga o sustancia ilegal o que produzca
            beneficios no probados para la salud. Juego y Apuestas Billetes de
            lotería, apuestas, membresías/inscripciones en sitios online de
            apuestas, y contenido relativo. Promoción de casinos físicos
            permitida. Productos de Venta en Oficinas de Farmacia Las entregas
            de productos de farmacia pueden estar limitadas o restringidas en
            determinados países y/o ciudades donde opera la Plataforma. No está
            permitido el encargo/envío de medicamentos sujetos a prescripción
            médica. Los medicamentos OTC/de venta libre, productos sanitarios
            complementarios, así como cualquier otro producto de higiene,
            nutrición o análogo, de venta en farmacia y de uso humano se
            encuentran sujetos al mandato que el Usuario confiera al Mandatario
            y a lo que estime conveniente el farmacéutico. Materiales para
            hacking y cracking Manuales, guías, información o equipamiento que
            infrinja la ley dañando o facilitando fraudulentamente el acceso a
            software, servidores, sitios web u otra propiedad protegida. Cuerpo
            humano Órganos u otras partes del cuerpo; fluidos corporales;
            células madre; embriones. Bienes robados o ilegales Materiales,
            productos o información que promueva bienes ilegales o facilite
            actos ilegales; bienes sobre los que no se tiene la propiedad o no
            se tiene el derecho a la venta; bienes producidos con violación de
            los derechos de terceros; bienes en violación de restricciones sobre
            importación, exportación o etiquetado; vehículos a motor sujetos a
            restricciones de transferencia; Usted (Usuario de Wilbby) es única y
            completamente responsable de verificar que todos los objetos son
            auténticos y legales. Equipamiento de telecomunicaciones ilegal
            Dispositivos que pretenden obtener señal satélite gratis, productos
            ilegales para modificar teléfonos móviles y otros equipamientos.
            Bienes ofensivos Bienes, literatura, productos, u otro material que:
            • Difame alguna persona o grupo de personas basándose en su raza,
            etnia, origen nacional, religión, sexo, u otro factor. • Difame a
            alguna persona o grupo de personas protegidas según la ley aplicable
            en supuestos de difamaciones (como por ejemplo la familia real en
            algunas jurisdicciones). • Enaltecimiento o incitación de actos
            violentos. • Promover intolerancia u odio. • Promover o apoyar
            membresía en grupos terroristas u otras organizaciones prohibidas
            por ley. • Contravenir la moralidad pública. Bienes ofensivos
            relacionados con un crimen Fotos u objetos de la escena de un
            crimen, como pertenencias personales, asociadas con criminales o con
            actos criminales. Artefactos culturalmente protegidos Material
            cubierto por la Convención de la UNESCO de 1970 sobre Medidas que
            Deben Adoptarse para Prohibir e Impedir la Importación, la
            Exportación y la Transferencia de Propiedad Ilícitas de Bienes
            Culturales u otros bienes restringidos por ley a su venta,
            exportación o transferencia; Artefactos, formaciones de cuevas
            (estalactitas y estalagmitas); Dispositivos pirotécnicos y
            sustancias peligrosas que requieran permiso especial Artículos
            pirotécnicos y bienes relacionados en mercados en que su entrega
            está regulada así como sustancias como la gasolina o el propano.
            Dispositivos de tráfico Radares, cubiertas de matrícula, cambiadores
            ilegales de tráfico y productos relacionados. Armas Armas de fuego,
            munición, y otros objetos incluyendo pero no limitando a armas de
            fuego, cuchillos camuflables, o indetectables, armas de artes
            marciales, silenciadores, munición o revistas de munición. Divisas y
            Dinero Cambio de divisas o divisas aseguradas con metales preciosos,
            así como billetes, monedas, o cualquier otro título de valor Escuela
            Primaria y Secundaria/Uso por menores Se reserva el derecho a
            rechazar pedidos de todos aquellos usuarios menores de edad.
            Adicionalmente, se reserva el derecho a rechazar pedidos en
            cualquier localización a los alrededores de escuelas de primaria o
            secundaria, así como a solicitar acreditación suficiente Mal uso de
            la plataforma/Abuso No toleraremos el uso de lenguaje abusivo así
            como de actitudes abusivas hacia nuestra compañía o hacia los
            profesionales independientes.
          </p>

          <h2>15. Solidaridad en Wilbby</h2>

          <p>
            Wilbby colabora con diferentes entidades, grupos de empresas y
            organizaciones no gubernamentales en proyectos benéficos,
            solidarios, campañas de donaciones y campañas sin ánimo de lucro en
            algunos países donde Wilbby opera (sujeto a aplicabilidad en cada
            país). Wilbby actúa en calidad de intermediario en la organización
            de tales campañas y proyectos, basándose exclusivamente en su buena
            fe y deseo de contribuir a las iniciativas sociales. Wilbby no asume
            la responsabilidad por el uso final ni por el resultado de dichas
            campañas. Tampoco proporciona ninguna garantía o aval, ni asume
            ninguna responsabilidad legal, ni ejerce ningún control sobre cómo
            las donaciones son utilizadas por los establecimientos antes
            mencionados.
          </p>

          <h2>16. Geolocalización</h2>

          <p>
            Wilbby podrá recoger, utilizar y compartir datos precisos sobre
            localizaciones, incluyendo la localización geográfica en tiempo real
            del ordenador o dispositivo móvil del Usuario, siempre que el
            Usuario lo autorice. Estos datos de localización pueden ser
            recogidos y utilizados por Wilbby para mostrar a los Usuarios la
            ubicación del origen de un recado y/o la ubicación del origen de
            destino. En este sentido, los Usuarios consienten expresamente que
            sus datos de geolocalización sean compartidos con otros Usuarios y
            Proveedores para poder llevar a éxito el recado solicitado en cada
            momento. Los Usuarios podrán optar por desactivar la Geolocalización
            en sus dispositivos según se detalla en la Política de Privacidad de
            Datos. Es responsabilidad del Usuario consignar correctamente las
            direcciones de recogida y entrega. En este sentido, Wilbby no se
            responsabiliza de error u omisión en la consignación de las mismas
            por parte del Usuario.
          </p>

          <h2>17. Obligaciones del Usuario</h2>

          <p>
            Los Usuarios son completamente responsables del acceso y correcto
            uso de su perfil y demás contenidos de la Plataforma con sujeción a
            la legalidad vigente, sea nacional o internacional del País desde el
            que hace uso de la Plataforma, así como a los principios de buena
            fe, a la moral, buenas costumbres y orden público. Y
            específicamente, adquiere el compromiso de observar diligentemente
            las presentes Condiciones Generales de Uso. Los Usuarios son
            responsables de consignar correctamente nombres de Usuario y
            contraseñas individuales, no transferibles y lo suficientemente
            complejas, así como no usar el mismo nombre de Usuario y contraseña
            que en otras plataformas, todo ello con la finalidad de proteger su
            cuenta del uso fraudulento por parte de terceros ajenos a la
            Plataforma. Los Usuarios se abstendrán de usar su perfil y el resto
            de contenidos de la Plataforma con fines o efectos ilícitos y que
            sean lesivos de los derechos e intereses de terceros, o que de
            cualquier forma puedan dañar, inutilizar, afectar o deteriorar la
            Plataforma, sus contenidos y sus servicios. Asimismo, queda
            prohibido impedir la normal utilización o disfrute de la Plataforma
            a otros Usuarios. Wilbby no podrá ser considerada responsable
            editorial, y declara expresamente que no se identifica con ninguna
            de las opiniones que puedan emitir los Usuarios de la Plataforma, de
            cuyas consecuencias se hace enteramente responsable el emisor de las
            mismas. Quienes incumplan tales obligaciones responderán de
            cualquier perjuicio o daño que ocasionen. Wilbby no responderá de
            ninguna consecuencia, daño o perjuicio que pudiera derivarse de
            dicho acceso o uso ilícito por parte de terceros.
          </p>

          <p>
            En general, los Usuarios se comprometen, a título enunciativo y no
            taxativo, a:
          </p>

          <ul>
            <li>
              <p>
                No alterar o modificar, total o parcialmente la Plataforma,
                eludiendo, desactivando o manipulando de cualquier otra las
                funciones o servicios de la misma;
              </p>
            </li>
            <li>
              <p>
                No infringir los derechos de propiedad industrial e intelectual
                o las normas reguladoras de la protección de datos de carácter
                personal;
              </p>
            </li>
            <li>
              <p>
                No usar la Plataforma para injuriar, difamar, intimidar, violar
                la propia imagen o acosar a otros Usuarios; - No acceder a las
                cuentas de correo electrónico de otros Usuarios;
              </p>
            </li>
            <li>
              <p>
                No introducir virus informáticos, archivos defectuosos, o
                cualquier otro programa informático que pueda provocar daños o
                alteraciones en los contenidos o sistemas de Wilbby o terceras
                personas;
              </p>
            </li>
            <li>
              <p>
                No remitir correos electrónicos con carácter masivo y/o
                repetitivo a una pluralidad de personas, ni enviar direcciones
                de correo electrónico de terceros sin su consentimiento;
              </p>
            </li>
            <li>
              <p>
                No realizar acciones publicitarias de bienes o servicios sin el
                previo consentimiento de Wilbby.
              </p>
            </li>
          </ul>

          <p>
            Cualquier Usuario podrá reportar a otro Usuario cuando considere que
            está incumpliendo las presentes Condiciones Generales de Uso,
            asimismo todos los Usuarios pueden informar a Wilbby de cualquier
            abuso o vulneración de las presentes condiciones, a través del
            Formulario de Contacto. Wilbby verificará este reporte, a la mayor
            brevedad posible, y adoptará las medidas que considere oportunas,
            reservándose el derecho a retirar y/o suspender a cualquier Usuario
            de la Plataforma por el incumplimiento de las presentes Condiciones
            Generales de Uso. Asimismo Wilbby se reserva el derecho a retirar
            y/o suspender cualquier mensaje con contenido ilegal u ofensivo, sin
            necesidad de previo aviso o posterior notificación.
          </p>

          <h2>18. Baja del Usuario</h2>

          <p>
            El Usuario podrá darse de baja de la Plataforma a través del
            Formulario de Contacto dentro de la Plataforma.
          </p>

          <h2>19. Responsabilidad de Wilbby</h2>

          <p>
            El Usuario es responsable de contar con los servicios y equipos
            necesarios para la navegación por Internet y para acceder a la
            Plataforma. En caso de cualquier incidencia o dificultad para
            acceder a la Plataforma, el Usuario puede informarlo a Wilbby a
            través de los canales de contacto puestos a disposición del Usuario,
            que procederá a analizar la incidencia y dará indicaciones al
            Usuario acerca de cómo resolverla en el plazo más breve posible.
            Wilbby no controla ni es responsable de los contenidos vertidos por
            los Usuarios a través de la Plataforma, siendo estos el único
            responsable de la adecuación legal de dichos contenidos. Wilbby no
            responderá en caso de interrupciones del servicio, errores de
            conexión, falta de disponibilidad o deficiencias en el servicio de
            acceso a Internet, ni por interrupciones de la red de Internet o por
            cualquier otra razón ajena a su control.
          </p>

          <p>
            Wilbby no se hace responsable de los errores de seguridad que se
            puedan producir ni de los daños que puedan causarse al sistema
            informático del Usuario (hardware y software), a los ficheros o
            documentos almacenados en el mismo, como consecuencia de:
          </p>

          <ul>
            <li>
              <p>
                La presencia de un virus en el sistema informático o terminal
                móvil del Usuario que sea utilizado para la conexión a los
                servicios y contenidos de la Plataforma;
              </p>
            </li>
            <li>
              <p>Un mal funcionamiento del navegador;</p>
            </li>
            <li>
              <p>Del uso de versiones no actualizadas del mismo.</p>
            </li>
          </ul>

          <p>20. Responsabilidad por contenidos</p>

          <p>
            Wilbby no tiene obligación de controlar y no controla la utilización
            que los Usuarios hacen de la Plataforma y, por consiguiente, no
            garantiza que los Usuarios utilicen la Plataforma de conformidad con
            lo establecido en las presentes Condiciones Generales de Uso, ni que
            hagan un uso diligente y/o prudente de la misma. Wilbby no tiene la
            obligación de verificar y no verifica la identidad de los Usuarios,
            ni la veracidad, vigencia, exhaustividad y/o autenticidad de los
            datos que los mismos proporcionan.
          </p>

          <p>
            Wilbby no verificará los productos que puedan recogerse o enviarse a
            través de los pedidos realizados a través de la Plataforma. Por esta
            razón, tanto el Usuario como el Mandatario eximen a Wilbby de
            cualquier responsabilidad que pudiera derivarse de la puesta a
            disposición y/o transporte de productos que requieran permisos o
            licencias específicos, así como productos prohibidos por cualquier
            normativa de aplicación, descritos, sin carácter limitativo, en la
            cláusula 15 precedente. En el supuesto que Wilbby tenga conocimiento
            que el Usuario y/o el Mandatario utilizan la Plataforma para el
            transporte de productos prohibidos, Wilbby podrá iniciar una
            investigación interna para esclarecer los hechos, interponer las
            medidas legales necesarias y/o bloquear al Usuario o Mandatario
            correspondiente hasta la finalización de la posible investigación.
            Entre las medidas a disposición de Wilbby se encuentra el poner a
            disposición de las autoridades los datos e informaciones de los
            Usuarios y Mandatarios. Wilbby excluye cualquier responsabilidad por
            los daños y perjuicios de toda naturaleza que pudieran deberse a la
            utilización ilícita de la Plataforma por parte de los Usuarios o que
            puedan deberse a la falta de veracidad, vigencia, exhaustividad y/o
            autenticidad de la información que los Usuarios proporcionan a otros
            Usuarios acerca de sí mismos y, en particular, aunque no de forma
            exclusiva, por los daños y perjuicios de toda naturaleza que puedan
            deberse a la suplantación de la personalidad de un tercero efectuada
            por un Usuario en cualquier clase de comunicación realizada a través
            de la Plataforma. En particular, Wilbby no se hace responsable del
            uso de la Plataforma así como de los pedidos que pudiera realizar un
            tercero desde la cuenta del Usuario. Sin perjuicio de lo anterior,
            Wilbby se reserva la facultad de limitar, total o parcialmente, el
            acceso a la Plataforma a determinados Usuarios, así como de
            cancelar, suspender, bloquear o eliminar determinado tipo de
            contenidos, mediante la utilización de instrumentos tecnológicos
            aptos al efecto, si tuviese conocimiento efectivo o existiesen
            indicios de que la actividad o la información almacenada es ilícita
            o de que lesiona bienes o derechos de un tercero. En este sentido,
            Wilbby podrá establecer los filtros necesarios a fin de evitar que a
            través del servicio puedan verterse en la red contenidos ilícitos o
            nocivos. La puesta a disposición por parte de los Usuarios de
            contenidos a través de la Plataforma supondrá la cesión a favor de
            Wilbby de todos los derechos de explotación derivados de los
            contenidos suministrados en la Plataforma.
          </p>

          <h2>21. Cláusula de "no garantía" en los recados ni en los pagos</h2>

          <p>
            Wilbby no ofrece ninguna garantía con respecto a la autenticidad, la
            exactitud, la novedad, la fiabilidad, legalidad o no violación de
            derechos de terceros por parte de los Mandatarios. En este sentido,
            los Usuarios manifiestan que comprenden que Wilbby es una plataforma
            tecnológica, cuya principal actividad es la intermediación, esto es,
            pone en contacto Usuarios y Mandatarios, por lo que no asume
            responsabilidad alguna derivada de la información proporcionada por
            los Mandatarios ni por los daños o pérdidas que eventualmente
            pudieran sufrir por un incumplimiento de las presentes Condiciones
            Generales de Uso. Al realizar el pedido a través de la Plataforma y
            su aceptación por parte del Mandatario, se establecerá una relación
            directa entre el Usuario y el Mandatario, siendo Wilbby ajena a la
            misma, facilitando únicamente la Plataforma para que las Partes
            puedan comunicarse y realizar cuantos pedidos sean necesarios. Por
            ello Wilbby no será nunca responsable ni de la disponibilidad de los
            Mandatarios ni del cumplimiento adecuado y satisfactorio de los
            recados por parte de éstos. Wilbby no realiza confirmación alguna,
            ni valida ningún Mandatario ni su identidad ni antecedentes. Sin
            perjuicio de lo anterior, Wilbby puede eventualmente realizar
            comprobaciones adicionales y poner en marcha procedimientos
            destinados a ayudar a verificar o a comprobar las identidades de los
            Mandatarios. En este sentido, cuando un Usuario alcanza el estatus
            de Mandatario, simplemente indica que dicho Usuario ha completado el
            proceso de alta correspondiente y ha aceptado las presentes
            Condiciones Generales de Uso y cualquier otra condición particular
            que le resultara aplicable. No implica ni certificación ni aval
            acerca de su fiabilidad, idoneidad y/o seguridad por parte de
            Wilbby. Por ello Wilbby les recomienda a los Usuarios que empleen
            siempre el sentido común y toda su atención a la hora de delegar un
            recado a un determinado Mandatario. Al ser miembros de la
            Plataforma, los Usuarios aceptan que cualquier responsabilidad legal
            que pretendan ejercitar como consecuencia de acciones u omisiones de
            otros Usuarios de la Plataforma o terceros se limitará a dichos
            Usuarios o terceros, no procediendo acción de responsabilidad alguna
            contra Wilbby.
          </p>

          <h2>22. Actualización y Modificación de la Plataforma</h2>

          <p>
            Wilbby se reserva el derecho de modificar, en cualquier momento y
            sin previo aviso, las presentes Condiciones Generales de Uso, la
            Política de Privacidad y la Política de Cookies. Los Usuarios
            deberán leer atentamente estas Condiciones Generales al acceder a la
            Plataforma. En cualquier caso, la aceptación de las Condiciones
            Generales será un paso previo e indispensable al acceso de los
            servicios y contenidos disponibles a través de la Plataforma Wilbby.
            Asimismo, Wilbby se reserva la facultad de efectuar, en cualquier
            momento y sin necesidad de previo aviso, actualizaciones,
            modificaciones o eliminación de información contenida en su
            Plataforma en la configuración y presentación de ésta y de las
            condiciones de acceso, sin asumir responsabilidad alguna por ello.
            Wilbby no garantiza la inexistencia de interrupciones o errores en
            el acceso de la Plataforma o a su contenido, ni que ésta se
            encuentre siempre actualizada, no obstante, Wilbby llevará a cabo,
            siempre que no concurran causas que lo hagan imposible o de difícil
            ejecución, y tan pronto tenga noticia de los errores, desconexiones
            o falta de actualización en los contenidos, todas aquellas labores
            tendentes a subsanar los errores, restablecer la comunicación y
            actualizar los contenidos.
          </p>

          <h2>23. Propiedad Intelectual</h2>

          <p>
            Wilbby es titular o licenciataria de todos los derechos de propiedad
            intelectual e industrial incluidos en la Plataforma así como sobre
            los contenidos accesibles a través de la misma. Los derechos de
            propiedad intelectual de la Plataforma, así como: textos, imágenes,
            diseño gráfico, estructura de navegación, información y contenidos
            que se recogen en ella son titularidad de Wilbby, a quien
            corresponde el ejercicio exclusivo de los derechos de explotación de
            los mismos en cualquier forma y, en especial, los derechos de
            reproducción, distribución, comunicación pública y transformación,
            de conformidad con la legislación española de derechos de propiedad
            intelectual e industrial. Aún cuanto descrito anteriormente, Wilbby
            podría no ser titular ni licenciatario de contenidos como nombres o
            imágenes, entre otros, de compañías con las cuales Wilbby no tiene
            relación comercial. En esos supuestos, Wilbby adquiere los
            contenidos de fuentes accesibles al público y en ningún caso se
            entenderá que Wilbby tenga relación con ningún derecho titularidad
            de Wilbby. La autorización al Usuario para el acceso a la Plataforma
            no supone renuncia, transmisión, licencia o cesión total ni parcial
            sobre derechos de propiedad intelectual o industrial por parte de
            Wilbby. No está permitido suprimir, eludir o manipular de ningún
            modo los contenidos de la Plataforma de Wilbby. Asimismo está
            prohibido modificar, copiar, reutilizar, explotar, reproducir,
            comunicar públicamente, hacer segundas o posteriores publicaciones,
            cargar archivos, enviar por correo, transmitir, usar, tratar o
            distribuir de cualquier forma la totalidad o parte de los contenidos
            incluidos en la Plataforma de Wilbby para propósitos públicos o
            comerciales, si no se cuenta con la autorización expresa y por
            escrito de Wilbby o, en su caso, del titular de los derechos a que
            corresponda. El Usuario que proceda a compartir cualquier tipo de
            contenido a través de la Plataforma, asegura que ostenta los
            derechos necesarios para hacerlo, quedando exento Wilbby de
            cualquier responsabilidad sobre el contenido y legalidad de la
            información ofrecida. La facilitación de contenidos por parte de los
            Usuarios a través de la Plataforma supondrá la cesión a Wilbby, con
            carácter gratuito, y con la máxima amplitud permitida por la
            legislación vigente, de los derechos de explotación de propiedad
            intelectual o industrial derivados de tales contenidos.
          </p>

          <h2>24. Independencia de las Cláusulas</h2>
          <p>
            Si cualquiera de las cláusulas de las presentes Condiciones
            Generales fuera nula de pleno derecho o anulable, se tendrá por no
            puesta. Dicha declaración de nulidad no invalidará el resto de
            cláusulas, que mantendrá su vigencia y eficacia entre las Partes.
          </p>

          <h2>25. Legislación Aplicable</h2>

          <p>
            La relación entre Wilbby y el Usuario, se regirá e interpretará de
            conformidad con las Condiciones Generales que en materia de
            interpretación, validez y ejecución se regirán por la legislación
            española; y cualquier controversia se someterá a los Tribunales de
            Barcelona, salvo que el Usuario solicite los tribunales de su
            domicilio de residencia.
          </p>

          <h2>26. Resolución extrajudicial de conflictos</h2>

          <p>
            Los conflictos o controversias que tuvieran lugar fuera de la Unión
            Europea quedarán sujetos a su resolución por parte de la Corte
            Española de Arbitraje de la Cámara de Comercio.
          </p>

          <h1>Términos y Condiciones Específicos de “WILBBY PLUS”</h1>

          <p>
            Los presentes términos y condiciones serán de aplicación para
            aquellos Usuarios que soliciten la suscripción a WILBBY PLUS. Los
            presentes Términos y Condiciones no sustituyen las “Condiciones
            Generales de Uso y Contratación” de la aplicación, en cualquier
            caso, los complementan, y podrán ser modificados y ampliados en
            cualquier momento por Wilbby.
          </p>

          <h3>1. ¿Qué es WILBBY PLUS?</h3>

          <p>
            WILBBY PLUS es un servicio que ofrece la Plataforma Wilbby mediante
            el que los Usuarios de la misma a cambio de abonar una tarifa
            mensual fija, pueden disfrutar de forma ilimitada del servicio de
            entrega para todos aquellos productos que encargan a los
            repartidores siempre que el logo de WILBBY PLUS conste expresamente
            en la Plataforma de la tienda en la que se oferten y todo ello sin
            coste adicional a la citada tarifa fija mensual.
          </p>

          <p>
            WILBBY PLUS no aplicará a tiendas y servicios que no tengan el logo
            de WILBBY PLUS en el momento de realizar la orden. El servicio
            WILBBY PLUS podrá utilizarse siempre y cuando se cumplan las
            disposiciones descritas en los presentes Términos y Condiciones.
          </p>

          <p>2. Proceso de inscripción</p>

          <p>
            Para poder darse de alta en el servicio WILBBY PLUS, se debe tener
            una cuenta activa vigente en Wilbby, ser mayor de edad y solicitar
            la suscripción a WILBBY PLUS mediante la sección “mi cuenta” en el
            perfil individualizado de cada Usuario de Wilbby.
          </p>

          <p>El proceso de activación de WILBBY PLUS es el siguiente:</p>

          <ul>
            <li>
              <p>
                Tras acceder a la sección “mi cuenta” y “WILBBY PLUS” se deberá
                seleccionar el apartado “Suscribir y pagar” y proceder al pago
                de la referida suscripción mensual.
              </p>
            </li>

            <li>
              <p>
                La fecha de pago será la fecha de inicio de la suscripción a
                WILBBY PLUS (en adelante “la Fecha de Activación”) de fecha a
                fecha. En la Fecha de Activación, los Usuarios que se hayan
                inscrito recibirán un email en el correo designado por cada uno
                en su cuenta Wilbby con la confirmación de la suscripción.
              </p>
            </li>
            <li>
              <p>
                Tras la Fecha de Activación, se dispondrán de 30 días naturales
                de fecha a fecha para utilizar el servicio WILBBY PLUS.
              </p>
            </li>
            <li>
              <p>
                El Usuario dispondrá de toda la información relacionada con su
                suscripción a WILBBY PLUS en la sección “mi cuenta” de la
                aplicación. En ella podrá consultar el tiempo restante de su
                Período de Prueba Gratuito en caso de corresponder y conforme lo
                establecido en el punto 10 de los presentes, cancelar su
                suscripción a WILBBY PLUS o actualizar su método de pago, entre
                otros.
              </p>
            </li>
          </ul>

          <p> 3. Período de suscripción</p>

          <p>
            3.1. El período de suscripción a WILBBY PLUS tendrá una duración de
            un mes de fecha a fecha (en adelante “Período de Suscripción”) a
            contar desde la Fecha de Activación, y se renovará mensualmente
            salvo que el Usuario cancele la suscripción. Así, si la Fecha de
            Activación es el 10 de octubre, el Período de Suscripción, se
            renovará el 10 de noviembre.
          </p>

          <p>
            3.2. Mediante la aceptación de los presentes términos y condiciones,
            el Usuario es conocedor que la suscripción a WILBBY PLUS se RENOVARÁ
            AUTOMÁTICAMENTE Y LA TARIFA DE SUSCRIPCIÓN APLICABLE SERÁ
            AUTOMÁTICAMENTE CARGADA en la cuenta designada por el Usuario.
          </p>
          <p>
            3.3. Si el Usuario desea que la suscripción no se renueve
            automáticamente y, por lo tanto, darse de baja del servicio WILBBY
            PLUS, éste puede ejercer su derecho de desistimiento en cualquier
            momento de acuerdo con lo previsto en las disposiciones de la
            cláusula siguiente y siempre y cuando notifique a Wilbby dos días
            hábiles antes de la fecha de renovación. De no dar cumplimiento con
            dicha notificación, el Usuario deberá abonar la tarifa de
            suscripción del mes siguiente.
          </p>

          <p>4. Derecho de desistimiento</p>

          <p>
            4.1. Si durante el Período de Suscripción al servicio de WILBBY
            PLUS, el Usuario desea ejercer su derecho de desistimiento, deberá
            acceder al perfil de su cuenta y seleccionar “darse de baja”.
          </p>

          <p>
            4.2. La desactivación de la cuenta se producirá cuando finalice el
            Período de Suscripción, por lo que tras finalizar el proceso de
            “baja” podrá seguir haciendo uso de su cuenta WILBBY PLUS hasta que
            se cumpla un mes de fecha a fecha desde la Fecha de Activación o
            renovación.
          </p>

          <p>
            4.3. El Usuario deberá ejercer el derecho de desistimiento como
            mínimo dos (2) días hábiles antes de la fecha de renovación de la
            suscripción a los efectos de que no se le cargue la tarifa.
          </p>

          <p>
            4.4. Los pagos no son reembolsables y no se otorgarán reembolsos ni
            créditos por los períodos de membresía utilizados parcialmente o por
            el no uso durante un mes de fecha a fecha, tampoco se facilitan
            devoluciones ni abonos por períodos parciales de suscripción
            mensual.
          </p>

          <p>
            4.5. Si el Usuario se ha dado de baja la suscripción al servicio y
            quisiera continuar con ella, puede volver a registrarse para renovar
            la suscripción a WILBBY PLUS de acuerdo con lo previsto en los
            presentes términos y condiciones.
          </p>

          <p>5. Tarifas del servicio</p>

          <p>
            5.1. La tarifa de WILBBY PLUS será la que se indique en la oferta
            comercial o en la sección “Mi cuenta”, salvo que se haya concedido
            al Usuario un período de prueba gratuito de WILBBY PLUS según el
            apartado 10 “Período de Prueba Gratuito” del presente documento.
          </p>

          <p>
            5.2. Las tarifas de WILBBY PLUS se ofertan con la imposición
            indirecta que en su caso corresponda incluida.
          </p>

          <p>
            5.3. La tarifa se cobrará de forma automática y mensual en la Fecha
            de Activación, en la cuenta designada por el Usuario en la Fecha de
            Activación y desde la misma, de fecha a fecha.
          </p>

          <p>
            5.4. Los Usuarios podrán hacer uso de WILBBY PLUS incluso en
            aquellos territorios en que se realice el pago del producto/pedido
            en efectivo, sin que ello tenga implicación alguna sobre el
            servicio.
          </p>

          <p>
            5.5. Wilbby se reserva el derecho a modificar las tarifas en
            cualquier momento, previa notificación a los suscriptores. Una vez
            se haya producido la notificación, Wilbby podrá cargar las nuevas
            tarifas.
          </p>

          <p>6. Condiciones específicas del servicio</p>

          <p>
            Serán de aplicación las siguientes condiciones particulares al
            servicio WILBBY PLUS:
          </p>
          <ul>
            <li>
              <p>
                El servicio WILBBY PLUS sólo será de aplicación para aquellas
                compras presenciales realizadas a través del
                repartidor/mensajero en locales con los que Wilbby tenga un
                acuerdo comercial cuyo importe supere los 10,00 euros para
                pedidos de restaurantes o por un importe que supere los 20,00
                euros para pedidos de supermercado, o su equivalente en la
                moneda de curso legal del país donde se hubiera realizado la
                activación del servicio.
              </p>
            </li>
            <li>
              <p>
                La activación de WILBBY PLUS sólo estará disponible en algunos
                países en los que opera Wilbby.
              </p>
            </li>
            <li>
              <p>
                La tarifa correspondiente a la suscripción a WILBBY PLUS sólo
                será aplicable en el país desde el que se haya realizado la
                activación de la suscripción WILBBY PLUS. Esto es, si se hubiera
                activado la suscripción en España y el Usuario estuviera de
                viaje en Italia, la tarifa correspondiente a la suscripción a
                WILBBY PLUS única y exclusivamente será aplicable en España y no
                a Italia.
              </p>
            </li>
            <li>
              <p>
                Sólo se ofrecerá a particulares, y únicamente destinado a uso
                particular, no comercial,
              </p>
            </li>
            <li>
              <p>
                En ningún caso la suscripción podrá compartirse ni cederse a
                personas que no sean los titulares de la misma, quedando
                expresamente excluidos del mismo las empresas, o el servicio
                “B2B”.
              </p>
            </li>
          </ul>

          <p> 7. Condiciones generales del servicio</p>

          <p>
            7.1. La tarifa plana mensual únicamente aplica al importe relativo a
            la recogida y entrega, no al importe de los productos que, en su
            caso, el Usuario pudiera solicitar.
          </p>

          <p>
            7.2. Wilbby podrá modificar los presentes términos y condiciones de
            WILBBY PLUS unilateralmente en cualquier momento, previa
            comunicación a los miembros del servicio de WILBBY PLUS. Las
            modificaciones serán de aplicación para los particulares activos en
            la Plataforma así como, en su caso, los que la utilizaran por
            primera vez.
          </p>

          <p>
            7.3. La suscripción a WILBBY PLUS es de uso exclusivo e
            intransferible del Usuario debidamente identificado y en ningún caso
            podrá ceder su uso a terceros.
          </p>

          <p>
            7.4. Wilbby se reserva el derecho a cancelar las cuentas de los
            suscriptores de WILBBY PLUS, así como a desactivar el servicio,
            previa comunicación a los miembros suscriptores del mismo.
          </p>

          <p>
            7.5. Se deja expresa constancia que, a excepción de los términos y
            condiciones especiales aquí expuestos, se aplicarán siempre
            supletoriamente y en todo aquello que no fuera modificado por los
            presentes términos, los Términos y Condiciones Generales de la
            Plataforma Wilbby.
          </p>

          <p>8. Fraude y bloqueo de cuentas</p>
          <p>
            En el caso que Wilbby detectase indicios de que se está realizando
            un uso indebido, inapropiado y/o fraudulento de la cuenta de WILBBY
            PLUS, podrá desactivar de forma automática y sin necesidad de
            comunicación previa, la suscripción, así como en su caso,
            adicionalmente la cuenta del Usuario en cualquier momento,
            reservándose el derecho a iniciar acciones legales en función de la
            gravedad de las circunstancias analizadas. Asimismo, Wilbby se
            reserva el derecho de cancelar el servicio y las suscripciones a su
            sola discreción y previo aviso a los Usuarios.
          </p>

          <p>9. Atención al Usuario</p>

          <p>
            Para cualquier incidencia relativa al servicio de WILBBY PLUS,
            Wilbby pondrá a disposición de todos los Usuarios el Formulario de
            Contacto correspondiente.
          </p>

          <p>10. Período de prueba gratuito</p>

          <p>
            10.1. Wilbby, a su discrecionalidad, podrá ofrecer el servicio
            WILBBY PLUS de forma gratuita, durante el tiempo que estime
            necesario.
          </p>

          <p>
            10.2. En cualquier caso, al finalizar el Período de Prueba Gratuito,
            se exigirá automáticamente a los Usuarios la tarifa, así como los
            mismos mismos términos y condiciones según correspondan que al resto
            de los Usuarios de WILBBY PLUS.
          </p>

          <p>
            10.3. Si se cancela la suscripción a WILBBY PLUS ya sea
            accidentalmente o por algún otro motivo antes de que expire el
            Periodo de Prueba Gratuito correspondiente, se considerará que se ha
            completado la oferta de prueba gratuita y no se dispondrá en ningún
            caso de un nuevo periodo de prueba gratuito.
          </p>

          <p>
            10.4. Wilbby se reserva el derecho a elegir aleatoriamente aquellos
            Usuarios beneficiarios del Período de Prueba Gratuito, así como a
            invitar, en su caso, a otras promociones relacionadas con el
            servicio.
          </p>

          <p>11. Legislación aplicable</p>

          <p>
            La relación entre Wilbby y el miembro de WILBBY PLUS, se regirá e
            interpretará de conformidad con los presentes Términos y
            Condiciones, que en materia de interpretación, validez y ejecución
            se regirán por la legislación española; y cualquier controversia se
            someterá a los Tribunales de Barcelona, salvo que el Usuario
            solicite los tribunales de su domicilio de residencia.
          </p>

          <h1>
            Términos y Condiciones específicos para PEDIDOS “PICKUP” Y “INSTORE”
          </h1>

          <p>
            El Usuariotiene la opción de solicitar a través de la modalidad
            PICKUP y/o INSTORE una serie de productos y/o servicios ofrecidos
            por determinados establecimientos con los que Wilbby tenga un
            acuerdo comercial. Los servicios PICKUP e INSTORE permiten a los
            Usuarios recoger los pedidos ellos mismos presencialmente en el
            local de restauración. Dichos servicios sólo estarán disponibles a
            través de aquellos establecimientos para los que esta opción se
            encuentre habilitada y en los países seleccionados por Wilbby.
            Asimismo, dichos servicios solo se ofrecerán a particulares y para
            un uso particular. En ningún caso se ofrecerán para un uso
            comercial. Los servicios PICKUP e INSTORE podrán utilizarse siempre
            y cuando se cumplan las disposiciones particulares descritas en el
            presente apartado, sin perjuicio de la aplicación de forma
            supletoria de los Términos y Condiciones Generales de la Plataforma
            Wilbby en todo lo no expresamente previsto en el presente apartado.
            Para las incidencias relativas al servicio PICKUP y/o INSTORE,
            Wilbby pondrá a disposición de todos los Usuarios el Formulario de
            Contacto presente en la Plataforma. Wilbby podrá modificar los
            presentes Términos y Condiciones de PEDIDOS “PICKUP” E “INSTORE”
            unilateralmente en cualquier momento, previa comunicación a los
            Usuarios de la Plataforma Wilbby. Asimismo, Wilbby podrá activar
            ulteriores funcionalidades relativas al servicio PICKUP y/o INSTORE
            que serán comunicadas a los Usuarios y añadidas a los presentes
            términos y condiciones.
          </p>

          <p>
            Una vez publicados, las modificaciones serán de aplicación para
            todos los Usuarios que utilicen la Plataforma.
          </p>

          <ul>
            <li>
              <p>Pedidos INSTORE</p>
            </li>
          </ul>

          <p>
            Cuando así esté habilitado en la aplicación en los pedidos INSTORE
            el Usuario podrá realizar el pedido directamente desde los
            restaurantes que se hayan adherido a esta modalidad y recogerlo en
            el momento en la tienda.
          </p>

          <p>¿Cómo funciona el servicio INSTORE?</p>

          <p>
            Para poder acceder al servicio INSTORE, el Usuario deberá escanear
            el código QR que el establecimiento tendrá expuesto en sus locales
            en una zona visible al público. Una vez escaneado el código QR, este
            enlazará con el menú del restaurante en el que se encuentre el
            Usuario para realizar el pedido. El Usuario podrá seleccionar los
            ítems del restaurante o tienda que desee y confirmar la compra a
            través de la opción INSTORE. En caso de que el escaneo del código QR
            no se pueda realizar por algún tipo de error o defecto, el Usuario
            podrá consultar la posibilidad de realizar el pedido buscando el
            restaurante directamente desde la Plataforma Wilbby en su
            dispositivo. Una vez que el pedido se haya confirmado, según el
            método de pago habilitado al efecto, será posible proceder a su
            cancelación a través de la Plataforma, únicamente hasta que el
            restaurante o establecimiento no haya aceptado preparar el pedido.
            Posteriormente, si el Usuario desea modificar su pedido o si el
            restaurante tarda en la preparación del mismo, el Usuario deberá
            dirigirse directamente a los encargados del restaurante en cuestión.
            Una vez que el pedido se haya confirmado, el número de pedido
            aparecerá en la pantalla del dispositivo utilizado por el Usuario,
            el cual tendrá que mostrarlo al personal del establecimiento al
            recoger el pedido. El precio de los pedidos adquiridos según la
            modalidad INSTORE no incluirán el coste adicional de envío. No será
            posible el uso de códigos promocionales en relación a los pedidos
            del servicio INSTORE.
          </p>

          <p>Pedidos PICKUP</p>

          <p>
            Cuando así esté habilitado en la aplicación en los pedidos PICKUP,
            el Usuario podrá realizar el pedido directamente desde la Plataforma
            en los establecimientos que se hayan adherido a esta modalidad y
            recogerlo en la tienda en los plazos comunicados.
          </p>

          <p>
            Para poder acceder al servicio PICKUP, el Usuario deberá seleccionar
            dicha opción a través de la Plataforma en los establecimientos que
            ofrezcan esta opción, junto con los productos que desea adquirir en
            dicho establecimiento y confirmar el pedido a través de la misma
            Plataforma. Una vez que el pedido se haya confirmado, según el
            método de pago habilitado al efecto, será posible proceder a su
            cancelación a través de la Plataforma únicamente hasta que el
            restaurante o establecimiento no haya aceptado y empezado a preparar
            el pedido. Posteriormente, si el Usuario desea modificar su pedido o
            si el restaurante tarda en la preparación del mismo, el Usuario
            deberá dirigirse directamente a los encargados del restaurante en
            cuestión. Una vez que el pedido se haya confirmado, el número de
            pedido aparecerá en la pantalla del dispositivo utilizado por el
            Usuario, el cual tendrá que mostrarlo al personal del
            establecimiento al recoger el pedido. El precio de los pedidos
            adquiridos según la modalidad PICKUP no incluirán el coste adicional
            de envío.
          </p>

          <h2>
            TÉRMINOS Y CONDICIONES SERVICIO FOOD RESCUE / RESCATE DE COMIDA
          </h2>

          <p>
            Los presentes términos y condiciones serán de aplicación para
            aquellos Usuarios que realicen un pedido FOOD RESCUE / RESCATE DE
            COMIDA a través de la aplicación de Wilbby. Los presentes Términos y
            Condiciones no sustituyen las “Condiciones Generales de Uso y
            Contratación” de la aplicación, en cualquier caso, los complementan,
            y podrán ser modificados y ampliados en cualquier momento por
            Wilbby. Los Términos y Condiciones Generales de Uso y Contratación
            aplicarán en todo aquello que no esté expresamente regulado en estos
            Términos y Condiciones específicos del servicio FOOD RESCUE /
            RESCATE DE COMIDA.
          </p>

          <p>¿Qué es FOOD RESCUE / RESCATE DE COMIDA?</p>

          <p>
            FOOD RESCUE / RESCATE DE COMIDA es un servicio que ofrece la
            Plataforma Wilbby mediante el que los Usuarios de ésta podrán
            realizar sus pedidos sobre el excedente de alimentos y excedente de
            otros productos de los establecimientos alimentarios que aparecen en
            la Plataforma de Wilbby. El Usuario reconoce que Wilbby ha lanzado
            este proyecto denominado “FOOD RESCUE” cuya finalidad principal
            consiste en la reducción del desperdicio alimentario. Concretamente,
            dicho proyecto consiste en poner a disposición de los Usuarios una
            opción en la Plataforma Wilbby mediante la que los Usuarios podrán
            pedir en los establecimientos indicados en la Plataforma un(os)
            pack(s) de comida que no se haya(n) vendido durante el día a precios
            rebajados y cuyo contenido no se detalla al usuario a la hora de
            efectuar el pedido (en adelante, “pack/s”, “Productos” o la “Caja
            Mágica”). En el momento de realizar un pedido de FOOD RESCUE /
            RESCATE DE COMIDA, el Usuario acepta los presentes Términos y
            Condiciones.
          </p>

          <p>Servicio</p>

          <p>
            Wilbby únicamente facilitará la celebración de un contrato entre el
            Usuario y el Establecimiento y no tiene responsabilidad alguna
            respecto del contenido del pack o del cumplimiento del contrato
            entre el Establecimiento y el Usuario. El Usuario podrá pedir los
            productos de los establecimientos que aparezcan en la Plataforma
            Wilbby a través de ésta. Wilbby no tiene responsabilidad por el
            cumplimiento de las obligaciones contractuales frente al Usuario
            respecto de los Productos, incluyendo la fabricación, venta, compra,
            almacenamiento, elaboración, producción, procesamiento, marcado,
            calidad, ingredientes, alérgenos o tratamiento de los Productos, y
            el cumplimiento de la legislación aplicable, salvo que se designe
            explícitamente a Wilbby como el fabricante o el vendedor del
            Producto en la Plataforma. El Usuario podrá encontrar en la
            Plataforma, un(os) pack(s) de comida que no se haya(n) vendido
            durante el día, a precios rebajados y cuyo contenido no se detalla
            al Usuario a la hora de efectuar el pedido. Los Establecimientos
            serán los responsables de proporcionar la información sobre los
            Productos y de garantizar que ésta sea objetivamente precisa y esté
            actualizada. Wilbby no asume responsabilidad sobre la información
            proporcionada por los Establecimientos y, por consiguiente, no tiene
            responsabilidad alguna por el contenido o la disponibilidad de la
            información relativa a los Productos. Wilbby no asumirá
            responsabilidad alguna por las reacciones adversas que pudieran
            sufrir los Usuarios derivadas de los Productos por la razón que
            fuere, incluyendo el consumo contrario al etiquetado o a la
            información indicada directamente por el Establecimiento. El
            Establecimiento es quien fabrica, vende, almacena, elabora, produce,
            procesa, marca, empaqueta, y trata los Productos, por lo que Wilbby
            se exime de cualquier responsabilidad por el daño o reacción aversa
            que puedan causar dichos productos.
          </p>

          <p>Recogida del pedido</p>

          <p>
            El Usuario podrá escoger entre dos opciones a la hora de realizar el
            pedido de FOOD RESCUE / RESCATE DE COMIDA:
          </p>

          <p>
            Recogida en el establecimiento. En este caso, el Usuario deberá ir a
            recoger su pedido de FOOD RESCUE / RESCATE DE COMIDA físicamente al
            Establecimiento indicado en la Plataforma Wilbby. Esto permite a los
            Usuarios recoger los pedidos ellos mismos presencialmente en el
            local de restauración.
          </p>

          <p>
            Entrega a domicilio. En este caso, el Usuario recibirá su pedido,
            que recogerá un Mandatario en el Establecimiento indicado, en la
            dirección de entrega que indique en la Plataforma Wilbby.
          </p>

          <h1>ANEXO I - DATOS DE LAS AFILIADAS DEL GRUPO WILBBY</h1>

          <p>España</p>

          <p>Denominación Social: Locatefit, S.L.</p>
          <p>NIF o Identificación fiscal: B09603440</p>
          <p>
            Sede Legal: C/ Francisco Sarmiento 13 8A, 09005, Burgos, España.
          </p>
          <h1>
            ANEXO II - PROMEDIO DE COSTES DE CANCELACIÓN DE PEDIDOS POR PAÍS (EN
            MONEDA LOCAL)
          </h1>

          <p>España 3</p>

          <a
            href="https://doc-wilbby.s3.eu-west-3.amazonaws.com/Politica-de-parivacidad-wilbby.pdf"
            download
          >
            Descargar en PDF
          </a>
        </div>
      </div>
    </div>
  );
}
