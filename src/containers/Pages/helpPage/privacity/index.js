import React, { useEffect } from "react";
import Barner from "../../../Components/Barner";

import "../index.css";

export default function Privacity() {
  useEffect(() => {
    document.title = "Política de Privacidad";
  }, []);
  return (
    <div>
      <Barner title="Aviso Legal y Política de Privacidad" />
      <div className="containers_hepls">
        <div className="cookies">
          <h2>Aviso Legal y Política de Privacidad</h2>
          <h1>
            <strong>
              <span>1. Información básica</span>
            </strong>
          </h1>
          <p>
            <strong>
              <span>
                Primera capa - Información básica sobre Protección de Datos
              </span>
            </strong>
          </p>
          <p>
            <strong>
              <span>Identidad: </span>
            </strong>
            <span>Locatefit, S.L.- CIF: B09603440</span>
          </p>
          <p>
            <strong>
              <span>Domicilio: </span>
            </strong>
            <span>C/ Francisco Sarmiento 13 8A, 09005, Burgos</span>
          </p>
          <p>
            <strong>
              <span>Teléfono: </span>
            </strong>
            <span>(+34) 664 02 81 61</span>
          </p>
          <p>
            <strong>
              <span>Email: </span>
            </strong>
            <span>legal@wilbby.com</span>
          </p>
          <p>
            <strong>
              <span>CIF: </span>
            </strong>
            <span>B09603440</span>
          </p>
          <p>
            <strong>
              <span>Finalidad: </span>
            </strong>
            <span>
              <span>Gestión y prestación de servicios solicitados</span>
            </span>
          </p>
          <p>
            <strong>
              <span>Legitimación: </span>
            </strong>
            <span>
              <span>
                Cumplimiento de la relación contractual, interés legítimo y
                consentimiento del Usuario.
              </span>
            </span>
          </p>
          <p>
            <strong>
              <span>Derechos: </span>
            </strong>
            <span>
              <span>
                Acceder, rectificar y suprimir los datos, así como otros
                derechos, como se explica en la información adicional.
              </span>
            </span>
          </p>
          <p>
            <strong>
              <span>Información Adicional: </span>
            </strong>
            <span>
              <span>
                Puede consultar la información adicional y detallada sobre
                Protección de datos en los apartados siguientes.
              </span>
            </span>
          </p>
          <h1>
            <strong>
              <span>2. Responsable del tratamiento</span>
            </strong>
          </h1>
          <p>
            <strong>
              <span>Responsable del tratamiento de sus datos</span>
            </strong>
          </p>
          <p>
            <strong>
              <span>Identidad: </span>
            </strong>
            <span>
              <span>Locatefit, S.L., - CIF: B09603440</span>
            </span>
          </p>
          <p>
            <strong>
              <span>Dirección Postal: </span>
            </strong>
            <span>
              <span>C/ Francisco Sarmiento 13 8A, 09005, Burgos</span>
            </span>
          </p>
          <p>
            <strong>
              <span>Teléfono: </span>
            </strong>
            <span>
              <span>(+34) 664 02 81 61</span>
            </span>
          </p>
          <p>
            <strong>
              <span>Formulario de Contacto: </span>
            </strong>
            <span>
              <span>
                <a href="/contacto">www.wilbby.com/contact</a>
              </span>
            </span>
          </p>
          <p>
            <strong>
              <span>Contacto DPO: </span>
            </strong>
            <span>
              <span>legal@wilbby.com</span>
            </span>
          </p>
          <p>
            El DPD interno de la Empresa ostenta una dirección de correo
            habilitada al efecto para que la comunicación con los usuarios sea
            fluida, directa y ágil: legal@wilbby.com.
          </p>
          <p>
            Adicionalmente a la información anterior, los detalles de cada una
            de las entidades del Grupo WILBBY se detallan en el Anexo 1 a la
            presente Política de Privacidad.
          </p>
          <p>
            Asimismo, WILBBY podrá compartir los datos de los usuarios
            (“Usuarios”) que se registren en la web o app (“Plataforma”) y de
            las personas que contacten con WILBBY a través de los formularios
            presentes en su Plataforma con cada una de las filiales y empresas
            del Grupo WILBBY a los efectos de poder ofrecer los servicios
            solicitados por los Usuarios a través de la Plataforma.
          </p>
          <p>
            <span>
              Se prohíbe expresamente la utilización de los contenidos de este
              sitio Web para su utilización con fines comerciales o para su
              distribución, transformación o comunicación.
            </span>
          </p>
          <p>
            En la presente política de privacidad se facilita información
            relativa al tratamiento de los datos personales de nuestros
            Usuarios, candidatos a puestos de trabajo, Usuarios de redes
            sociales que interactúan con nosotros y usuarios del formulario de
            contacto de nuestra web, siguiendo lo estipulado por el Reglamento
            General de Protección de Datos (“RGPD”).
          </p>
          <h1>
            3. Tratamiento de datos de Usuarios y personas que contacten con
            WILBBY
          </h1>
          <p>
            <strong>
              <span>3.1 Datos tratados</span>
            </strong>
          </p>
          <p>
            <span>a) Información los Usuarios nos facilitan directamente:</span>
          </p>
          <ul>
            <li>
              <p>
                Datos de Registro: la información que el Usuario nos facilita
                cuando se crea una cuenta en la Plataforma de WILBBY: Nombre de
                usuario y correo electrónico.
              </p>
            </li>
            <li>
              <p>
                Información del Perfil del Usuario: la información que el
                Usuario añade en la Plataforma a efectos de poder utilizar el
                servicio de WILBBY, esto es, el teléfono móvil y la dirección de
                entrega del Usuario. El Usuario puede ver y editar los datos
                personales de su perfil cuando estime oportuno. WILBBY no
                almacena los datos de la tarjeta de crédito del Usuario, pero
                éstos se facilitan a prestadores de servicios de pago
                electrónico con licencia, que recibirán directamente los datos
                incluidos y los almacenarán para facilitar el proceso de pago al
                Usuario y gestionarán el mismo en nombre de WILBBY. En ningún
                caso estos datos se almacenan en los servidores de WILBBY. El
                Usuario podrá, en cualquier momento, eliminar los datos de sus
                tarjetas vinculados a su cuenta. Con esa acción, el proveedor de
                servicios procederá al borrado de los datos y será necesario
                volver a introducirlos o seleccionarlos para poder realizar
                nuevos pedidos a través de la Plataforma. En cualquier momento
                el Usuario podrá solicitar las políticas de privacidad de estos
                proveedores.
              </p>
            </li>
            <li>
              <p>
                Información adicional que el Usuario quiere compartir: la
                información que podría facilitar el Usuario a WILBBY con otros
                fines. Por ejemplo, una fotografía suya o la dirección de
                facturación, en caso de solicitar recibir facturas de WILBBY.
              </p>
            </li>
            <li>
              <p>
                Información acerca de las comunicaciones realizadas con WILBBY:
                WILBBY tendrá acceso a información que los Usuarios faciliten
                para la resolución de dudas o quejas sobre el uso de la
                plataforma, ya sea a través del formulario de contacto, mediante
                correo electrónico o a través del servicio de atención al
                cliente de forma telefónica.
              </p>
            </li>
            <li>
              <p>
                Información sobre accidentes que involucren alguno de las partes
                que intervienen en la prestación de servicios a través de la
                Plataforma para la presentación de siniestros o otras
                actuaciones con las aseguradoras contratadas por WILBBY.
              </p>
            </li>
            <li>
              <p>
                Transcripción y grabación de las conversaciones mantenidas por
                el USUARIO con WILBBY para la tramitación de incidencias, dudas
                u otras consultas que pudiera realizar. Información sobre las
                comunicaciones entre usuarios y Mandatarios: WILBBY tendrá
                acceso a las comunicaciones que los Usuarios realicen con los
                Mandatarios que colaboran con la Plataforma a través del chat
                previsto en la Plataforma.
              </p>
            </li>
          </ul>
          <p>
            1. b) Información que los Usuarios nos facilitan indirectamente:
          </p>
          <p>
            - Datos derivados del Uso de la Plataforma: WILBBY recoge los datos
            derivados del Uso de la Plataforma por parte del Usuario cada vez
            que éste interactúa con la Plataforma.
          </p>
          <p>
            - Datos de la aplicación y del dispositivo: WILBBY almacena los
            datos del dispositivo y de la Aplicación que el Usuario utiliza para
            acceder a los servicios. Estos son:
          </p>
          <ul>
            <li>
              <p>
                Dirección IP de Internet que utiliza el Usuario para conectarse
                a Internet con su ordenador o móvil.
              </p>
            </li>
            <li>
              <p>
                Información de su ordenador o móvil, como su conexión a
                Internet, su tipo de navegador, la versión y el sistema
                operativo, y el tipo de dispositivo.
              </p>
            </li>
            <li>
              <p>
                El Clikstream completo de localizadores de recursos uniformes
                (URL), incluyendo la fecha y la hora.
              </p>
            </li>
            <li>
              <p>
                Datos de la cuenta del Usuario: la información de los pedidos
                realizados por parte del Usuario, así, como las valoraciones y/o
                comentarios que realice acerca de los mismos.
              </p>
            </li>
            <li>
              <p>El historial de navegación y las preferencias del Usuario.</p>
            </li>
          </ul>
          <p>
            - Datos derivados del origen del Usuario: si el Usuario llega a la
            Plataforma de WILBBY a través de una fuente externa (como sería por
            ejemplo un enlace de otra página web o de una red social), WILBBY
            recopila los datos de la fuente de la que procede el Usuario de
            WILBBY.- Datos derivados de la gestión de incidencias: si el Usuario
            se dirige a la Plataforma de WILBBY a través del Formulario de
            Contacto o a través del teléfono de WILBBY, WILBBY recopilará los
            mensajes recibidos en el formato utilizado por el Usuario y los
            podrá utilizar y almacenar para gestionar incidencias presentes o
            futuras.
          </p>
          <p>
            - Datos derivados de las “cookies”: WILBBY utiliza cookies propias y
            de terceros para facilitar la navegación a sus usuarios y con fines
            estadísticos (ver <a href="/cookies">Política de Cookies</a>).
          </p>
          <p>
            - Datos derivados de terceros externos: WILBBY podría recopilar
            información o datos de carácter personal de terceros externos
            únicamente si el Usuario autoriza a esos terceros a compartir la
            información citada con WILBBY. Por ejemplo, en caso de que el
            Usuario se cree una cuenta a través de su Facebook, Facebook podría
            cedernos datos de carácter personal de dicho Usuario en caso de que
            éste los tuviera en su perfil de Facebook (nombre, genero, edad…).
          </p>
          <p>
            Del mismo modo, si el usuario accede a WILBBY a través de los
            productos y servicios ofrecidos por Google, éste podrá enviar datos
            de navegación del Usuario a WILBBY, con el acceso a la plataforma a
            través de los enlaces creados por Google.
          </p>
          <p>
            La información facilitada por el tercero externo podrá ser
            controlada por el Usuario según la propia política de privacidad del
            tercero externo.
          </p>
          <p>
            - Datos sobre la Geolocalización: siempre y cuando los Usuarios lo
            autoricen, WILBBY recogerá datos relacionados con su localización,
            incluyendo la localización geográfica en tiempo real del ordenador o
            dispositivo móvil de los Usuario.
          </p>
          <p>3.2 Finalidad</p>
          <p>3.2.1. Utilizar la Plataforma de WILBBY</p>
          <p>
            WILBBY utiliza los datos que recopila de los Usuarios para que
            puedan acceder y comunicarse con la plataforma de WILBBY, y para
            prestar los servicios que soliciten a través de su cuenta en la
            Plataforma WILBBY, según el mecanismo descrito en los “Términos de
            Uso”.
          </p>
          <p>3.2.2. Envío de comunicaciones</p>
          <p>
            WILBBY utiliza los datos de los Usuarios para realizar
            comunicaciones vía correo electrónico y/o enviar SMS al Usuario
            acerca de la operativa del servicio.
          </p>
          <p>
            WILBBY podrá enviar mensajes al móvil del Usuario con información
            relativa al estado del pedido solicitado, y una vez éste haya
            terminado, WILBBY enviará un resumen/ recibo del pedido y del precio
            del mismo al correo electrónico del Usuario.
          </p>
          <p>
            3.2.3. Detectar e investigar fraudes y posibles comisiones de
            delitos
          </p>
          <p>
            WILBBY también utiliza la información para investigar y analizar
            cómo mejorar los servicios que presta a los Usuarios, así como para
            desarrollar y mejorar las características del servicio que ofrece.
            Internamente, WILBBY utiliza la información con fines estadísticos a
            efectos de analizar el comportamiento y las tendencias de los
            Usuarios, de comprender cómo los Usuarios utilizan la Plataforma de
            WILBBY, y de gestionar y mejorar los servicios ofrecidos, incluyendo
            la posibilidad de añadir nuevos distintivos a la Plataforma.
          </p>
          <p>
            WILBBY podrá controlar todas las actuaciones que podrían derivar en
            fraude o en la comisión de algún delito relacionado con los
            elementos de pago utilizados por los usuarios, WILBBY podrá
            solicitar al usuario una copia de su DNI, así como cierta
            información sobre la tarjeta bancaria utilizada para la solicitud
            del encargo. Todos los datos serán, en cualquier caso, tratados por
            WILBBY con la única finalidad de cumplimiento de las funciones de
            vigilancia y prevención del fraude y serán almacenados en tanto la
            relación con el usuario se mantenga vigente, así como incluso en el
            periodo posterior en tanto no prescriba el derecho del usuario a
            presentar reclamaciones o acciones legales relativas al pago de los
            productos o servicios solicitados a través de WILBBY. Los datos
            relativos a la tarjeta de crédito utilizada se conservarán mientras
            dure la resolución de la incidencia y durante los siguientes 120
            días desde su fecha de resolución. En el supuesto que se detecten
            irregularidades en el uso de la misma que pudieran ser consideradas
            como actividades ilegales, WILBBY se reserva el derecho a conservar
            los datos facilitados, así como podrá compartirlos con las
            autoridades competentes con la finalidad de investigar el supuesto.
            WILBBY podrá compartir los datos con las autoridades basado en la
            obligación legal de perseguir conductas contrarias a la normativa de
            aplicación.
          </p>
          <p>
            3.2.4. Garantizar la seguridad y un entorno adecuado para la segura
            prestación de servicios WILBBY podrá utilizar los datos a efectos de
            velar por el buen uso de los productos solicitados en su Plataforma
            (asegurar consejo farmacéutico, garantizar la entrega a mayores de
            18 años, ...).
          </p>
          <p>
            Cuando WILBBY intermedie en la recogida de productos Farmacéuticos,
            cuando el Usuario entra la Plataforma en el espacio Farmacia, WILBBY
            podrá facilitar los datos personales necesarios al farmacéutico con
            la finalidad que el farmacéutico responsable de la dispensación se
            pueda poner en contacto con el comprador si lo considerase oportuno
            y así remitirle la información correspondiente sobre el tratamiento
            que permita su correcto uso y realizar el envío, garantizando de
            esta forma el consejo Farmacéutico, sin que el Farmacéutico pueda
            utilizar los datos para otra finalidad que la de prestarle el
            consejo necesario para la prestación del servicio encomendado.
          </p>
          <p>
            3.2.5. Cumplir con la normativa, defensa e interposición de acciones
          </p>
          <p>
            WILBBY informa al usuario que las conversaciones que se lleven a
            cabo a través del chat para la comunicación con el Mandatario podrán
            ser revisadas y utilizadas por WILBBY para la interposición y/o
            defensa de reclamaciones y/o acciones legales necesarias, así como
            para la gestión de incidencias en los pedidos.
          </p>
          <p>
            3.2.6. Promoción y realización de ofertas comerciales (online y
            offline)
          </p>
          <p>
            WILBBY utiliza tecnología de terceros integrada en su Plataforma a
            los efectos de recabar sus datos y preferencias y utilizarlos con
            sistemas de CRM y tecnología avanzada en beneficio de los Usuarios.
            Así, mediante la información recabada se realizarán los siguientes
            tratamientos de sus datos:
          </p>
          <ul>
            <li>
              <p>
                WILBBY, podrá enviar por correo electrónico mensajes
                promocionales y/o ofertas relativas al servicio que ofrece y que
                pudieran interesar al Usuario. WILBBY podrá medir y personalizar
                dicha publicidad en función de las preferencias de los Usuarios
                de WILBBY. Si el Usuario de WILBBY desea no recibir la citada
                información y/o comunicaciones comerciales, podrá optar en
                cualquier momento a la opción de “Cancelar la suscripción” en el
                propio correo electrónico, y consecuentemente, WILBBY cesará
                inmediatamente en el envío de la citada información.
              </p>
            </li>
            <li>
              <p>
                WILBBY también podrá enviar al Usuario mensajes y/o ofertas
                relativas a dichos servicios mediante notificaciones “push”
                consistentes en el envío a su teléfono móvil de dichos mensajes
                promocionales y/o ofertas. Si el Usuario de WILBBY desea no
                recibir las comunicaciones comerciales del presente punto y el
                anterior 3.1., el Usuario podrá eliminarlas de forma conjunta
                desactivando sus preferencias de privacidad en su perfil con un
                solo click.
              </p>
            </li>
            <li>
              <p>
                WILBBY y/o los terceros asociados con WILBBY, podrá utilizar la
                dirección de entrega del pedido introducida por el Usuario a
                efectos de realizar actividades promocionales para la entrega de
                muestras o productos gratuitos del servicio relacionado con
                WILBBY (i.e. entrega de muestras gratuitas a domicilio o
                folletos publicitarios) junto a la entrega del pedido que puedan
                ser de interés del Usuario.
              </p>
            </li>
            <li>
              <p>
                Mediante el uso de la Plataforma de WILBBY, los Usuarios también
                podrán recibir comunicaciones comerciales de terceros asociados
                con la Plataforma como Facebook y Google, todo ello en función
                de las preferencias de privacidad que el Usuario tenga en dichas
                Plataformas.
              </p>
            </li>
          </ul>
          <p>
            Los Usuarios podrán hacer uso de su centro de gestión de privacidad
            a los efectos de dar de baja los servicios de “marketing” online o
            cancelar su cuenta que no estuviera de acuerdo en recibir muestras
            junto con los pedidos de WILBBY.
          </p>
          <p>3.2.7. Fines estadísticos y análisis de servicios</p>
          <p>
            WILBBY utiliza la información con fines estadísticos a efectos de
            analizar el comportamiento y las tendencias de los Usuarios, de
            comprender cómo los Usuarios utilizan la Plataforma de WILBBY, y de
            gestionar y mejorar los servicios ofrecidos, incluyendo la
            posibilidad de añadir nuevos distintivos a la Plataforma.
          </p>
          <p>
            WILBBY también utiliza la información para investigar y analizar
            cómo mejorar los servicios que presta a los Usuarios, así como para
            desarrollar y mejorar las características del servicio que ofrece.
          </p>
          <p>
            3.2.8. Garantizar la seguridad y un entorno adecuado para la segura
            prestación de servicios
          </p>
          <p>
            WILBBY podrá utilizar los datos a efectos de velar por el buen uso
            de los productos solicitados en su Plataforma (asegurar consejo
            farmacéutico, garantizar la entrega a mayores de 18 años, ...).
          </p>
          <p>3.2.9. Tramitar siniestros y reclamaciones con aseguradoras</p>
          <p>
            En el supuesto que el Usuario contacte a WILBBY para reportar algún
            daño o imprevisto que pueda quedar cubierto por el seguro contratado
            pro WILBBY, éste tratará todos los datos relativos al siniestro con
            la finalidad de gestionar y dar respuesta a las solicitudes.
          </p>
          <p>3.3 Base legal del tratamiento</p>
          <p>
            El tratamiento de los datos de los Usuarios se realiza en base a las
            siguientes bases legales:
          </p>
          <ul>
            <li>
              <p>
                Ejecutar la relación contractual una vez registrado en la
                Plataforma (por ejemplo, el tratamiento de tus datos para
                entregarte una orden solicitada).
              </p>
            </li>
            <li>
              <p>
                En base a nuestro interés legítimo (por ejemplo, para la
                realización de controles para evitar el fraude a través de la
                Plataforma).
              </p>
            </li>
            <li>
              <p>
                Cumplimiento de obligaciones legales (por ejemplo, cuando
                autoridades competentes soliciten datos en el marco de
                investigaciones judiciales y/o la interposición de las acciones
                necesarias para proteger los intereses de WILBBY.
              </p>
            </li>
            <li>
              <p>
                Consentimiento explícito para la comunicación de datos de
                usuarios a terceros con la finalidad de realizar comunicaciones
                comerciales.
              </p>
            </li>
          </ul>
          <p>3.4 Destinatarios de los datos</p>
          <p>
            WILBBY garantiza que todos los socios comerciales, técnicos,
            proveedores, o terceros independientes están unidos mediante
            promesas contractualmente vinculantes para procesar información que
            se comparte con ellos siguiendo las indicaciones que WILBBY le da,
            la presente Política de Privacidad, así como la legislación
            aplicable en materia de protección de datos. No cederemos tus datos
            personales a ningún tercero que no esté sometido a nuestras
            instrucciones y ninguna comunicación incluirá vender, alquilar,
            compartir o revelar de otro modo información personal de clientes
            con fines comerciales de modo contrario a los compromisos adquiridos
            en la presente Política de Privacidad.
          </p>
          <p>
            3.4.1. Durante la ejecución de un pedido los datos podrán
            compartirse con:
          </p>
          <ul>
            <li>
              <p>
                El Mandatario, quién ejecuta el mandato de recogida y entrega
                del producto.
              </p>
            </li>
            <li>
              <p>
                Por el establecimiento o local encargado de la venta del
                producto, en el supuesto que el Usuario hubiera solicitado la
                adquisición de un producto. En el supuesto que el Usuario
                contacte directamente con los proveedores descritos y facilite
                sus datos directamente a éstos, WILBBY no será responsable por
                el uso que los proveedores hagan.
              </p>
            </li>
            <li>
              <p>
                Por los servicios de Atención al Cliente contratados por WILBBY
                a los efectos de advertir al Usuario sobre posibles incidencias
                o solicitarle el porqué de una valoración negativa del servicio.
                WILBBY podrá utilizar los datos facilitados a los efectos de
                gestionar las incidencias que pudieran producirse en la
                prestación de servicios.
              </p>
            </li>
            <li>
              <p>
                La Plataforma y proveedores de pago a los efectos de que estos
                puedan cargar el importe en su cuenta.
              </p>
            </li>
            <li>
              <p>
                Por prestadores de servicios de telecomunicaciones, cuando se
                utilicen para el envío de comunicaciones sobre órdenes o
                incidencias relacionadas con éstas.
              </p>
            </li>
            <li>
              <p>
                Prestadores que ofrecen servicios de encuestas de satisfacción
                realizadas por WILBBY
              </p>
            </li>
          </ul>
          <p>3.4.2. Compartir los datos de los Usuarios con terceros</p>
          <p>
            Para poder seguir prestando los servicios ofrecidos a través de la
            Plataforma, WILBBY podrá compartir ciertos datos de carácter
            personal de los Usuario con:
          </p>
          <ul>
            <li>
              <p>
                Proveedores de servicios. los terceros proveedores de servicios
                de WILBBY que ejecutan pedidos, envían paquetes, ejecutan los
                pedidos y/o solucionan incidencias con los envíos tendrán acceso
                a la información personal de los Usuarios necesaria para
                realizar sus funciones, pero no podrán utilizarla para otros
                fines. Estos deberán tratar la información personal de
                conformidad con la presente Política de Privacidad y la
                legislación aplicable en la materia de protección de datos.
              </p>
            </li>
            <li>
              <p>
                Farmacias. WILBBY podrá facilitar el nombre y el teléfono de los
                Usuarios a aquellos farmacéuticos que dispensan productos a los
                Usuarios con el objetivo de asegurar el consejo farmacéutico de
                acuerdo con la legislación vigente en la materia.
              </p>
            </li>
            <li>
              <p>
                Proveedores de servicios de pago. Cuando un Usuario introduzca
                en la Plataforma de WILBBY su número de tarjeta, ésta se
                almacena directamente por las Plataformas de Pago contradas por
                WILBBY, quienes permitirán que el cobro de la cuenta del
                Usuario. Los proveedores de servicios de pago han sido elegidos
                en base a sus medidas de seguridad y cumpliendo, en todo caso,
                las medidas de seguridad establecidas por la normativa de
                servicios de pago, siendo PC1 Compliant según el Estándar de
                Seguridad de Datos para la Industria de Tarjeta de Pago o PCI
                DSS. WILBBY no almacena en ningún caso dichos datos.
              </p>
            </li>
            <li>
              <p>
                Prestadores de servicios para el control del fraude. WILBBY
                compartirá los datos de los Usuarios con proveedores que ofrecen
                servicios de control del fraude, para estudiar el riesgo de las
                operaciones que se llevan a cabo.
              </p>
            </li>
            <li>
              <p>
                Prestadores de servicio para la anonimización de algunos datos.
                Para evitar el uso indebido de los datos de los Usuarios por
                parte de terceros prestadores de servicios, WILBBY puede ceder
                los datos de los Usuarios a fin de que estos puedan ser
                anonimizados y utilizados únicamente para la prestación del
                servicio a los Usuarios. Por ejemplo, WILBBY podrá ceder a
                terceros el número de teléfono de los Usuarios para
                anonimizarlos y facilitarlos en este formato a los prestadores
                utilizados para dar cumplimiento a los servicios contratados por
                los Usuarios.
              </p>
            </li>
            <li>
              <p>
                Empresas de seguridad y Fuerzas y Cuerpos de Seguridad. WILBBY
                podrá divulgar información personal y datos de cuentas de
                nuestros clientes cuando crea que su divulgación es necesaria
                para el cumplimiento de la ley, para hacer cumplir o aplicar los
                “Términos de Uso” o para proteger los derechos, la propiedad o
                seguridad de WILBBY, sus usuarios o terceros. Lo anterior
                incluye, por lo tanto, el intercambio de información con otras
                sociedades y organizaciones, así como con Fuerzas y Cuerpos de
                Seguridad para la protección contra el fraude y la reducción del
                riesgo de crédito. Previo requerimiento legal, WILBBY podrá
                compartir información con organismos de autoridades ejecutivas
                y/o terceros con respecto a peticiones de información relativas
                a investigaciones de carácter penal y presuntas actividades
                ilegales.
              </p>
            </li>
            <li>
              <p>
                Servicios de Call Center y gestión de incidencias. Para ofrecer
                un Servicio de Atención al cliente y centros de atención
                telefónica, acciones dirigidas a la medición del grado de
                satisfacción de los Usuarios y prestación de servicios de apoyo
                administrativo, WILBBY podrá comunicar los datos de los Usuarios
                a compañías situadas en países fuera del EEE, siempre y cuando
                se le autorice y se cumplan los requisitos de seguridad
                mencionados en el punto anterior.
              </p>
            </li>
            <li>
              <p>
                Servicios de telecomunicaciones. Para poder ofrecer a los
                Usuarios servicios de contacto telefónico, WILBBY podrá
                contactar con empresas de telecomunicaciones que facilitan
                líneas y sistemas seguros para contactar con los Usuarios.
              </p>
            </li>
            <li>
              <p>
                Empresas del grupo WILBBY. Para poder prestar los servicios de
                WILBBY, según la zona geográfica desde la que los usuarios nos
                solicitan los servicios, se podrán transferir ciertos datos de
                carácter personal de los Usuarios a sus filiales. Se informa a
                los Usuarios que al darse de alta en la Plataforma desde
                cualquier país en el que opera WILBBY sus datos serán
                almacenados en la base de datos de WILBBY situada en Irlanda y
                titularidad de la sociedad española WILBBY. Los datos serán
                comunicados, en caso de filiales fuera del EEE, mediante los
                sistemas habilitados por la Comisión Europea y el Reglamento, a
                países que tengan reconocido un nivel adecuado de protección de
                datos personales o a través de contratos aprobados por la
                Comisión Europea por los que se establezcan y garanticen los
                derechos de los interesados.
              </p>
            </li>
            <li>
              <p>
                Redes sociales conectadas por el Usuario. En el supuesto que el
                Usuario conecte su cuenta de WILBBY a otra red social o a la
                plataforma de un tercero, WILBBY podría utilizar la información
                cedida a esa red social o tercero, siempre que dicha información
                se haya puesto a disposición de WILBBY cumpliendo con la
                política de privacidad de dicha red social o plataforma de
                tercero.
              </p>
            </li>
            <li>
              <p>
                Terceros asociados con WILBBY para comunicaciones comerciales.
                Con su consentimiento explícito, WILBBY podrá transferir sus
                datos personales a terceros asociados con WILBBY siempre y
                cuando el Usuario hubiera aceptado expresa, informada e
                inequívocamente dicha cesión de datos y conociera el propósito y
                destinatario de los mismos.
              </p>
            </li>
            <li>
              <p>
                Supuestos de cambio de propiedad. Si la propiedad de WILBBY
                cambiara o la mayor parte de sus activos fueran adquiridos por
                un tercero, los Usuarios son informados que WILBBY transferirá
                sus datos a las entidades del adquirente con la finalidad de
                seguir prestando los servicios objeto del tratamiento de datos.
                El nuevo responsable comunicará a los Usuarios sus datos de
                identificación. WILBBY manifiesta que cumplirá con el deber de
                información a la Autoridad de Control pertinente en caso de que
                las situaciones citadas ocurrieran, y comunicará a los Usuarios
                el cambio de responsable en el momento en que se produzca. El
                presente tratamiento se realizará en virtud del contrato
                suscrito con WILBBY.
              </p>
            </li>
            <li>
              <p>
                Compañías aseguradoras. WILBBY podrá comunicar los datos de los
                usuarios a las compañías aseguradoras y corredurías de seguros
                con las que WILBBY tengan un acuerdo para la gestión y
                tramitación de reclamaciones y siniestros derivados de la
                actividad de WILBBY y sus colaboradores.
              </p>
            </li>
          </ul>
          <p>
            Los datos de los Usuarios de WILBBY no se transmitirán a ningún
            tercero a excepción de que (i) sea necesario para los servicios
            solicitados, en caso de que WILBBY colabore con terceros (ii) cuando
            WILBBY tenga autorización expresa e inequívoca por parte del
            Usuario, (iii) cuando se solicite por parte de autoridad competente
            en el ejercicio de sus funciones (para investigar, prevenir o tomar
            acciones relativas a acciones ilegales) o (iv) por último, si lo
            requiere la ley.
          </p>
          <p>
            3.5. Tratamiento de candidatos que contactan a través de los
            formularios previstos en la sección WILBBY Jobs
          </p>
          <p>
            Las presentes disposiciones serán de aplicación para las personas
            que contacten con WILBBY a través de la página web de WILBBY a
            efectos de presentar su candidatura para una posición abierta
            (“Candidatos”).
          </p>
          <p>
            <strong>3.6.1. Responsable</strong>
          </p>
          <p>Responsable del tratamiento de sus datos</p>
          <p>
            <strong>
              <span>Identidad: </span>
            </strong>
            <span>Locatefit, S.L.- CIF: B09603440</span>
          </p>
          <p>
            <strong>
              <span>Domicilio: </span>
            </strong>
            <span>C/ Francisco Sarmiento 13 8A, 09005, Burgos</span>
          </p>
          <p>
            <strong>
              <span>Teléfono: </span>
            </strong>
            <span>(+34) 664 02 81 61</span>
          </p>
          <p>
            <strong>
              <span>Email: </span>
            </strong>
            <span>legal@wilbby.com</span>
          </p>
          <p>
            <strong>
              <span>CIF: </span>
            </strong>
            <span>B09603440</span>
          </p>
          <p>3.6.2. Finalidad</p>
          <p>
            Estudiar la candidatura, presente o futura, del Candidato para
            alguna/s de las posiciones abiertas en WILBBY.
          </p>
          <p>
            Asimismo, WILBBY tratará los datos del Candidato para realizar las
            entrevistas que considere necesarias para la posición, realizar test
            para evaluar el conocimiento del Candidato, contactar empresas dónde
            haya estado contratado previamente, comprobar referencias aportadas
            y, en general, evaluar las capacidades y habilidades del Candidato.
          </p>
          <p>3.6.3. Base legal para el tratamiento</p>
          <p>
            El Candidato, de forma previa a la presentación de la candidatura,
            prestará su consentimiento para el trato de datos, aceptando
            expresamente cuanto contenido en la presente Política de Privacidad.
            WILBBY podrá tratar sus datos, asimismo, a los efectos de estudiar
            la posibilidad de establecer una relación contractual y dado que el
            Candidato ha sido la persona en solicitar dicho estudio.
          </p>
          <p>3.6.4. Destinarios de los datos</p>
          <p>
            Los datos del Candidato podrán ser accedidos por proveedores de
            servicios tecnológicos y por plataformas contratadas pro WILBBY para
            la gestión de procesos de selección, como Greenhouse Software, Inc.,
            ubicado en los Estados Unidos de América y contratado por WILBBY
            para gestionar las tareas de selección y procesos de contratación.
            En consecuencia, si el Candidato se encuentra fuera de los Estados
            Unidos, sus datos personales serán transferidos a los Estados
            Unidos. Dado que la Comisión de la Unión Europea ha determinado que
            las leyes de privacidad de datos de los Estados Unidos no garantizan
            un nivel adecuado de protección para datos personales, la
            transferencia estará sujeta a garantías adicionales apropiadas según
            las cláusulas contractuales estándar y/o el conocido como Pivacy
            Shield. Al mismo tiempo, los datos del Candidato se almacenarán en
            Google, Inc y Servidores de Amazon Web Services, que actúan como
            encargados del tratamiento, los cuales cumplen con el Reglamento
            General de Protección de Datos y WILBBY dispone de un acuerdo por
            escrito con cada uno de ellos.
          </p>
          <p>
            Dependiendo de la oferta a la que el Candidato presente su
            candidatura, su información personal puede ser transferida a otras
            compañías del grupo WILBBY, a efectos de valorar la candidatura para
            el país correspondiente.
          </p>
          <p>3.6.5. Conservación</p>
          <p>
            Los datos del Candidato se conservarán mientras dure el proceso de
            selección y, en caso de no ser seleccionado, durante veinticuatro
            (24) meses a contar de la fecha de finalización del proceso,
            pudiendo ejercer en cualquier momento cualquiera de los derechos
            presentes en el apartado 4 de la presente política de privacidad.
          </p>
          <p>3.6. Transferencias internacionales de datos</p>
          <p>
            En la elección de los prestadores de servicios, WILBBY puede
            transferir datos de los usuarios fuera de las fronteras del Espacio
            Económico Europeo. En estos casos, y de forma previa al envío,
            WILBBY se encargará de que estos prestadores de servicios cumplan
            con los estándares mínimos de seguridad establecidos por la Comisión
            Europea y que éstos traten los datos siempre según las instrucciones
            prestadas por WILBBY. WILBBY podrá tener una relación contractual
            con ellos dónde los prestadores de servicios se comprometan a
            cumplir las instrucciones de WILBBY y a incorporar las medidas de
            seguridad necesarias para proteger los datos de los Usuarios.
          </p>
          <p>3.7. Plazo de conservación</p>
          <p>
            Los datos de los Usuarios serán conservados durante la ejecución y
            mantenimiento de la relación contractual, esto es, mientras sigan
            siendo Usuarios de WILBBY o hasta que ejercites tu derecho de
            limitación.
          </p>
          <p>
            En caso de darse de baja de la Plataforma, WILBBY conservará los
            datos de los Usuarios durante el período establecido en la normativa
            tributaria, de sanidad, penal y cualquier otra normativa que fuera
            de aplicación, para la ejecución y defensa de acciones en las que
            WILBBY pudiera ser parte. WILBBY bloqueará, en todo caso, los datos
            de los Usuarios para que solo puedan ser consultado en caso de deber
            interponer acción o defenderse ante las mismas.
          </p>
          <p>
            De forma específica, sin que pueda entenderse como limitativa de
            otra normativa que resultare de aplicación, los datos se
            conservarán, tras la baja del Usuario, según el cuadro presente en
            el Anexo II.
          </p>
          <p>
            En relación con información anónima, WILBBY aplicará cuanto descrito
            en el Considerando 26 del RGPD, que establece que “Por lo tanto los
            principios de protección de datos no deben aplicarse a la
            información anónima, es decir información que no guarda relación con
            una persona física identificada o identificable, ni a los datos
            convertidos en anónimos de forma que el interesado no sea
            identificable, o deje de serlo”. En consecuencia, el presente
            Reglamento no afecta al tratamiento de dicha información anónima,
            inclusive con fines estadísticos o de investigación.
          </p>
          <h1>4. Ejercicio de Derechos</h1>
          <p>
            El Usuario podrá ejercitar sus derechos en cualquier momento de
            forma gratuita a través del formulario presente en la Plataforma.
            También podrá ejercitar sus derechos escribiendo un email a la
            siguiente dirección de correo electrónico: legal@wilbby.com. En ese
            correo deberá especificarse qué derecho se desea ejercer, así como
            los datos identificativos y registrados en la Plataforma, de ser el
            caso. En el supuesto que, para verificar la identidad, necesitemos
            datos adicionales a los que nos facilites, comunicaremos al Usuario
            en tal sentido.
          </p>
          <p>Podrás ejercitar ante WILBBY los siguientes derechos:</p>
          <ul>
            <li>
              <p>
                Derecho de acceso a tus datos personales para saber cuáles están
                siendo objeto de tratamiento y las operaciones de tratamiento
                llevadas a cabo con ellos;
              </p>
            </li>
            <li>
              <p>
                Derecho de rectificación de cualquier dato personal inexacto;
              </p>
            </li>
            <li>
              <p>
                Derecho de supresión de tus datos personales, cuando esto sea
                posible;
              </p>
            </li>
            <li>
              <p>
                Derecho a solicitar la limitación del tratamiento de tus datos
                personales cuando la exactitud, la legalidad o la necesidad del
                tratamiento de los datos resulte dudosa, en cuyo caso, podremos
                conservar los datos conservarlos para el ejercicio o la defensa
                de reclamaciones.
              </p>
            </li>
            <li>
              <p>
                Derecho de oposición al tratamiento de tus datos para resolver
                cualquier cuestión que nos hayas planteado a través del
                formulario de contacto, así como al tratamiento de tus datos a
                través de una red social y/o para el tratamiento de tu
                currículum. Igualmente, podrás retirar tu consentimiento para la
                recepción de comunicaciones comerciales en cualquier momento, a
                través del perfil del Usuario en la Plataforma, mediante el link
                incluido al efecto en cada comunicación comercial o mediante el
                envío de correo electrónico.
              </p>
            </li>
          </ul>
          <p>
            Si crees que WILBBY ha cometido una infracción de la legislación de
            protección de datos personales, por favor, no dudes en ponerte en
            contacto con nosotros en la dirección de correo electrónico
            legal@wilbby.com, contándonos tus apreciaciones, para que podamos
            solucionar el problema a la mayor brevedad. De todos modos, también
            puedes ponerlo en conocimiento de la Agencia Española de Protección
            de Datos y reclamar ante este organismo la protección de tus
            derechos.
          </p>
          <h1>5. Medidas de Seguridad</h1>
          <p>
            WILBBY ha adoptado las medidas necesarias recomendadas por la
            Comisión Europea y por la autoridad competente para mantener el
            nivel de seguridad requerido, según la naturaleza de los datos
            personales tratados y las circunstancias del tratamiento, con el
            objeto de evitar, en la medida de lo posible y siempre según el
            estado de la técnica, su alteración, pérdida, tratamiento o acceso
            no autorizado. Como se ha mencionado anteriormente, los datos
            personales facilitados no serán cedidos a terceros sin autorización
            previa por parte del titular de los mismos.
          </p>
          <h1>6. Notificaciones y Modificaciones</h1>
          <p>
            Como se ha indicado anteriormente, todos los Usuarios tienen
            derechos a acceder, actualizar y cancelar sus datos, así como
            oponerse a su tratamiento. Puede ejercer estos derechos o realizar
            cualquier consulta en relación con la Política de Privacidad de
            WILBBY a través del Formulario de Contacto.
          </p>
          <p>
            A causa de la continua evolución de las actividades de WILBBY, la
            presente Política de Privacidad, la Política de Cookies y los
            Términos de Uso podrán igualmente modificarse. WILBBY enviará al
            Usuario los avisos sobre los cambios y modificaciones sustanciales
            de dichos documentos a través del correo electrónico, o de otro
            medio que asegure la recepción de los mismos. De todas formas,
            WILBBY en ningún caso modificará las políticas ni prácticas para
            hacerlas menos eficaces en la protección de los datos personales de
            nuestros clientes almacenados anteriormente.
          </p>
          <p>
            En caso de discrepancia entre las versiones traducidas y la versión
            en castellano de este texto, prevalecerá esta última.
          </p>
          <h2>ANEXO I – PLAZOS DE CONSERVACIÓN</h2>
          <p>
            Tratamiento Datos Conservación Origen del criterio Clientes Facturas
            15 años Código Penal, Normativa contable, Código de Comercio,
            Normativa IVA, LIS 10 años Ley Blanqueo de Capitales Formularios y
            cupones 15 años Recomendación Ribas y Asociados Contratos 5 años
            Prescripción Código Civil 10 años Ley Blanqueo de Capitales 15 años
            Código Penal Seguros Pólizas y declaraciones de salud para seguros
            de vida, seguros médicos y seguros de accidentes 6 años (regla
            general) 2 años (seguro de daños) 5 años (seguros personales) 10
            años (seguro de vida) Código de Comercio, Ley de Contrato de Seguro,
            Código Civil 10 años Ley Blanqueo de Capitales 15 años Código Penal
            Jurídico Reclamaciones judiciales 5 años Ley de Patentes, Ley de
            Marcas, Ley de Protección Jurídica del Diseño, Ley de propiedad
            Intelectual Contratos 5 años Código Civil 15 años Código Penal
            Morosos 6 años desde la fecha de expiración del permiso, licencia,
            certificado Código de Comercio 15 años Código Penal Acuerdos de
            confidencialidad Siempre o plazo de duración obligación
            confidencialidad Recomendación Ribas y Asociados Comunicaciones
            Comunicaciones electrónicas y a las redes públicas de comunicaciones
            12 meses des de la comunicación. Opción de reducir a un mínimo de 6
            meses y opción de ampliar hasta un máximo de 2 años. Ley 25/2007, de
            18 de octubre, de conservación de datos relativos a las
            comunicaciones electrónicas y a las redes públicas de
            comunicaciones. LOPD 2018 Marcado de datos 4 años Ley Órganica de
            Protección de Datos Marketing Bases de datos 1 año Visitantes web 1
            año
          </p>
          <a
            href="https://doc-wilbby.s3.eu-west-3.amazonaws.com/Politica-de-parivacidad-wilbby.pdf"
            download
          >
            Descargar en PDF
          </a>
        </div>
      </div>
    </div>
  );
}
