import React from "react";
import {
  StarFilled,
  HeartOutlined,
  HeartFilled,
  ClockCircleOutlined,
} from "@ant-design/icons";
import { withRouter } from "react-router-dom";
import { useMutation } from "react-apollo";
import {
  ANADIR_RESTAURANT_FAVORITE,
  ELIMINAR_RESTAURANT_FAVORITE,
} from "../../GraphQL/mutation";
import "./index.css";
import { message } from "antd";
import { scheduleTime } from "../../Utils/scheduledTime";
import { formaterPrice } from "../../Utils/formatePrice";

function CardRestaurant(props) {
  const { history, res, key, refetch, city } = props;
  const [eliminarFavorito] = useMutation(ELIMINAR_RESTAURANT_FAVORITE);
  const [crearFavorito] = useMutation(ANADIR_RESTAURANT_FAVORITE);

  const usuario = localStorage.getItem("id");
  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  let rating = { 1: 0, 2: 0, 3: 0, 4: 0, 5: 0 };
  res.Valoracion.forEach((start) => {
    if (start.value === 1) rating["1"] += 1;
    else if (start.value === 2) rating["2"] += 1;
    else if (start.value === 3) rating["3"] += 1;
    else if (start.value === 4) rating["4"] += 1;
    else if (start.value === 5) rating["5"] += 1;
  });

  const ar =
    (5 * rating["5"] +
      4 * rating["4"] +
      3 * rating["3"] +
      2 * rating["2"] +
      1 * rating["1"]) /
    res.Valoracion.length;
  let averageRating = 0;
  if (res.Valoracion.length) {
    averageRating = Number(ar.toFixed(1));
  }

  let consideracion = "";
  let color = "#90C33C";

  switch (averageRating) {
    case 0:
      consideracion = "Sin valoraciones";
      color = "#FFA500";
      break;

    case 1:
    case 1.1:
    case 1.2:
    case 1.3:
    case 1.4:
    case 1.5:
    case 1.5:
    case 1.6:
    case 1.7:
    case 1.8:
    case 1.9:
      consideracion = "Mala";
      color = "#F5365C";
      break;

    case 2:
    case 2.1:
    case 2.2:
    case 2.3:
    case 2.4:
    case 2.5:
    case 2.5:
    case 2.6:
    case 2.7:
    case 2.8:
    case 2.9:
      consideracion = "Regular";
      color = "#FFFF00";
      break;

    case 3:
    case 3.1:
    case 3.2:
    case 3.3:
    case 3.4:
    case 3.5:
    case 3.5:
    case 3.6:
    case 3.7:
    case 3.8:
    case 3.9:
      consideracion = "Buena";
      color = "#1a73e8";
      break;

    case 4:
    case 4.1:
    case 4.2:
    case 4.3:
    case 4.4:
    case 4.5:
    case 4.5:
    case 4.6:
    case 4.7:
    case 4.8:
    case 4.9:
      consideracion = "Excelente";
      color = "#95ca3e";
      break;

    case 5:
      consideracion = "Excelente";
      color = "#95ca3e";
      break;
  }

  const anadirFavorite = (restaurant, id) => {
    if (!usuario) {
      message.warning("Debes iniciar sesión para añadir a favorito");
      setTimeout(() => {
        history.push(`/login?id=${restaurant}`);
      }, 2000);
    } else {
      crearFavorito({ variables: { restaurantID: restaurant, usuarioId: id } })
        .then(() => {
          message.success("Tienda añadido a la lista de deseos");
          refetch();
        })
        .catch((err) => {
          message.error("Algo salió mal intentalo de nuevo por favor");
          console.log(err);
        });
    }
  };

  const eliminarFavorite = (restaurant) => {
    eliminarFavorito({ variables: { id: restaurant } })
      .then(() => {
        message.success("Tienda eliminado a la lista de deseos");
        refetch();
      })
      .catch((err) => {
        message.error("Algo salió mal intentalo de nuevo por favor");
        console.log(err);
      });
  };

  const isOK = () => {
    if (scheduleTime(res.schedule) && res.open && city) {
      return true;
    }
    return false;
  };

  return (
    <div className="card_Store" key={key}>
      <div className="favourites">
        {res.anadidoFavorito ? (
          <HeartFilled
            onClick={() => eliminarFavorite(res._id)}
            style={{ cursor: "pointer" }}
          />
        ) : (
          <HeartOutlined
            onClick={() => anadirFavorite(res._id, usuario)}
            style={{ cursor: "pointer" }}
          />
        )}
      </div>
      <img
        src={res.image}
        alt={res.title}
        className={isOK() ? "mainImg" : "mainImgInac"}
        onClick={() =>
          isOK()
            ? history.push(`/store/${city}/${res.slug}`)
            : message.warning(
                "Tienda cerrada te avisaremos cuando vuleva a abrir"
              )
        }
      />
      <div
        className="info_container"
        onClick={() =>
          isOK()
            ? history.push(`/store/${city}/${res.slug}`)
            : message.warning(
                "Tienda cerrada te avisaremos cuando vuleva a abrir"
              )
        }
      >
        <div className="shipping">
          Envío{" "}
          {Number(res.shipping) == 0
            ? "Gratis"
            : `${formaterPrice(res.shipping, language, currency)}`}
        </div>

        <h1>{res.title}</h1>
        <div style={{ display: "flex", color: color }}>
          <StarFilled
            style={{
              marginRight: 10,
              color: color,
              marginTop: 3,
            }}
          />{" "}
          {averageRating} {consideracion} ({res.Valoracion.length})
        </div>
        {!isOK() ? (
          <div style={{ paddingTop: 10 }}>
            <p>
              <ClockCircleOutlined /> Volvemos Pronto
            </p>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default withRouter(CardRestaurant);
