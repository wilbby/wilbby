import React, { useState } from "react";
import { Query } from "react-apollo";
import { TIPO_TIENDAS } from "../../GraphQL/query";
import "./index.css";

export default function Sliders(props) {
  const { onClick, tipo } = props;
  return (
    <div className="con-cat">
      <Query query={TIPO_TIENDAS}>
        {(response) => {
          if (response.loading) {
            return null;
          }

          if (response) {
            const cat =
              response && response.data && response.data.getTipoTienda
                ? response.data.getTipoTienda.data
                : [];

            const render = (d, i) => {
              switch (d.title) {
                case "Envío Express":
                  return null;

                case "Algo Especial":
                  return null;

                default:
                  return (
                    <p
                      onClick={() => onClick(d._id)}
                      key={i}
                      className={
                        tipo === d._id ? "cat_item-seleted" : "cat_item"
                      }
                    >
                      {d.title}
                    </p>
                  );
              }
            };
            return (
              <>
                {cat.map((c, i) => {
                  return render(c, i);
                })}
              </>
            );
          }
        }}
      </Query>
    </div>
  );
}
