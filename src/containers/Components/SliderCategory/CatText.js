import React, { useState } from "react";
import { Query } from "react-apollo";
import { CATEGORY } from "../../GraphQL/query";
import "./index.css";
import { useTranslation } from "react-i18next";

export default function Sliders(props) {
  const { t } = useTranslation("common", { useSuspense: false });
  const { onClick, category } = props;
  return (
    <div className="category__cont">
      <div className="category__cont_item">
        <Query query={CATEGORY}>
          {(response) => {
            if (response.loading) {
              return null;
            }

            if (response) {
              const cat =
                response && response.data && response.data.getCategory
                  ? response.data.getCategory.data
                  : [];

              const render = (d, i) => {
                switch (d.title) {
                  case "Envío Express":
                    return null;

                  case "Algo Especial":
                    return null;

                  default:
                    return (
                      <div
                        className={category === d._id ? "cats-active" : "cats"}
                        key={i}
                        onClick={() => onClick(d)}
                      >
                        <img src={d.image} className="imagenCat" />
                        <p>{t(`category:${d.title}`)}</p>
                      </div>
                    );
                }
              };
              return (
                <>
                  {cat.map((c, i) => {
                    return render(c, i);
                  })}
                </>
              );
            }
          }}
        </Query>
      </div>
    </div>
  );
}
