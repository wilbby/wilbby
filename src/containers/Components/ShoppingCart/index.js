import React, { useState } from "react";
import { Query, useMutation } from "react-apollo";
import { GET_MY_CART_RESTAURANT } from "../../GraphQL/query";
import { DELETE_CARD_ITEMS, ELIMINAR_TO_CARD } from "../../GraphQL/mutation";
import { Tag, Modal, Button, message } from "antd";
import Complemento from "../../Components/Menu/complementos";
import Opiniones from "../Menu/opiniones";
import { withRouter } from "react-router-dom";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Lottie from "react-lottie";
import "./index.css";
import { formaterPrice } from "../../Utils/formatePrice";

const NoData = require("../../Assets/Animate/emtypcard.json");

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: NoData,
  rendererSettings: {
    preserveAspectRatio: "xMidYMid slice",
  },
};

function ShoppingCart(props) {
  const {
    id,
    restaurant,
    restaurants,
    history,
    fromPedido,
    llevar,
    city,
  } = props;

  const [showModal, setShowModal] = useState(false);
  const [dataModal, setDataModal] = useState(null);
  const [eliminarCardItem] = useMutation(DELETE_CARD_ITEMS);
  const [eliminarCart] = useMutation(ELIMINAR_TO_CARD);

  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  const show = (datas) => {
    setShowModal(true);
    setDataModal(datas);
  };

  const hide = () => {
    setShowModal(false);
    setDataModal(null);
  };

  const deleteItemCards = (id, refetch) => {
    setShowModal(false);
    eliminarCardItem({ variables: { id: id } })
      .then(async () => {
        eliminarCart({ variables: { id: id } })
          .then((res) => refetch())
          .catch((err) => {
            console.log("err", err);
          });
        refetch();
        message.success("Articulo eliminado del carrito");
        if (fromPedido) {
          history.goBack();
        }
      })
      .catch((err) => {
        console.log("err", err);
        message.error("Algo va mal intentalo de nuevo por favor");
      });
  };

  return (
    <Query
      query={GET_MY_CART_RESTAURANT}
      variables={{ id: id, restaurant: restaurant }}
    >
      {(response) => {
        if (response.loading) {
          return (
            <div className="menu__container" style={{ marginRight: 30 }}>
              <SkeletonTheme color="#E1E9EE" highlightColor="#F2F8FC">
                <div>
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                  <Skeleton
                    height={100}
                    width="100%"
                    style={{ marginBottom: 20 }}
                  />
                </div>
              </SkeletonTheme>
            </div>
          );
        }
        if (response) {
          const respuesta =
            response && response.data && response.data.getMyItemCart
              ? response.data.getMyItemCart.list
              : [];

          response.refetch();

          var suma = 0;
          var extras = 0;
          respuesta.forEach(function (numero) {
            const numeros = Number(numero.plato.price) * numero.plato.cantd;
            extras += Number(numero.extra);
            suma += numeros;
          });
          const navegar = () => {
            if (restaurants.minime > suma) {
              message.warning(
                `El minímo que puedes comprar en esta tienda es ${formaterPrice(
                  suma,
                  language,
                  currency
                )}`
              );
            } else {
              history.push(
                `/my-order/${id}/${restaurant}/${suma}/${restaurants.shipping}/${extras}/${restaurants.categoryName}/${llevar}/${restaurants.autoshipping}/${restaurants.extras}/${city}`
              );
            }
          };

          return (
            <div className="menu__container">
              {!fromPedido ? <h1 style={{ marginBottom: 30 }}>Cesta</h1> : null}

              {respuesta.length === 0 ? (
                <div className="No_opinion">
                  <Lottie options={defaultOptions} height={200} width={200} />
                  <p style={{ marginTop: 20 }}>
                    Aún no has añadido articulo al carrito
                  </p>
                </div>
              ) : null}

              {respuesta.map((p, i) => {
                const total = Number.parseFloat(p.plato.price) * p.plato.cant;
                return (
                  <div
                    className="plato-card my_cart"
                    key={i}
                    onClick={() => show(p)}
                  >
                    <div>
                      <div style={{ display: "flex" }}>
                        <h4>
                          <span style={{ color: "#90C33C" }}>
                            {p.plato.cant}× {""}
                          </span>
                          {p.plato.title}
                        </h4>
                        {p.news ? (
                          <Tag
                            style={{ marginLeft: 15, height: 22 }}
                            color="lime"
                          >
                            Nuevo
                          </Tag>
                        ) : null}
                      </div>
                      {p &&
                        p.complementos.map((c, i) => {
                          return (
                            <p key={i}>
                              <span style={{ color: "#90C33C" }}>- </span>
                              {c.name}
                            </p>
                          );
                        })}
                      <div style={{ display: "flex" }}>
                        <p style={{ fontWeight: 600, color: "black" }}>
                          {formaterPrice(total, language, currency)}
                        </p>
                        {p.plato.oferta ? (
                          <p
                            style={{
                              fontWeight: 200,
                              color: "#95ca3e",
                              marginLeft: 15,
                            }}
                          >
                            Oferta
                          </p>
                        ) : null}
                        {p.plato.popular ? (
                          <p
                            style={{
                              fontWeight: 200,
                              color: "#FFA500",
                              marginLeft: 15,
                            }}
                          >
                            Popular
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <img src={p.plato.imagen} alt={p.plato.title} />
                  </div>
                );
              })}

              {dataModal ? (
                <Modal
                  title={dataModal.plato.title}
                  visible={showModal}
                  footer={false}
                  onCancel={hide}
                >
                  <div className="modal_contents">
                    <img
                      src={dataModal.plato.imagen}
                      alt={dataModal.plato.title}
                    />
                    <h2>{dataModal.plato.title}</h2>
                    <p>{dataModal.plato.ingredientes}</p>
                    <div style={{ display: "flex" }}>
                      <p style={{ paddingTop: 5, color: "black" }}>
                        {formaterPrice(
                          dataModal.plato.price,
                          language,
                          currency
                        )}
                      </p>
                      {dataModal.plato.oferta ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#95ca3e",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Oferta
                        </p>
                      ) : null}
                      {dataModal.plato.popular ? (
                        <p
                          style={{
                            fontWeight: 200,
                            color: "#FFA500",
                            marginLeft: 15,
                            paddingTop: 5,
                          }}
                        >
                          Popular
                        </p>
                      ) : null}
                    </div>

                    <div>
                      <Complemento
                        id={dataModal.plato._id}
                        acompanante={dataModal.complementos}
                        refetch={response.refetch}
                        hide={hide}
                        fromPedido={fromPedido}
                        precio={dataModal.plato.price}
                        cantidad={dataModal.plato.cant}
                        plato={dataModal.plato}
                        restaurant={restaurant}
                      />
                    </div>
                    <Button
                      danger
                      shape="round"
                      onClick={() =>
                        deleteItemCards(dataModal.plato._id, response.refetch)
                      }
                      type="dashed"
                      style={{ marginTop: 30, width: "100%", height: 60 }}
                    >
                      Eliminar articulo
                    </Button>
                    <div style={{ marginTop: 40 }}>
                      <Opiniones
                        id={dataModal.plato._id}
                        plato={dataModal.plato.title}
                      />
                    </div>
                  </div>
                </Modal>
              ) : null}

              {!fromPedido ? (
                <>
                  {respuesta.length > 0 ? (
                    <>
                      <div className="btn-cont">
                        <Button
                          shape="round"
                          onClick={() => navegar()}
                          type="primary"
                          style={{
                            marginTop: 30,
                            width: "100%",
                            height: 60,
                            borderRadius: 10,
                          }}
                        >
                          {`Ver cesta total ${formaterPrice(
                            suma,
                            language,
                            currency
                          )}`}
                        </Button>
                      </div>
                    </>
                  ) : null}
                </>
              ) : null}
            </div>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(ShoppingCart);
