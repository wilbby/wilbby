import React, { useState } from "react";
import { GET_ACOMPANANTE, GET_MY_CART_RESTAURANT } from "../../GraphQL/query";
import { CREATE_CARD_ITEMS, ADD_TO_CARD } from "../../GraphQL/mutation";
import { Query, useMutation } from "react-apollo";
import {
  CheckCircleFilled,
  PlusCircleFilled,
  MinusCircleFilled,
  DeleteOutlined,
} from "@ant-design/icons";
import { message, Button } from "antd";
import { withRouter } from "react-router-dom";
import { formaterPrice } from "../../Utils/formatePrice";
import "./complementos.css";

function Complementos(props) {
  const {
    id,
    acompanante,
    cantidad,
    refetch,
    hide,
    precio,
    plato,
    history,
    fromPedido,
  } = props;
  const [comple, setComple] = useState(acompanante ? acompanante : []);
  const [count, setCount] = useState(cantidad ? cantidad : 1);
  const [acomp, setAcomp] = useState(0);
  const [extra, setExtra] = useState(0);
  const [createItemCard] = useMutation(CREATE_CARD_ITEMS);
  const [crearCart] = useMutation(ADD_TO_CARD);

  const user = localStorage.getItem("id");
  const token = localStorage.getItem("token");
  const language = localStorage.getItem("language");
  const currency = localStorage.getItem("currency");

  const total = Number.parseFloat(precio) * count;

  const totalFinaly = total + Number(extra);

  const rescantidad = (id) => {
    if (count < 2) {
      message.warning("El máximo que puede seleccionar es 1");
    } else {
      setCount(count - 1);
    }
  };

  const sumcantidad = () => {
    if (count > 999) {
      message.warning("El máximo que puede seleccionar es 999");
    } else {
      setCount(count + 1);
    }
  };

  function removeItemFromArr(arr, selectedItems, da, item) {
    var i = arr.findIndex((x) => x.mid == da.id);
    if (i !== -1) {
      arr.splice(i, 1);
      setComple(comple.concat());
    }
  }

  const onSelectedItemsChange = (da, item, selectedItems, price) => {
    if (comple && comple.filter((e) => e.mid == da.id).length > 0) {
      removeItemFromArr(comple, selectedItems, da, item);
      setExtra(extra - extra + price);
      setComple(comple.concat([Object.assign(item, { mid: da.id })]));
    } else {
      setComple(comple.concat([Object.assign(item, { mid: da.id })]));
      setExtra(extra + price);
    }
  };

  let complemets = [];

  comple &&
    comple.forEach(
      (element) =>
        (complemets = complemets.concat({
          cant: element.cant,
          id: element.id,
          mid: element.mid,
          name: element.name,
          price: element.price,
          plu: element.plu,
        }))
    );

  const createItemCards = (userId, platoID, datas) => {
    if (!token) {
      message.warning("Debes iniciar sesión para continuar");
      history.push(`/login?id=${datas.restaurant}`);
      return null;
    } else {
      const plato = {
        _id: datas._id ? datas._id : datas.id,
        title: datas.title,
        ingredientes: datas.ingredientes,
        price: datas.price,
        imagen: datas.imagen,
        restaurant: datas.restaurant,
        menu: datas.menu,
        oferta: datas.oferta,
        popular: datas.popular,
        cant: count,
        anadidoCart: true,
        plu: datas.plu,
      };
      const cardInput = {
        userId: userId,
        restaurant: datas.restaurant,
        plato: plato,
        platoID: platoID,
        total: `${totalFinaly}`,
        extra: `${extra}`,
        complementos: complemets,
      };
      createItemCard({ variables: { input: cardInput } })
        .then(async () => {
          crearCart({
            variables: {
              PlatoId: platoID,
              usuarioId: userId,
              cantd: Number(count),
            },
          })
            .then(() => {
              refetch();
              message.success("Articulo añadido al carrito de la compra");
              hide();
              if (fromPedido) {
                history.goBack();
              } else {
                window.location.reload();
              }
            })
            .catch((e) => {
              console.log(e);
              message.warning("Vuelve a intentarlo por favor");
              refetch();
            });
        })
        .catch((err) => {
          console.log("err", err);
          message.warning("Algo va mal intentalo de nuevo por favor");
          refetch();
        });
    }
  };

  return (
    <Query query={GET_ACOMPANANTE} variables={{ id: id }}>
      {(response) => {
        if (response) {
          const resp =
            response && response.data && response.data.getAcompanante
              ? response.data.getAcompanante.list
              : [];
          setAcomp(resp.length);
          response.refetch();
          const noAcompanante = resp.length === 0;
          const done = resp.filter((require) => require.required === true);

          const verifyAdd = () => {
            if (noAcompanante) {
              return true;
            } else if (comple && comple.length >= done.length) {
              return true;
            } else {
              return false;
            }
          };

          return (
            <div className="container-complementos">
              {resp.map((a, i) => {
                return (
                  <div className="complement" key={i}>
                    <div className="complement__title">
                      <h3>{a.name}</h3>
                      {a.required ? <span>Requerido</span> : null}
                    </div>
                    <div className="childerm">
                      {a.children.map((c, i) => {
                        return (
                          <div
                            key={i}
                            className="list-chill"
                            onClick={() =>
                              onSelectedItemsChange(
                                a,
                                c,
                                c.name,
                                Number(c.price)
                              )
                            }
                          >
                            <div>
                              <p>
                                {c.name}{" "}
                                {c.price
                                  ? `+ (${formaterPrice(
                                      c.price,
                                      language,
                                      currency
                                    )})`
                                  : null}
                              </p>
                              {c.recomended ? (
                                <span
                                  style={{
                                    color: "#90C33C",
                                    fontSize: 10,
                                    lineHeight: 0,
                                  }}
                                >
                                  Recomendado
                                </span>
                              ) : null}
                            </div>

                            {comple.map((s, i) =>
                              c.id === s.id ? (
                                <>
                                  {c.price ? (
                                    <DeleteOutlined
                                      key={i}
                                      onClick={() => {
                                        removeItemFromArr(comple, c.name, a, c);
                                        setExtra(extra - Number(c.price));
                                        message.success(
                                          "Eliminado de la lista"
                                        );
                                      }}
                                      style={{
                                        marginLeft: "auto",
                                        color: "red",
                                        fontSize: 18,
                                        cursor: "pointer",
                                      }}
                                    />
                                  ) : (
                                    <CheckCircleFilled
                                      key={i}
                                      style={{
                                        marginLeft: "auto",
                                        color: "#90C33C",
                                        fontSize: 18,
                                      }}
                                    />
                                  )}
                                </>
                              ) : null
                            )}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
              <div className="sum-cantidad">
                <MinusCircleFilled
                  style={{ fontSize: 26, marginRight: 20, color: "#90C33C" }}
                  onClick={() => rescantidad()}
                />
                {count}
                <PlusCircleFilled
                  style={{ fontSize: 26, marginLeft: 20, color: "#90C33C" }}
                  onClick={() => sumcantidad()}
                />
              </div>
              <Button
                shape="round"
                type="primary"
                disabled={verifyAdd() ? false : true}
                style={{ width: "100%", height: 60 }}
                onClick={() => createItemCards(user, id, plato)}
              >
                <p
                  style={{
                    paddingTop: 16,
                    fontWeight: 700,
                    fontSize: 16,
                    color: "white",
                  }}
                >
                  Total {formaterPrice(totalFinaly, language, currency)}
                </p>
              </Button>
            </div>
          );
        }
      }}
    </Query>
  );
}

export default withRouter(Complementos);
