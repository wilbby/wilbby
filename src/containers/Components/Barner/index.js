import React from "react";
import Header from "../Header";
import GetApp from "../GetApp";

import "./index.css";

export default function Barners(props) {
  const { title, subtitle, btntitle, href, btn } = props;
  return (
    <>
      <GetApp />
      <Header fromHome={true} />
      <div className="containers_barnes">
        <div className="containers_barnes_children">
          <div>
            <h1>{title}</h1>
            <p>{subtitle}</p>
            {btn ? (
              <a className="btn-help" href={href}>
                {btntitle}
              </a>
            ) : null}
          </div>
        </div>
      </div>
    </>
  );
}
