import React from "react";
import "./Headers.css";
import $ from "jquery";
import { USER_DETAIL } from "../../GraphQL/query";
import { Query } from "react-apollo";
import { IMAGES_PATH } from "../../Utils/UrlConfig";
import { Link } from "react-router-dom";
import { EnvironmentOutlined } from "@ant-design/icons";

const Headers = (props) => {
  let barra = localStorage.getItem("id") ? (
    <UsuarioAutenticado fromHome={props.fromHome} />
  ) : (
    <UsuarioNoAutenticado fromHome={props.fromHome} />
  );
  return <div>{barra}</div>;
};

$(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 30) {
      $(".navTranp").addClass("shadowTransp");
      $(".logo").addClass("logo-white");
    } else {
      $(".navTranp").removeClass("shadowTransp");
      $(".logo").removeClass("logo-white");
    }
  });
});

let id = localStorage.getItem("id");
let city = localStorage.getItem("city");

const UsuarioAutenticado = (props) => {
  return (
    <Query query={USER_DETAIL} variables={{ id }}>
      {({ loading, error, data }) => {
        if (error) {
          console.log("Response Error-------", error);
          return null;
        }
        if (loading) {
          return null;
        }
        if (data) {
          const userDatas = data.getUsuario.data;
          return (
            <div className={props.fromHome ? "navTranp" : "nav navline"}>
              <div className="ContainerNav">
                <div>
                  <Link to="/">
                    <div className={props.fromHome ? "logo" : "logoNornal"} />
                  </Link>
                </div>
                <div className="info_Content_header">
                  <div className="locations">
                    <div className="locations_active">
                      <EnvironmentOutlined style={{ marginRight: 2 }} />
                      <p>{city}</p>
                    </div>
                    <div>
                      <p>Ahora</p>
                    </div>
                  </div>
                  <Link to="/profile">
                    <img
                      src={IMAGES_PATH + userDatas.avatar}
                      alt={userDatas.name}
                      className="avatar_header"
                    />
                  </Link>
                </div>
              </div>
            </div>
          );
        }
      }}
    </Query>
  );
};

const UsuarioNoAutenticado = (props) => {
  return (
    <div className={props.fromHome ? "navTranp" : "nav navline"}>
      <div className="ContainerNav">
        <div>
          <Link to="/">
            <div className={props.fromHome ? "logo" : "logoNornal"} />
          </Link>
        </div>
        <div className="items">
          <Link to="/register" className="btn-custom-secundary">
            Regístrarme
          </Link>
          <Link to="/login" className="btn-custom-ptimary">
            Iniciar sesión
          </Link>
        </div>

        <div className="RespMenu">
          <Link to="/login" className="btn-custom-ptimary">
            Iniciar sesión
          </Link>
        </div>
      </div>
    </div>
  );
};
export default Headers;
