import gql from "graphql-tag";

export const AUTENTICAR_USUARIO = gql`
  mutation autenticarUsuario(
    $email: String!
    $password: String!
    $input: deviceInfo
  ) {
    autenticarUsuario(email: $email, password: $password, input: $input) {
      success
      message
      data {
        token
        id
        verifyPhone
      }
    }
  }
`;

export const NUEVO_USUARIO = gql`
  mutation crearUsuario($input: UsuarioInput) {
    crearUsuario(input: $input) {
      success
      message
      data {
        _id
        name
        lastName
        email
        city
        verifyPhone
      }
    }
  }
`;

export const ACTUALIZAR_USUARIO = gql`
  mutation actualizarUsuario($input: ActualizarUsuarioInput) {
    actualizarUsuario(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation singleUpload($imgBlob: Upload) {
    singleUpload(file: $imgBlob) {
      filename
    }
  }
`;

export const ELIMINAR_USUARIO = gql`
  mutation eliminarUsuario($id: ID!) {
    eliminarUsuario(id: $id) {
      success
      messages
    }
  }
`;

export const ANADIR_RESTAURANT_FAVORITE = gql`
  mutation crearFavorito($restaurantID: ID, $usuarioId: ID) {
    crearFavorito(restaurantID: $restaurantID, usuarioId: $usuarioId) {
      success
      messages
    }
  }
`;

export const ELIMINAR_RESTAURANT_FAVORITE = gql`
  mutation eliminarFavorito($id: ID) {
    eliminarFavorito(id: $id) {
      success
      messages
    }
  }
`;

export const CREATE_CARD_ITEMS = gql`
  mutation createItemCard($input: CreateCardInput) {
    createItemCard(input: $input) {
      success
      messages
    }
  }
`;

export const ACTUALIZAR_CARD = gql`
  mutation actualizarCardItem($input: AtualizarCardInput) {
    actualizarCardItem(input: $input) {
      id
      userId
      restaurant
      plato
      complementos {
        cant
        id
        mid
        name
        price
      }
    }
  }
`;

export const DELETE_CARD_ITEMS = gql`
  mutation eliminarCardItem($id: ID) {
    eliminarCardItem(id: $id) {
      success
      messages
    }
  }
`;

export const CREAR_MODIFICAR_ORDEN = gql`
  mutation crearModificarOrden($input: OrdenCreationInput) {
    crearModificarOrden(input: $input) {
      id
      display_id
      cupon
      nota
      aceptaTerminos
      restaurant
      time
      cantidad
      cubiertos
      userID
      propina
      descuento {
        clave
        descuento
        tipo
      }
      platos {
        userId
        platoID
        restaurant
        complementos {
          cant
          id
          mid
          name
          price
        }
        plato {
          _id
          title
          ingredientes
          price
          imagen
          restaurant
          menu
          oferta
          popular
          anadidoCart
          cant
        }
      }
      restaurants {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        slug
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }

      estado
      status
      isvalored
      progreso
      created_at
      total
    }
  }
`;

export const CREAR_VALORACIONES = gql`
  mutation crearValoracion($input: ValoracionCreationInput) {
    crearValoracion(input: $input) {
      id
      comment
      value
      restaurant
      user
    }
  }
`;

export const CREATE_NOTIFICATION = gql`
  mutation createNotification($input: NotificationInput) {
    createNotification(input: $input) {
      messages
      success
    }
  }
`;

export const READ_NOTIFICATION = gql`
  mutation readNotification($notificationId: ID) {
    readNotification(notificationId: $notificationId) {
      messages
      success
    }
  }
`;

export const CREATE_OPINIO = gql`
  mutation createOpinion($input: OpinionInput) {
    createOpinion(input: $input) {
      messages
      success
    }
  }
`;

export const CREATE_ADRESS = gql`
  mutation createAdress($input: adreesUser) {
    createAdress(input: $input) {
      messages
      success
      data {
        id
        formatted_address
        puertaPiso
        type
        usuario
        lat
        lgn
      }
    }
  }
`;

export const EDIT_ADRESS = gql`
  mutation actualizarAdress($input: adreesUserEdit) {
    actualizarAdress(input: $input) {
      messages
      success
    }
  }
`;

export const DELETE_ADRESS = gql`
  mutation eliminarAdress($id: ID) {
    eliminarAdress(id: $id) {
      messages
      success
    }
  }
`;

export const ORDER_PROCEED = gql`
  mutation OrdenProceed(
    $orden: ID!
    $estado: String!
    $progreso: String!
    $status: String!
  ) {
    OrdenProceed(
      orden: $orden
      estado: $estado
      progreso: $progreso
      status: $status
    ) {
      messages
      success
    }
  }
`;

export const ADD_TO_CARD = gql`
  mutation crearCart($PlatoId: ID, $usuarioId: ID, $cantd: Int) {
    crearCart(PlatoId: $PlatoId, usuarioId: $usuarioId, cantd: $cantd) {
      success
      messages
    }
  }
`;

export const ELIMINAR_TO_CARD = gql`
  mutation eliminarCart($id: ID) {
    eliminarCart(id: $id) {
      success
      messages
    }
  }
`;

export const CREATE_CUSTOM_ORDER = gql`
  mutation createCustonOrder($input: CustonOrderInput) {
    createCustonOrder(input: $input) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

export const CUSTOM_ORDER_PROCESS = gql`
  mutation actualizarOrderProcess($input: CustonOrderInput) {
    actualizarOrderProcess(input: $input) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

export const ACTUALIZAR_RIDER = gql`
  mutation actualizarRiders($input: ActualizarUsuarioInput) {
    actualizarRiders(input: $input) {
      _id
      name
      lastName
      city
      email
      avatar
      rating
    }
  }
`;
