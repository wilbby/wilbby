import gql from "graphql-tag";

export const USER_DETAIL = gql`
  query getUsuario {
    getUsuario {
      messages
      success
      data {
        _id
        name
        lastName
        email
        avatar
        city
        telefono
        created_at
        updated_at
        termAndConditions
        verifyPhone
        StripeID
        OnesignalID
      }
    }
  }
`;

export const USUARIO_ACTUAL = gql`
  query obtenerUsuario {
    obtenerUsuario {
      _id
      name
      lastName
      email
      avatar
      telefono
      city
      created_at
      updated_at
      termAndConditions
      verifyPhone
      StripeID
      OnesignalID
    }
  }
`;

export const CATEGORY = gql`
  query getCategory {
    getCategory {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

export const TIPO = gql`
  query getTipo {
    getTipo {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

export const TIPO_TIENDAS = gql`
  query getTipoTienda {
    getTipoTienda {
      messages
      success
      data {
        _id
        title
        image
        description
      }
    }
  }
`;

export const RESTAURANT = gql`
  query getRestaurant($city: String) {
    getRestaurant(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        slug
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const RESTAURANT_ID = gql`
  query getRestaurantForIDWeb($id: ID, $city: String) {
    getRestaurantForIDWeb(id: $id, city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        slug
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const RESTAURANT_SLUG = gql`
  query getRestaurantForSlugWeb($slug: String) {
    getRestaurantForSlugWeb(slug: $slug) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        slug
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const RESTAURANT_SLUG_FOR_DEATILS = gql`
  query getRestaurantForDetails($slug: String, $city: String) {
    getRestaurantForDetails(slug: $slug, city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        slug
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const RESTAURANT_FAVORITO = gql`
  query getRestaurantFavorito($id: ID) {
    getRestaurantFavorito(id: $id) {
      message
      success
      list {
        _id
        restaurantID
        usuarioId
        restaurant {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          slug
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
          stimateTime
          ispartners
          highkitchen
          schedule {
            Monday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Tuesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Wednesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Thursday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Friday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Saturday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Sunday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }
          }
        }
      }
    }
  }
`;

export const RESTAURANT_FOR_CATEGORY = gql`
  query getRestaurantForCategory(
    $city: String
    $category: String
    $tipo: String
    $llevar: Boolean
  ) {
    getRestaurantForCategory(
      city: $city
      category: $category
      tipo: $tipo
      llevar: $llevar
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        isnew
        phone
        email
        logo
        slug
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const GET_MENU = gql`
  query getMenu($id: ID) {
    getMenu(id: $id) {
      message
      success
      list {
        _id
        title
        subtitle
        restaurant
      }
    }
  }
`;

export const GET_MENU_MARCA = gql`
  query getMenumarca($id: ID) {
    getMenumarca(id: $id) {
      message
      success
      list {
        _id
        title
        subtitle
        restaurant
      }
    }
  }
`;

export const GET_PLATO = gql`
  query getPlato($id: ID, $page: Int, $limit: Int) {
    getPlato(id: $id, page: $page, limit: $limit) {
      message
      success
      list {
        _id
        title
        ingredientes
        price
        previous_price
        imagen
        restaurant
        menu
        out_of_stock
        oferta
        popular
        anadidoCart
        cant
        cantd
        news
        requireOption
        plu
        deliverectID
        opiniones {
          id
          plato
          comment
          rating
          created_at
          user
        }
      }
    }
  }
`;

export const GET_ACOMPANANTE = gql`
  query getAcompanante($id: ID) {
    getAcompanante(id: $id) {
      message
      success
      list {
        id
        name
        plato
        restaurant
        required
        children {
          id
          name
          price
          recomended
          cant
          plu
        }
      }
    }
  }
`;

export const GET_CART = gql`
  query getItemCarts($id: ID) {
    getItemCarts(id: $id) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        plato {
          _id
          title
          ingredientes
          price
          previous_price
          imagen
          restaurant
          anadidoCart
          cant
          cantd
          menu
          oferta
          popular
          cant
          requireOption
          plu
          deliverectID
        }
        Restaurant {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
          stimateTime
          ispartners
          highkitchen
          schedule {
            Monday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Tuesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Wednesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Thursday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Friday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Saturday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Sunday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }
          }
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_CART_RESTAURANT = gql`
  query getItemCart($id: ID, $PlatoID: ID) {
    getItemCart(id: $id, PlatoID: $PlatoID) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        complementos {
          cant
          id
          mid
          name
          price
        }
        plato {
          _id
          title
          ingredientes
          price
          previous_price
          imagen
          restaurant
          menu
          anadidoCart
          cant
          cantd
          oferta
          popular
          cant
          requireOption
          plu
          deliverectID
        }
        Restaurant {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_MY_CART_RESTAURANT = gql`
  query getMyItemCart($id: ID, $restaurant: ID) {
    getMyItemCart(id: $id, restaurant: $restaurant) {
      message
      success
      list {
        id
        userId
        restaurant
        platoID
        total
        extra
        complementos {
          cant
          id
          mid
          name
          price
        }
        plato {
          _id
          title
          ingredientes
          price
          previous_price
          imagen
          restaurant
          menu
          anadidoCart
          cant
          cantd
          oferta
          popular
          cant
          requireOption
          plu
          deliverectID
        }
        Restaurant {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
        }
        UserId {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_CUPON = gql`
  {
    getCuponAll {
      id
      clave
      tipo
      descuento
    }
  }
`;

export const GET_ADRESS_STORE = gql`
  query getAdressStore($id: ID, $city: String) {
    getAdressStore(id: $id, city: $city) {
      success
      message
      data {
        _id
        calle
        numero
        codigoPostal
        ciudad
        store
        lat
        lgn
      }
    }
  }
`;

export const GET_MY_CART_RESTAURANT_FOR_ORDER = gql`
  query getMyItemCartForOrder($id: ID, $restaurant: ID) {
    getMyItemCartForOrder(id: $id, restaurant: $restaurant) {
      message
      success
      list {
        id
        userId
        platoID
        restaurant
        extra
        total
        complementos {
          cant
          id
          mid
          name
          price
        }
        plato {
          _id
          title
          ingredientes
          price
          previous_price
          imagen
          restaurant
          menu
          oferta
          popular
          cant
          cantd
          requireOption
          plu
          deliverectID
        }
      }
    }
  }
`;

export const GET_ORDEN = gql`
  query getOrderByUsuario($usuarios: ID!, $dateRange: DateRangeInput) {
    getOrderByUsuario(usuarios: $usuarios, dateRange: $dateRange) {
      message
      success
      list {
        id
        display_id
        invoiceUrl
        nota
        isvalored
        aceptaTerminos
        restaurant
        cubiertos
        AdresStore {
          _id
          calle
          numero
          codigoPostal
          ciudad
          store
          lat
          lgn
        }
        Adress {
          id
          formatted_address
          type
          usuario
          lat
          lgn
        }
        Riders {
          _id
          name
          lastName
          avatar
          telefono
          email
          OnesignalID
          rating
        }
        restaurants {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
          stimateTime
          ispartners
          highkitchen
          schedule {
            Monday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Tuesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Wednesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Thursday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Friday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Saturday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Sunday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }
          }
        }
        time
        schedule
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos {
            cant
            id
            mid
            name
            price
          }
          plato {
            _id
            title
            ingredientes
            price
            previous_price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
            cantd
            requireOption
            plu
            deliverectID
          }
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        estado
        status
        progreso
        created_at
        total
        llevar
        adresStore
        city
        subtotal
        complement
        descuentopromo
        tarifa
        envio
        propinaTotal
      }
    }
  }
`;

export const GET_ALL_ORDEN = gql`
  query getOrderByUsuarioAll($usuarios: ID!, $dateRange: DateRangeInput) {
    getOrderByUsuarioAll(usuarios: $usuarios, dateRange: $dateRange) {
      message
      success
      list {
        id
        display_id
        invoiceUrl
        nota
        isvalored
        aceptaTerminos
        restaurant
        cubiertos
        AdresStore {
          _id
          calle
          numero
          codigoPostal
          ciudad
          store
          lat
          lgn
        }
        Adress {
          id
          formatted_address
          type
          usuario
          lat
          lgn
        }
        Riders {
          _id
          name
          lastName
          avatar
          telefono
          email
          OnesignalID
          rating
        }
        restaurants {
          _id
          title
          image
          llevar
          description
          rating
          anadidoFavorito
          inOffert
          type
          Valoracion {
            id
            comment
            value
            created_at
          }
          adress {
            calle
            numero
            codigoPostal
            ciudad
            lat
            lgn
          }
          collections
          isDeliverectPartner
          categoryID
          autoshipping
          alegeno_url
          categoryName
          minime
          shipping
          extras
          menu
          tipo
          isnew
          phone
          email
          logo
          apertura
          cierre
          aperturaMin
          cierreMin
          diaslaborales
          open
          OnesignalID
          stimateTime
          ispartners
          highkitchen
          schedule {
            Monday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Tuesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Wednesday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Thursday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Friday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Saturday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }

            Sunday {
              day
              isOpen
              openHour1
              openHour2
              openMinute1
              openMinute2
              closetHour1
              closetHour2
              closetMinute1
              closetMinute2
            }
          }
        }
        time
        schedule
        cantidad
        userID
        propina
        platos {
          userId
          platoID
          restaurant
          complementos {
            cant
            id
            mid
            name
            price
          }
          plato {
            _id
            title
            ingredientes
            price
            previous_price
            imagen
            restaurant
            menu
            oferta
            popular
            anadidoCart
            cant
            cantd
            requireOption
            plu
            deliverectID
          }
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        estado
        status
        progreso
        created_at
        total
        llevar
        adresStore
        city
        subtotal
        complement
        descuentopromo
        tarifa
        envio
        propinaTotal
      }
    }
  }
`;

export const GET_VALORACION = gql`
  query getValoraciones($restaurant: ID) {
    getValoraciones(restaurant: $restaurant) {
      messages
      success
      data {
        id
        comment
        value
        user
        created_at
        Usuario {
          _id
          name
          lastName
          email
          city
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
      }
    }
  }
`;

export const GET_NOTIFICATION = gql`
  query getNotifications($Id: ID) {
    getNotifications(Id: $Id) {
      messages
      success
      notifications {
        _id
        user
        usuario
        read
        ordenId
        restaurant
        type
        created_at
        Riders {
          _id
          name
          lastName
          avatar
        }
        Restaurant {
          _id
          title
          image
          description
          type
          logo
          tipo
        }
        Usuarios {
          _id
          name
          lastName
          avatar
        }
        Orden {
          id
        }
      }
    }
  }
`;

export const RESTAURANT_SEARCH = gql`
  query getRestaurantSearch(
    $search: String
    $city: String
    $category: String
    $price: Int
    $llevar: Boolean
    $tipo: String
  ) {
    getRestaurantSearch(
      search: $search
      city: $city
      category: $category
      price: $price
      llevar: $llevar
      tipo: $tipo
    ) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        isnew
        phone
        slug
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const GET_OPINIONES = gql`
  query getOpinion($id: ID) {
    getOpinion(id: $id) {
      messages
      success
      data {
        id
        rating
        comment
        plato
        created_at
        user
        Usuario {
          _id
          name
          lastName
          avatar
        }
      }
    }
  }
`;

export const GET_ADRESS = gql`
  query getAdress($id: ID) {
    getAdress(id: $id) {
      success
      message
      data {
        id
        formatted_address
        puertaPiso
        type
        usuario
        lat
        lgn
      }
    }
  }
`;

export const GET_PRODUCT_SEARCH = gql`
  query getProductoSearch(
    $farmacy: String
    $search: String
    $page: Int
    $limit: Int
    $category: String
  ) {
    getProductoSearch(
      farmacy: $farmacy
      search: $search
      page: $page
      limit: $limit
      category: $category
    ) {
      messages
      success
      data {
        _id
        title
        ingredientes
        price
        previous_price
        imagen
        restaurant
        menu
        oferta
        anadidoCart
        cant
        popular
        cantd
        requireOption
        plu
        deliverectID
        opiniones {
          id
          plato
          comment
          rating
          created_at
          user
        }
        news
      }
    }
  }
`;

export const GET_STORE_IN_OFFERT = gql`
  query getStoreInOffert($city: String) {
    getStoreInOffert(city: $city) {
      messages
      success
      data {
        _id
        title
        image
        llevar
        description
        rating
        anadidoFavorito
        inOffert
        type
        Valoracion {
          id
          comment
          value
          created_at
        }
        adress {
          calle
          numero
          codigoPostal
          ciudad
          lat
          lgn
        }
        collections
        isDeliverectPartner
        categoryID
        autoshipping
        alegeno_url
        categoryName
        minime
        shipping
        extras
        menu
        tipo
        slug
        isnew
        phone
        email
        logo
        apertura
        cierre
        aperturaMin
        cierreMin
        diaslaborales
        open
        OnesignalID
        stimateTime
        ispartners
        highkitchen
        schedule {
          Monday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Tuesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Wednesday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Thursday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Friday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Saturday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }

          Sunday {
            day
            isOpen
            openHour1
            openHour2
            openMinute1
            openMinute2
            closetHour1
            closetHour2
            closetMinute1
            closetMinute2
          }
        }
      }
    }
  }
`;

export const GET_CUSTOM_ORDER = gql`
  query getCustomOrder($id: ID) {
    getCustomOrder(id: $id) {
      success
      messages
      data {
        id
        display_id
        riders
        Riders {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          rating
          OnesignalID
        }
        usuario {
          _id
          name
          lastName
          email
          avatar
          telefono
          created_at
          updated_at
          termAndConditions
          verifyPhone
          StripeID
          OnesignalID
        }
        origin {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        destination {
          address_name
          address_number
          postcode
          type
          lat
          lgn
        }
        schedule
        distance
        nota
        date
        city
        userID
        estado
        status
        progreso
        created_at
        total
        product_stimate_price
      }
    }
  }
`;

export const GET_CITY = gql`
  query getCity($city: String) {
    getCity(city: $city) {
      success
      messages
      data {
        id
        close
        city
        title
        subtitle
        imagen
      }
    }
  }
`;

export const OFFERT = gql`
  query getOfferts($city: String) {
    getOfferts(city: $city) {
      messages
      success
      data {
        id
        imagen
        city
        store
        slug
        cierre
        apertura
        open
      }
    }
  }
`;
