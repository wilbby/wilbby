import i18n from "i18next";
import Backend from "i18next-http-backend";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

import es from "./language/es.json";

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: "es",
    resources: { es },
    ns: ["common"],
    defaultNS: "common",
    debug: true,

    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
